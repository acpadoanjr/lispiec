(defpackage #:sclparser
  (:use #:cl #:iterate #:cl-ppcre #:cxml-stp)
  (:export
   #:vars-read-from-file
   #:load-and-save-site-with-confs
   #:delete-files-in-dir
   #:da-variable
   #:da-variable-ied
   #:da-variable-ld
   #:da-variable-ln
   #:da-variable-do
   #:da-variable-da
   #:da-variable-cdc
   #:da-variable-alias
   #:da-variable-fc
   #:da-variable-bay
   #:da-variable-ldbay
   #:da-variable-scada-function
   #:da-variable-voltage))
