(in-package :sclparser)

(defvar *scl* nil)
(defvar *ied* nil)

(defvar scl-uri "http://www.iec.ch/61850/2003/SCL")
(defvar rte-uri "http://www.rte-france.com")
(defvar compas-uri "https://www.lfenergy.org/compas/extension/v1")

(defun scl-write-to-file (file-path)
  (with-open-file (str file-path :direction :output :if-exists :supersede)
    (cxml-stp:serialize (parent *scl*) (cxml:make-character-stream-sink str :indentation 2))))

(defun substation-write-to-file (file-path)
  (with-open-file (str file-path :direction :output :if-exists :supersede)
    (scl-clean nil)
    (cxml-stp:serialize (parent *scl*) (cxml:make-character-stream-sink str :indentation 2))))

(defun ied-write-ip-to-file (file-path &optional (ied *ied*))
  (with-open-file (str file-path :direction :output :if-exists :supersede)
    (let ((ip (ied-get-ip))
          (iedname (get-element-name-attribute ied)))
      (print (cons iedname ip) str))))

(defun vars-write-to-file (vars file-path)
  (with-open-file (str file-path :direction :output :if-exists :supersede)
    (print vars str)))

(defun vars-write-to-file-json (vars file-path)
  (with-open-file (str file-path :direction :output :if-exists :supersede)
    (cl-json:encode-json vars str)))

(defun vars-read-from-file (file-path)
  (with-open-file (str file-path :direction :input :if-does-not-exist nil)
    (read str)))

(defun ied-ip-read-from-file (file-path)
  (with-open-file (str file-path :direction :input :if-does-not-exist nil)
    (read str)))

(defun ied-enum-read-from-file (file-path)
  (with-open-file (str file-path :direction :input :if-does-not-exist nil)
    (read str)))

(defun list-ieds-names ()
  (let ((ieds (get-ieds)))
    (mapcar #'(lambda (r) (value (find-attribute-named r "name"))) ieds)))

(defun find-in-list (lst-to-find attr-name)
  (lambda (el)
    (let ((aname (attribute-value el attr-name)))
      (find aname lst-to-find
            :test 'string=))))

(defun get-element-name-attribute (scl-element)
  (attribute-value scl-element "name"))

(defun get-cons-attribute (attr)
  (cons (local-name attr) (if (null (value attr)) "" (value attr))))

(defun get-cons-attribute-list (elem)
  (mapcar #'get-cons-attribute (list-attributes elem)))

(defun get-idrcs-long-label (node)
  (let ((idrcs (filter-recursively (of-name "IDRC" rte-uri) node)))
    (mapcar #'(lambda (r) (attribute-value r "longLabel")) idrcs)))

(defun get-idrc-long-label (idrc)
   (attribute-value idrc "longLabel"))

(defun get-idrc-bay (idrc)
  (substitute #\_ #\. (attribute-value idrc "BayLabel")))

(defun get-idrcs-apparition (node)
  (let ((idrcs (filter-recursively (of-name "IDRC" rte-uri) node)))
    (mapcar #'(lambda (r) (attribute-value r "ApparitionVal")) idrcs)))

(defun get-idrcs-cons-list (node)
  (let ((idrcs (filter-recursively (of-name "IDRC" rte-uri) node)))
    (mapcar #'get-cons-attribute-list idrcs)))

(defun get-idrcs-parent (node)
  (let ((idrcs (filter-recursively (of-name "IDRC" rte-uri) node)))
    (mapcar #'parent (mapcar #'parent idrcs))))

(defun get-idrc-parent (idrc)
  (parent (parent idrc)))

(defun only-space-str (str)
  (every #'(lambda (r) (or (char= r #\Space) (char= r #\Tab) (char= r #\NewLine))) str))

(defun find-empty-text (node)
  (and (typep node 'text)
       (only-space-str (data node))))

(defun detach-empty-text (node)
  (length (mapcar #'detach (filter-recursively #'find-empty-text node))))

(defun of-el-name-and-att-value (name attr value &optional (uri ""))
  (when (find #\: name)
    (stp-error "of-name used with QName as an argument"))
  (lambda (x)
    (and (typep x 'element)
         (or (null name) (equal (local-name x) name))
         (equal (namespace-uri x) uri)
         (find-attribute-named x attr)
         (string= (attribute-value x attr) value))))

(defun of-el-name-and-att-value-contains (name attr value &optional (uri ""))
  (when (find #\: name)
    (stp-error "of-name used with QName as an argument"))
  (lambda (x)
    (and (typep x 'element)
         (or (null name) (equal (local-name x) name))
         (equal (namespace-uri x) uri)
         (find-attribute-named x attr)
         (search value (attribute-value x attr) :test #'string=))))


(defun ied-get-logical-devices-attrs (ied)
  (let ((lds (filter-recursively (of-name "LDevice" scl-uri) ied)))
    (mapcar #'get-cons-attribute-list lds)))

(defun scl-flatten2d (lst-of-lst)
  (iter:iter outer
    (iter:for d1 iter:in lst-of-lst)
    (iter:iter (iter:for d2 iter:in d1)
      (iter:in outer (iter:collect d2)))))

(defun lst-els-remove-not-in-list! (input-lst attr-name lst-to-find)
  (mapcar #'detach (remove-if ;this in place of remove-if-not : because of detach function
                    (find-in-list lst-to-find attr-name)
                    input-lst)))

(defun lst-els-remove-not-in-list (input-lst attr-name lst-to-find)
  (remove-if-not
   (find-in-list lst-to-find attr-name)
   input-lst))

(defun ied-get-bay (ied)
  (let ((priv (find-child t ied
                          :key (of-el-name-and-att-value
                                "Private"
                                "type"
                                "COMPAS-Bay"
                                scl-uri))))
    (substitute #\_ #\. (attribute-value (first-child priv) "MainShortLabel"))))

(defun scl-get-bay (bay-name)
  (let* ((bay-new-name (substitute #\. #\_ bay-name))
         (bay (filter-recursively (of-el-name-and-att-value "Bay" "name" bay-new-name scl-uri)
                                  (get-substation))))
    (when (> (length bay) 0)
      (first bay))))


(defun bay-get-voltage (bay-name)
  (let ((bay (scl-get-bay bay-name)))
    (when bay
      (concatenate 'string "P_"
                   (attribute-value (parent bay) "name")))))

(defun ld-get-bay-by-idrc (ld-name &optional (ied *ied*))
  (let* ((lds (ied-get-logical-devices-els ied))
        (ld (find ld-name lds :key #'(lambda (r) (attribute-value r "inst")) :test 'string=)))
    (when ld
      (let ((idrcs (filter-recursively (of-name "IDRC" rte-uri) ld)))
        (when (> (length idrcs) 0)
          (get-idrc-bay (first idrcs)))))))

(defun get-parent-recursively (node parent-name)
  (let ((nn (local-name node)))
    (if node
        (if (string= nn parent-name)
            node
            (get-parent-recursively (parent node) parent-name))
        nil)))

(defun ld-get-bay-by-substation (ld-name &optional (ied *ied*))
  (let* ((iedname (get-element-name-attribute ied))
         (lds (ied-get-logical-devices-els ied))
         (ld (find ld-name lds :key #'(lambda (r) (attribute-value r "inst")) :test 'string=)))
    (when ld
      (let ((lns (filter-recursively (of-el-name-and-att-value "LNode" "iedName" iedname scl-uri) (get-substation))))
        (let ((my-ln (find "LLN0"
                           (remove-if-not #'(lambda (r) (string= (attribute-value r "ldInst") (attribute-value ld "inst"))) lns)
                           :test #'string=
                           :key #'(lambda (r) (attribute-value r "lnClass")))))
          (if my-ln
              (substitute #\_ #\. (get-element-name-attribute
                                   (get-parent-recursively my-ln "Bay")))
              (ld-get-bay-by-idrc ld-name)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                                        ;          da-variable-struct         ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defstruct da-variable
  ied
  ld
  ln
  do
  da
  fc
  cdc
  inds? ;;in dataset?
  alias
  bay
  ldbay
  voltage
  scada-function
  enumtype
  uuid)

(defun da-variable-to-alist (var)
  (loop for slot in '(da-variable-ied
                      da-variable-ld
                      da-variable-ln
                      da-variable-do
                      da-variable-da
                      da-variable-fc
                      da-variable-cdc
                      da-variable-inds?
                      da-variable-alias
                      da-variable-bay
                      da-variable-ldbay
                      da-variable-voltage
                      da-variable-scada-function
                      da-variable-enumtype
                      da-variable-uuid)
        collect (cons (subseq (string slot) 12) (funcall slot var))))

(defun da-variable-same-do? (var1 var2)
  (and (da-variable-p var1)
       (da-variable-p var2)
       (string= (da-variable-ied var1)
                (da-variable-ied var2))
       (string= (da-variable-ld var1)
                (da-variable-ld var2))
       (string= (da-variable-ln var1)
                (da-variable-ln var2))
       (string= (da-variable-do var1)
                (da-variable-do var2))))

(defun da-variable-update-do (var1-target var2)
  (when (da-variable-same-do? var1-target var2)
    (setf (da-variable-alias var1-target)
          (da-variable-alias var2))
    (setf (da-variable-voltage var1-target)
          (da-variable-voltage var2))
    (setf (da-variable-bay var1-target)
          (da-variable-bay var2))
    (setf (da-variable-uuid var1-target)
          (da-variable-uuid var2))))

(defparameter *var-regex* (create-scanner "(\\w+)\\|(\\w+)/(\\w+).(\\w+)\\((\\w+)\\)(.+)"))
(defparameter *var-regex-sdo* (create-scanner "\\.(\\w+)\\((\\w+)\\)(.+)"))
(defparameter *var-regex-da* (create-scanner "\\.\\[(\\w\\w)\\]\\.(.+)"))

(defun var-struct (var)
  (let ((ied "")
        (ld "")
        (ln "")
        (do1 "")
        (cdc1 "")
        (reg-rest nil)
        (do2 "")
        (cdc2 "")
        (reg-rest2 nil))
    (progn (ppcre:register-groups-bind (aied ald aln ado1 acdc1 areg-rest) (*var-regex* var)
             (setf ied aied)
             (setf ld ald)
             (setf ln aln)
             (setf do1 ado1)
             (setf cdc1 acdc1)
             (setf reg-rest areg-rest))
           (ppcre:register-groups-bind (ado2 acdc2 areg-rest2) (*var-regex-sdo* reg-rest)
             (setf reg-rest2 areg-rest2)
             (setf cdc2 acdc2)
             (setf do2 ado2)
             (unless (null reg-rest2)
               (setf do1 (concatenate 'string do1 "." do2))
               (setf cdc1 cdc2)
               (setf reg-rest reg-rest2)))
           (ppcre:register-groups-bind (afc ada) (*var-regex-da* reg-rest)
             (make-da-variable :ied ied
                               :ld ld
                               :ln ln
                               :do do1
                               :da ada
                               :cdc cdc1
                               :fc afc)))))

(defun ied-get-struct-vars (ied)
  (let* ((ldbay (ied-get-bay ied))
        (voltage (bay-get-voltage ldbay))
        (vars (mapcar #'var-struct (ied-get-variables-names ied))))
    (mapcar #'(lambda (r) (progn
                            (setf (da-variable-ldbay r) ldbay)
                            (setf (da-variable-voltage r) voltage)))
            vars)
    vars))

(defun set-ds-vars-with-fcda (vars fcda)
  (mapcar #'(lambda (r) (setf (da-variable-inds? r) t))
          (remove-if-not #'(lambda (r) (var-fcda-eq r fcda)) vars)))

(defun unset-ds-vars-q-t (vars)
  (let ((qts (remove-if-not #'(lambda (r) (or (string= (da-variable-da r) "q")
                                              (string= (da-variable-da r) "t")))
                            vars)))
    (iter:iter (iter:for v iter:in qts)
      (setf (da-variable-inds? v) nil))))

(defun set-ds-vars-with-fcdas (vars &optional (with-ds-name nil))
  (let ((fcdas (scl-get-fcdas-dos with-ds-name)))
    (iter:iter (iter:for v iter:in fcdas)
      (set-ds-vars-with-fcda vars v))
    (unset-ds-vars-q-t vars)
    (remove-if-not #'da-variable-inds? vars)))

(defun set-var-alias-with-idrcs (vars ied)
  (iter:iter (iter:for i iter:in (ied-get-idrc-var-struct ied))
    (iter:iter (iter:for v iter:in vars)
      (da-variable-update-do v i))))

(defun set-var-uuid-with-uuids (vars ied)
  (iter:iter (iter:for i iter:in (ied-get-uuid-var-struct ied))
    (iter:iter (iter:for v iter:in vars)
      (da-variable-update-do v i))))

(defun var-fcda-eq (var fcda)
  (and (string= (da-variable-ld var) (attribute-value fcda "ldInst"))
       (string= (da-variable-ln var) (concatenate 'string
                                                  (attribute-value fcda "prefix")
                                                  (attribute-value fcda "lnClass")
                                                  (attribute-value fcda "lnInst")))
       (string= (da-variable-do var) (attribute-value fcda "doName"))
                                        ;(string= (da-variable-da var) (attribute-value fcda "daName"))
       (string= (da-variable-fc var) (attribute-value fcda "fc"))))

(defparameter *reg-grp* (create-scanner "^LD((GRP|AGSA|GRPR|MODEXPF)[0-9]+|ASLD|GW|TOPO|ATB)$"))
(defun ied-get-supervised-vars (&optional (ied *ied*) (ds-regex ""))
  (let ((vars (set-ds-vars-with-fcdas
               (ied-get-struct-vars ied)
               ds-regex)))
    (set-var-uuid-with-uuids vars ied)
    (set-var-alias-with-idrcs vars ied)
    (mapcar #'(lambda (r) (setf (da-variable-scada-function r) (ld-get-fun (da-variable-ld r))))
            vars)
    (mapcar #'(lambda (v)
                (let ((newbay (ld-get-bay-by-substation (da-variable-ld v))))
                  (setf (da-variable-bay v) newbay)
                  (setf (da-variable-ldbay v) newbay)))
            (remove-if-not #'(lambda (v) (ppcre:scan *reg-grp* (da-variable-ld v))) vars))
    (mapcar #'(lambda (v)
                (unless (da-variable-bay v) (setf (da-variable-bay v) (da-variable-ldbay v))))
            vars)
    (mapcar #'(lambda (v)
                (setf (da-variable-voltage v) (bay-get-voltage (da-variable-ldbay v))))
            vars)
    (mapcar #'(lambda (v)
                (setf (da-variable-enumtype v) (get-btype-from-variable-name v)))
            vars)
    (remove-if #'(lambda (v) (let ((atname (da-variable-da v))
                                   (cdc (da-variable-cdc v)))
                               (or (search "origin" atname)
                                   (search "ctlNum" atname)
                                   (search "range" atname)
                                   (search "cval.ang" atname)
                                   (search "inst" atname)
                                   (search "ACD" cdc)
                                   (search "BCR" cdc)
                                   ;; (search "IN" cdc)
                                   ;; (search "LPL" cdc)
                                   (search "SEQ" cdc))))
               vars)))

(defun get-oldest-file-date (in-dir file-extension)
  (when (probe-file in-dir)
    (let ((files (directory (merge-pathnames (concatenate 'string "*." file-extension) in-dir))))
      (if files
          (apply #'min (iter:iter (iter:for f iter:in files)
                         (collect (file-write-date f))))
          0))))

(defun get-newest-file-date (in-dir file-extension)
  (when (probe-file in-dir)
    (let ((files (directory (merge-pathnames (concatenate 'string "*." file-extension) in-dir))))
      (if files
          (apply #'max (iter:iter (iter:for f iter:in files)
                       (collect (file-write-date f))))
          0))))

(defun delete-files-in-dir (in-dir file-extension)
  (when (probe-file in-dir)
    (let ((files (directory (merge-pathnames (concatenate 'string "*." file-extension) in-dir))))
      (mapcar #'uiop:delete-file-if-exists files))))

(defun cp-files-in-dir-to-tmp (in-dir file-extension)
  (when (probe-file in-dir)
    (let ((files (directory (merge-pathnames (concatenate 'string "*." file-extension) in-dir))))
      (mapcar #'(lambda (f) (uiop:copy-file f (merge-pathnames (file-namestring f) "/tmp/"))) files))))


(defparameter *reg-sec1* (create-scanner "^LDCMD(S\\w*)$"))
(defparameter *reg-sec2* (create-scanner "^LDSU(S\\w[0-9]?)$"))
(defparameter *reg-sec3* (create-scanner "^LD(S\\w\\w?)$"))
(defun ld-get-fun (ldinst)
  (cond
    ((search "LDCMDDJ" ldinst) "DJ")
    ((search "LDSUDJ" ldinst) "DJ")
    ((search "LDDJ" ldinst) "DJ")
    ((search "LDITFSUDJ" ldinst) "DJ")
    ((search "LDSUIED" ldinst) "IED")
    ((search "System" ldinst) "IED")
    ((ppcre:scan *reg-sec1* ldinst)
     (ppcre:register-groups-bind (f1) (*reg-sec1* ldinst) f1))
    ((ppcre:scan *reg-sec2* ldinst)
     (ppcre:register-groups-bind (f1) (*reg-sec2* ldinst) f1))
    ((ppcre:scan *reg-sec3* ldinst)
     (ppcre:register-groups-bind (f1) (*reg-sec3* ldinst) f1))
    (t "Tranche")))


(defun get-btype-from-variable-name (mystruct)
  (let ((server (ied-get-server))
        (myparent nil)
        (dtt (get-dtt)))
    (setf myparent
          (cxml-stp:find-child (da-variable-ld mystruct)
                               server
                               :key #'(lambda (r) (cxml-stp:attribute-value r "inst"))
                               :test #'string=))
    (setf myparent
          (find-child (da-variable-ln mystruct)
                      myparent
                      :key #'(lambda (r) (concatenate 'string
                                                      (attribute-value r "prefix")
                                                      (attribute-value r "lnClass")
                                                      (attribute-value r "inst")))
                      :test #'string=))
    (when myparent
      (let ((my-do-list (split "\\." (da-variable-do mystruct)))
            (my-da-list (split "\\." (da-variable-da mystruct)))
            (lntype
              (find-child (attribute-value myparent "lnType")
                          dtt
                          :key #'(lambda (r) (attribute-value r "id"))
                          :test #'string=)))
        (let* ((my-dotype (find-obj-r my-do-list lntype))
               (my-enum (find-obj-r my-da-list my-dotype)))
          (when my-enum (attribute-value my-enum "id")))))))

(defun find-obj-r (my-list obj)
  (let ((first-obj (car my-list))
        (rest-obj (cdr my-list)))
    (if first-obj
        (find-obj-r rest-obj
                    (find-child (attribute-value
                                 (find-child first-obj
                                             obj
                                             :key #'(lambda (r) (attribute-value r "name"))
                                             :test #'string=)
                                 "type")
                                (get-dtt)
                                :key #'(lambda (r) (attribute-value r "id"))
                                :test #'string=))
        obj)))

(defun get-enums ()
  (mapcar #'(lambda (e)
              (let ((id (attribute-value e "id"))
                    (enumvals (filter-children (of-name "EnumVal" scl-uri) e)))
                (cons id
                      (mapcar #'(lambda (v)
                                  (cons (parse-integer
                                         (attribute-value v "ord"))
                                        (if (first-child v)
                                          (data (first-child v))
                                          "none")))
                              enumvals))))
          (filter-children (of-name "EnumType" scl-uri) (get-dtt))))
