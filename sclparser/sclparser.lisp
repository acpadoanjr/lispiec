(in-package :sclparser)



;; (stp:serialize document (cxml-xmls:make-xmls-builder))
;; (make-octet-stream-sink stream :indentation 2 :canonical nil)

;; (defun load-scl (file-path)
;;   "Load a SCL file using stp builder"
;;   (let ((pth (probe-file file-path)))
;;     (setq *scl*
;;           (stp:find-child t
;;                           (cxml:parse
;;                            pth
;;                            myparser)
;;                           :key (of-name "SCL" scl-uri)))
;;     (detach-empty-text *scl*)
;;     (sb-kernel::gc :full t)))

(defun load-ied-scl (file-path iedname)
  "Load a SCL file using stp builder but ignores IED elements which are
not /iedname/. The IED filtering is executed using a SAX parser, preventing
high memory usage."
  (let ((pth (probe-file file-path))
        (myparser (make-scl-ied-filter (cxml:make-whitespace-normalizer (stp:make-builder)) iedname)))
    (setq *scl*
          (stp:find-child t
                          (cxml:parse
                           pth
                           myparser)
                          :key (of-name "SCL" scl-uri)))
    (detach-empty-text *scl*)
    (setq *ied* (first (get-ieds)))
    (scl-clean nil)
    (when *ied* (ied-fix-dais))
    (sb-kernel::gc :full t)
    (xml-ieds-name-found myparser)))

(defun load-and-save-site-with-confs (scd-path &optional (output-dir nil) (ds-regex ""))
  "This function reads a SCD file, splits it in several icd files (one per IED).
Output is saved in /output-dir/ if provided. Otherwise, the icd files are saved
in same directory.

In addition, it creates IED lisp files with a struct list containing
data attributes to be instantiated when loading a site with the simulator.

This function also calls a bash script that generates libiec61850 configuration files
to the simulator. Because the full process can take few minutes, the generation is only
executed when new files or editions are detected."
  (let ((pth (probe-file scd-path))
        (outp (if (null output-dir)
                  (make-pathname :directory (pathname-directory (probe-file scd-path)))
                  (probe-file output-dir))))
    (when (and pth outp (> (file-write-date pth) (get-oldest-file-date outp "icd")))
      (delete-files-in-dir pth "icd")
      (delete-files-in-dir pth "cfg")
      (let ((ieds (load-ied-scl pth "parse-no-ied")))
        (substation-write-to-file (merge-pathnames "substation.xml" outp))
        (iter:iter (iter:for ied in ieds)
          (load-ied-scl pth ied)
          (when (ied-get-server)
            (let ((vars (ied-get-supervised-vars *ied* ds-regex)))
              (vars-write-to-file vars
                                  (merge-pathnames
                                   (concatenate 'string ied ".lisp")
                                   outp))
              (vars-write-to-file-json (mapcar #'da-variable-to-alist
                                               vars)
                                       (merge-pathnames
                                        (concatenate 'string ied ".json")
                                        outp)))
            (vars-write-to-file (get-enums)
                                (merge-pathnames
                                 (concatenate 'string ied "-enums.lisp")
                                 outp))
            (ied-write-ip-to-file (merge-pathnames
                                   (concatenate 'string ied "-ip.lisp")
                                   outp))
            (scl-clean-substation!)
            (scl-write-to-file (merge-pathnames
                                (concatenate 'string ied ".icd")
                                outp))))))
    (when (and pth outp (> (get-newest-file-date outp "icd") (get-oldest-file-date outp "cfg")))
      (delete-files-in-dir pth "cfg")
      (uiop:with-current-directory  (outp)
        (with-open-file
            (str (merge-pathnames "generate.log" outp) :direction :output :if-exists :supersede)
          (uiop:run-program "generate.sh" :error-output str :if-error-output-exists :supersede))))))

(defun get-ieds ()
  (filter-children (of-name "IED" scl-uri) *scl*))

(defun get-dtt ()
  (first (filter-children (of-name "DataTypeTemplates" scl-uri) *scl*)))

(defun get-substation ()
  (first (filter-children (of-name "Substation" scl-uri) *scl*)))

(defun get-ied (name)
  (let ((ieds (get-ieds)))
    (find name ieds :test #'string= :key #'get-element-name-attribute)))


(defun get-idrc-name-var-struct (idrc)
  (let* ((doi (get-idrc-parent idrc))
         (doiname (get-element-name-attribute doi))
         (lnel (parent doi))
         (lnname (concatenate 'string
                              (attribute-value lnel "prefix")
                              (attribute-value lnel "lnClass")
                              (attribute-value lnel "inst")))
         (ldel (parent lnel))
         (ldname (attribute-value ldel "inst"))
         (iedel (parent (parent (parent ldel))))
         (iedname (get-element-name-attribute iedel)))
    (make-da-variable :ied iedname
                      :ld ldname
                      :ln lnname
                      :do doiname
                      :alias (get-idrc-long-label idrc)
                      :bay (get-idrc-bay idrc)
                      :voltage (bay-get-voltage (get-idrc-bay idrc)))))

(defun get-uuid-name-var-struct (uuid)
  (let* ((doi (parent (parent uuid)))
         (doiname (get-element-name-attribute doi))
         (lnel (parent doi))
         (lnname (concatenate 'string
                              (attribute-value lnel "prefix")
                              (attribute-value lnel "lnClass")
                              (attribute-value lnel "inst")))
         (ldel (parent lnel))
         (ldname (attribute-value ldel "inst"))
         (iedel (parent (parent (parent ldel))))
         (iedname (get-element-name-attribute iedel)))
    (make-da-variable :ied iedname
                      :ld ldname
                      :ln lnname
                      :do doiname
                      ;; :bay (get-uuid-bay uuid)
                      ;; :voltage (bay-get-voltage (get-uuid-bay uuid))
                      :uuid (attribute-value uuid "value"))))

(defun ied-get-idrc-var-struct (&optional (ied *ied*))
  (mapcar #'get-idrc-name-var-struct
          (filter-recursively (of-name "IDRC" rte-uri) ied)))

(defun ied-get-uuid-var-struct (&optional (ied *ied*))
  (mapcar #'get-uuid-name-var-struct
          (filter-recursively (of-name "UUID" rte-uri) ied)))

(defun ied-get-server (&optional (ied *ied*))
  (find-child-if (of-name "Server" scl-uri) (find-child-if (of-name "AccessPoint" scl-uri) ied)))

(defun ied-get-ip-el (&optional (ied *ied*))
  (let ((aps (first
              (filter-recursively
               (of-el-name-and-att-value "ConnectedAP" "iedName" (get-element-name-attribute ied) scl-uri)
               *scl*))))
    (unless (null aps)
      (let ((addrs (find-child t aps :key (of-name "Address" scl-uri))))
        (unless (null addrs)
          (find-child t addrs :key (of-el-name-and-att-value "P" "type" "IP" scl-uri)))
        ))))

(defun ied-get-logical-devices-els (&optional (ied *ied*))
  (let ((lds (filter-recursively (of-name "LDevice" scl-uri) ied)))
    lds))

(defun ied-get-ip (&optional (ied *ied*))
  (if (ied-get-ip-el ied)
      (remove #\Newline (remove #\Space (string-value (ied-get-ip-el ied))))
      "0.0.0.0"))

(defun ied-get-dais (&optional (ied *ied*))
  (filter-recursively (of-name "DAI" scl-uri) ied))

(defun ied-fix-dais (&optional (ied *ied*))
  (let ((dais (ied-get-dais ied)))
    (mapcar #'(lambda (r) (setf (cxml-stp:data
                                 (first-child (find-child-if (of-name "Val" scl-uri) r)))
                                "Not Parsable by spacio"))
            ;; (mapcar #'(lambda (dai)
            ;;             (string-value dai)))
            (remove-if #'(lambda (r) (or (string= (string-value r) "")
                                         (ppcre:scan "^(\\w|-)" (string-value r))))
                       dais))))


(defun ieds-remove-logical-devices-not-used! (&optional (ied *ied*))
  (unless (null ied)
    (let ((lds (ied-get-logical-devices-els ied)))
      (when (> (length lds) 0)
        (mapcar #'detach (remove-if-not #'ld-is-off-status-only lds))))))

(defun ld-is-off-status-only (ld)
  "This function can be used to check if a logical device should be ignored."
  (let* ((ln0 (cxml-stp:find-child t ld :key (of-name "LN0" scl-uri)))
         (doimod (cxml-stp:find-child t ln0 :key (of-el-name-and-att-value "DOI" "name" "Mod" scl-uri)))
         (stval (unless (null doimod) (cxml-stp:find-child t doimod :key (of-el-name-and-att-value "DAI" "name" "stVal" scl-uri))))
         (ctlmodel (unless (null doimod) (cxml-stp:find-child t doimod :key (of-el-name-and-att-value "DAI" "name" "ctlModel" scl-uri))))
         (stvalval (unless (null stval) (cxml-stp:find-child t stval :key (of-name "Val" scl-uri))))
         (ctval (unless (null ctlmodel) (cxml-stp:find-child t ctlmodel :key (of-name "Val" scl-uri)))))
    (unless (or (null doimod) (null stvalval) (null ctval))
      (and (string=  "off" (cxml-stp:data (cxml-stp:first-child stvalval)))
           (string= "status-only" (cxml-stp:data (cxml-stp:first-child ctval)))))))

(defun ld-get-logical-device-lns (ld)
  (let ((lns (cxml-stp:filter-children (of-name "LN" scl-uri) ld))
        (ln0s (cxml-stp:filter-children (of-name "LN0" scl-uri) ld)))
    (concatenate 'list lns ln0s)))

(defun ied-get-used-lntypes (&optional (ied *ied*))
  (let ((lst nil))
    (iter (for ld in (ied-get-logical-devices-els ied))
      (setf lst (concatenate 'list lst (mapcar #'(lambda (row) (cxml-stp:attribute-value row "lnType")) (ld-get-logical-device-lns ld)))))
    (remove-duplicates lst :test 'string=)))

(defun ieds-get-used-lntypes ()
  (scl-flatten2d
   (mapcar #'ied-get-used-lntypes (get-ieds))))

(defun dtt-get-lntypes ()
  (let ((lns (filter-children (of-name "LNodeType" scl-uri) (get-dtt))))
    lns))

(defun scl-clean-privates! ()
  (mapcar #'detach (filter-recursively (of-el-name-and-att-value "Private" "type" "RTE-Machines" scl-uri) *scl*))
  (mapcar #'detach (filter-recursively (of-el-name-and-att-value "Private" "type" "RTE-BAP" scl-uri) *scl*))
  (mapcar #'detach (filter-recursively (of-el-name-and-att-value "Private" "type" "RTE-FIP" scl-uri) *scl*))
  (mapcar #'detach (filter-recursively (of-el-name-and-att-value "Private" "type" "COMPAS-Flow" scl-uri) *scl*)))

(defun scl-clean-substation! ()
  (when (get-substation) (detach (get-substation))))

(defun ied-clean-ln-types-in-dtt! (ied)
  (let ((lst-to-find (ied-get-used-lntypes ied))
        (dtts (dtt-get-lntypes)))
    (lst-els-remove-not-in-list! dtts "id" lst-to-find)))

(defun ieds-clean-ln-types-in-dtt! ()
  (let ((lst-to-find (ieds-get-used-lntypes))
        (dtts (dtt-get-lntypes)))
    (lst-els-remove-not-in-list! dtts "id" lst-to-find)))

(defun scl-clean-communication! ()
  (let ((ieds (list-ieds-names))
        (adminsub (first (filter-recursively (of-el-name-and-att-value "SubNetwork" "name" "RSPACE_ADMIN_NETWORK" scl-uri) *scl*))))
    (when adminsub
      (detach adminsub))
    (lst-els-remove-not-in-list! (filter-recursively (of-name "ConnectedAP" scl-uri) *scl*) "iedName" ieds)))

(defun ieds-clean-do-types-in-dtt! ()
  (let ((lst-to-find (get-do-types))
        (dtts (filter-recursively (of-name "DOType" scl-uri) (get-dtt))))
    (lst-els-remove-not-in-list! dtts "id" lst-to-find)))

(defun ies-clean-do-types-in-dtt ()
  (let ((lst-to-find (get-do-types))
        (dtts (filter-recursively (of-name "DOType" scl-uri) (get-dtt))))
    (lst-els-remove-not-in-list dtts "id" lst-to-find)))

(defun ieds-clean-da-types-in-dtt! ()
  (let ((lst-to-find (get-da-types))
        (dtts (filter-recursively (of-name "DAType" scl-uri) (get-dtt)))
        (dttenum (filter-recursively (of-name "EnumType" scl-uri) (get-dtt))))
    (lst-els-remove-not-in-list! dttenum "id" lst-to-find)
    (lst-els-remove-not-in-list! dtts "id" lst-to-find)))


(defparameter *reg-white* (create-scanner "  +"))
(defun clean-element-whitespace (element-name)
  (let ((els (filter-recursively (of-name element-name scl-uri) *scl*)))
    (iter:iter (iter:for el iter:in els)
      (do-children (obj el)
        (let ((oldstring (cxml-stp:data obj)))
          (when (typep obj 'text)
            (setf (cxml-stp:data obj)
                  (remove #\Newline
                          (ppcre:regex-replace *reg-white* oldstring "")))))))))

(defparameter *reg-inref* (create-scanner "InRef[0-9]+"))
(defun scl-clean-inrefs! ()
  (let ((dos (filter-recursively (of-name "DO" scl-uri) *scl*))
        (dois (filter-recursively (of-name "DOI" scl-uri) *scl*)))
    (mapcar #'detach (remove-if-not #'(lambda (d) (ppcre:scan *reg-inref* (get-element-name-attribute d))) dos))
    (mapcar #'detach (remove-if-not #'(lambda (d) (ppcre:scan *reg-inref* (get-element-name-attribute d))) dois))))

(defun scl-clean (&optional (clean-substation t) (clean-inref nil))
  (ieds-remove-logical-devices-not-used!)
  (ieds-clean-ln-types-in-dtt!)
  (ieds-clean-do-types-in-dtt!)
  (ieds-clean-da-types-in-dtt!)
  (scl-clean-privates!)
  (scl-clean-communication!)
  (mapcar #'(lambda (r)
              (let ((at1 (find-attribute-named r "valKind"))
                    (at2 (find-attribute-named r "valImport")))
                (when at1 (remove-attribute r at1))
                (when at2 (remove-attribute r at2))))
          (filter-recursively (of-name "DAI" scl-uri) *scl*))
  (when clean-inref (scl-clean-inrefs!))
  (clean-element-whitespace "P")
  (clean-element-whitespace "Val")
  (clean-element-whitespace "EnumVal")
  (clean-element-whitespace "MinTime")
  (clean-element-whitespace "MaxTime")
  (when clean-substation
    (scl-clean-substation!)))

(defun get-do-types ()
  (let* ((l1 (remove-duplicates
              (mapcar #'(lambda (r) (attribute-value r "type"))
                      (filter-recursively (of-name "DO" scl-uri) (get-dtt)))
              :test 'string=))
         (dos (filter-children (find-in-list l1 "id") (get-dtt)))
         (sdo (mapcar #'(lambda (r) (filter-children (of-name "SDO" scl-uri) r)) dos))
         (l2 (remove-duplicates
              (mapcar #'(lambda (r) (attribute-value r "type"))
                      (scl-flatten2d sdo))
              :test 'string=)))
    (remove-duplicates (concatenate 'list l1 l2))))

(defun get-datype-recursively (datype-el-list &optional (prev-lst nil))
  (let* ((bdas
           (scl-flatten2d (mapcar #'(lambda (r) (filter-children (of-name "BDA" scl-uri) r)) datype-el-list)))
         (bdasid
           (remove-duplicates (mapcar #'(lambda (r) (attribute-value r "type")) bdas)))
         (newdatypes
           (filter-children (find-in-list bdasid "id") (get-dtt))))
    (if (eql 0 (length newdatypes))
        (concatenate 'list datype-el-list prev-lst)
        (get-datype-recursively newdatypes (concatenate 'list datype-el-list prev-lst)))))

(defun get-da-types ()
  (let* ((l1 (remove-duplicates
              (mapcar #'(lambda (r) (attribute-value r "type"))
                      (filter-recursively (of-name "DA" scl-uri) (get-dtt)))
              :test 'string=))
         (datypes1 (filter-children (find-in-list l1 "id") (get-dtt)))
         (datypesandenum (get-datype-recursively datypes1)))
    (remove-duplicates
     (mapcar #'(lambda (r) (attribute-value r "id"))
             datypesandenum)
     :test 'string=)))


(defun get-sdo-types ()
  (remove-duplicates
   (mapcar #'(lambda (r) (attribute-value r "type"))
           (filter-recursively (of-name "SDO" scl-uri) (get-dtt)))
   :test 'string=))

(defun dtt-get-child (id)
  (find-child id (get-dtt) :key #'(lambda (x) (attribute-value x "id")) :test 'string=))


(defun dtt-get-children (node &optional (name-prefix nil))
  (if (null node)
      nil
      (let* ((eltype (cxml-stp:local-name node))
             (real-children
               (cond ((string= eltype "LNodeType")
                      (filter-children (of-name "DO" scl-uri) node))
                     ((string= eltype "DOType")
                      (concatenate 'list
                                   (filter-children (of-name "SDO" scl-uri) node)
                                   (filter-children (of-name "DA" scl-uri) node)))
                     ((string= eltype "DAType")
                      (filter-children (of-name "BDA" scl-uri) node))
                     (t nil)))
             (lst (mapcar #'(lambda (r) (let ((fc (attribute-value r "fc"))
                                              (aname (attribute-value r "name")))
                                          (cons (if fc
                                                    (concatenate 'string "[" fc "]." aname)
                                                    aname)
                                                (attribute-value r "type"))))
                          real-children)))
        (mapcar #'(lambda (r)
                    (let ((n (dtt-get-child (cdr r))))
                      (cons (concatenate 'string
                                         name-prefix
                                         "."
                                         (car r)
                                         (when (and (not (null n))
                                                    (string= "DOType" (local-name n)))
                                           (concatenate 'string
                                                        "("
                                                        (attribute-value n "cdc")
                                                        ")")))
                            n)))
                lst))))


(defun dtt-get-variables-names (cons-name-node &optional (lst-var nil))
  (let ((ilst lst-var)
        (inode cons-name-node))
    (unless (consp cons-name-node)
      (setf inode (cons "" cons-name-node)))
    (let ((conschildren (dtt-get-children (cdr inode) (car inode))))
      (if (eql 0 (length conschildren))
          (setf ilst (push (car inode) ilst))
          (iter:iter (iter:for i iter:in conschildren)
            (setf ilst (dtt-get-variables-names i ilst))))
      ilst)))

(defun dtt-get-children-names (node)
  (mapcar #'car (dtt-get-children node)))

(defun ln-get-variables-names (ln-el  &optional (lst-var nil))
  (when (or (string= "LN" (local-name ln-el))
            (string= "LN0" (local-name ln-el)))
    (let ((lnname (concatenate 'string
                               (attribute-value ln-el "prefix")
                               (attribute-value ln-el "lnClass")
                               (attribute-value ln-el "inst")))
          (lntypeid (attribute-value ln-el "lnType")))
      (concatenate 'list
                   lst-var
                   (dtt-get-variables-names (cons lnname (dtt-get-child lntypeid)))))))

(defun ld-get-variables-names (ld-el  &optional (lst-var nil))
  (when (string= "LDevice" (local-name ld-el))
    (concatenate 'list
                 lst-var
                 (scl-flatten2d
                  (mapcar #'(lambda (r)
                              (mapcar
                               #'(lambda (d) (concatenate 'string (attribute-value ld-el "inst") "/" d))
                               (ln-get-variables-names r)))
                          (ld-get-logical-device-lns ld-el))))))

(defun ied-get-variables-names (ied-el  &optional (lst-var nil))
  (when (string= "IED" (local-name ied-el))
    (concatenate 'list
                 lst-var
                 (scl-flatten2d
                  (mapcar #'(lambda (r)
                              (mapcar
                               #'(lambda (d) (concatenate 'string (attribute-value ied-el "name") "|" d))
                               (ld-get-variables-names r)))
                          (ied-get-logical-devices-els ied-el))))))



(defun scl-get-ds (&optional (with-name nil))
  (let ((dss
          (filter-recursively (of-name "DataSet" scl-uri) *scl*))
        (reg (ppcre:create-scanner with-name)))
    (if with-name
        (remove-if-not #'(lambda (r) (ppcre:scan reg (attribute-value r "name"))) dss)
        dss)))


(defun scl-get-fcdas (&optional (with-name nil))
  (let ((dss (scl-get-ds with-name)))
    (iter:iter outer (iter:for ds iter:in dss)
      (iter:iter (iter:for fcda iter:in
                           (filter-children (of-name "FCDA" scl-uri) ds))
        (iter:in outer (collect fcda))))))

(defun scl-get-fcdas-dos (&optional (with-name nil))
  (scl-get-fcdas with-name) ;; not doing anymore this filter: returning directly this function.
  ;; (remove-if-not #'(lambda (r) (eql 0 (length (attribute-value r "daName"))))
                 ;; (scl-get-fcdas with-name))
  )
