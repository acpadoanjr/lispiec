(in-package :sclparser)


;;;;;;; filter
(defclass scl-ied-filter (cxml:sax-proxy)
  ((models :initform nil :accessor xml-filter-models)
   (ied-name :initform nil :accessor xml-ied-name :initarg :ied-name)
   (ieds-names-found :initform nil :accessor xml-ieds-name-found)
   (is-filtering :initform nil :accessor xml-is-filtering)))

(defun push-lname (lname handler)
  (push lname (xml-filter-models handler)))

(defun pop-lname (handler)
  (pop (xml-filter-models handler))
  (xml-filter-models handler))

(defun get-models-length (handler)
  (length (xml-filter-models handler)))

(defun make-scl-ied-filter (chained-handler ied-name)
  (make-instance 'scl-ied-filter
                 :chained-handler chained-handler
                 :ied-name ied-name))

(defun find-ied-with-name (handler attrs)
  (let ((att (sax:find-attribute "name" attrs)))
    (if (null att)
        nil
        (string= (sax:attribute-value att) (xml-ied-name handler)))))

(defun get-ied-name (handler attrs)
  (declare (ignore handler))
  (let ((att (sax:find-attribute "name" attrs)))
    (if (null att)
        nil
        (sax:attribute-value att))))


(defmethod sax:start-element
    ((handler scl-ied-filter) uri lname qname attrs)
  (declare (ignore uri))
  (when (string= lname "IED")
    (push (get-ied-name handler attrs) (xml-ieds-name-found handler)))
  (when (and (string= lname "IED")
             (not (find-ied-with-name handler attrs)))
    (setf (xml-is-filtering handler) t))
  (if (xml-is-filtering handler)
      (push-lname lname handler)
      (call-next-method)))

(defmethod sax:start-cdata ((handler scl-ied-filter))
  (unless (xml-is-filtering handler)
    (call-next-method)))
(defmethod sax:end-cdata ((handler scl-ied-filter))
  (unless (xml-is-filtering handler)
    (call-next-method)))

(defmethod sax:end-element ((handler scl-ied-filter) uri lname qname)
  (declare (ignore uri lname qname))
  (let ((yes-call-next-method-if-possible t))
    (when (xml-is-filtering handler)
      (pop-lname handler)
      (when (eql (get-models-length handler) 0)
        (setf (xml-is-filtering handler) nil)
        (setf yes-call-next-method-if-possible nil)))
    (when yes-call-next-method-if-possible
      (unless (xml-is-filtering handler)
        (call-next-method)))))

(defmethod sax:characters ((handler scl-ied-filter) data)
  (unless (xml-is-filtering handler)
    (call-next-method)))
