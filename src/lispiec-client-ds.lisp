(in-package :lispiec)

(defclass dataset-client ()
  ((iec-server
    :reader iec-server
    :initarg :iec-server)
   (ds-ref
    :reader ds-ref
    :initarg :ds-ref)
   (ds-capture
    :reader ds-capture
    :initform nil)
   (ds-old
    :initform nil
    :reader ds-old)
   (ds-capture-tmp
    :reader ds-capture-tmp
    :initform nil)
   (fcdas
    :reader fcdas)
   (fcdas-dobjs
    :initform nil)
   (var-spec
    :reader var-spec)
   (mms-list
    :reader mms-list)
   (dobj-list
    :reader dobj-list
    :initform '())))

(defstruct dobjs-with-mms
  name
  obj
  main-value
  q
  timestamp
  others)

(defmethod iec-server-client-make-dataset ((iec-server iec-server) ds-ref)
  (with-slots (client server client-ds) iec-server
    (when client
      (let ((my-objs (client-list-dataset-fcdas client ds-ref)))
        (if (eql my-objs
                 :ied-error-object-does-not-exist)
            :ied-error-object-does-not-exist
            (let ((inst (make-instance 'dataset-client
                                       :iec-server iec-server
                                       :ds-ref ds-ref)))
              (with-slots (ds-capture ds-capture-tmp ds-old fcdas var-spec fcdas-dobjs mms-list dobj-list) inst
                (setf ds-capture (client-read-ds client ds-ref))
                (setf ds-capture-tmp (client-read-ds client ds-ref))
                (setf ds-old (client-read-ds client ds-ref))
                (setf fcdas (client-list-dataset-fcdas-as-list client ds-ref))
                (setf var-spec (client-collect-var-spec-str-from-dataset (client iec-server) ds-ref))
                (setf fcdas-dobjs (mapcar #'(lambda (ff oo) (cons ff oo)) fcdas (dataset-client-get-concerned-data-objects inst)))
                (setf mms-list (mapcar #'cons (flatten var-spec) (flatten (dataset-listify (clientdataset-getvalues ds-capture)))))
                (setf dobj-list (mapcar #'(lambda (r) (dataset-client-find-attributes inst r))
                                        (delete-duplicates
                                         (dataset-client-get-concerned-data-objects inst)
                                         :test #'string= :key #'lispiec::obj-reference-str))))
              (unless (find ds-ref client-ds :test #'string= :key #'ds-ref)
                (push inst client-ds))
              inst)
          )))))

(defmethod iec-server-client-make-dataset :before ((iec-server iec-server) ds-ref)
  (restart-case (progn
                  (unless (iec-server-client-test iec-server)
                    (error (make-condition
                            'iec-server-client-connection-nil
                            :iec-server iec-server))))
    (start-connection () (iec-server-client-create-connection iec-server))
    (start-connection-local () (iec-server-client-create-connection iec-server t))))

(defmethod dataset-client-find-do-refs ((ds dataset-client))
  (delete-duplicates (mapcar #'first (fcdas ds)) :test #'string=))

(defgeneric dataset-client-compare-mms (ds &optional with-old))
(defmethod dataset-client-compare-mms ((ds dataset-client) &optional with-old)
  (with-slots (ds-capture ds-capture-tmp ds-old) ds
    (mmsvalue-equals (clientdataset-getvalues ds-capture)
                     (clientdataset-getvalues (if with-old
                                                  ds-old
                                                  ds-capture-tmp)))))

(defmethod dataset-client-update-mms-by-pool ((ds dataset-client))
  (with-slots (iec-server ds-ref ds-capture ds-capture-tmp) ds
    (let ((my-mms (client-read-ds (client iec-server) ds-ref ds-capture-tmp)))
      (when my-mms
        (unless (dataset-client-compare-mms ds)
          (mmsvalue-update
           (clientdataset-getvalues ds-capture) (clientdataset-getvalues my-mms)))))))

(defgeneric cdc-get-main-da-as-str (cdc)
  (:documentation "Only return the main value attribute as string")
  (:method ((cdc cdc-qt))
    "stVal")
  (:method ((cdc cdc-mv))
    (if (mv-is-float cdc)
        "mag.f"
        "mag.i"))
  (:method ((cdc cdc-cmv))
    "cVal.mag.f")
  (:method ((cdc cdc-act))
    "general"))

(defmethod dataset-client-get-name-with$ ((ds dataset-client))
  (ppcre:regex-replace-all "LLN0\."
                           (ds-ref ds)
                           "LLN0$"))

(defgeneric dataset-client-find-attributes (ds cdc)
  (:method ((ds dataset-client) (cdc cdc-qt))
    (with-slots (var-spec obj-reference-str) cdc
      (let ((da-addr (concatenate 'string
                                  obj-reference-str
                                  "."
                                  (cdc-get-main-da-as-str cdc)))
            (q-addr (concatenate 'string
                                 obj-reference-str
                                 ".q"))
            (t-addr (concatenate 'string
                                 obj-reference-str
                                 ".t")))
        (with-slots (mms-list) ds
          (make-dobjs-with-mms :name obj-reference-str
                               :obj cdc
                               :main-value (find da-addr mms-list :test #'string= :key #'car)
                               :q (find q-addr mms-list :test #'string= :key #'car)
                               :timestamp (find t-addr mms-list :test #'string= :key #'car)
                               :others nil))))))


(defun cdc-mms-struct-update (mms-struct)
  (when (dobjs-with-mms-main-value mms-struct)
    (cdc-set-value-as-supervision
     (dobjs-with-mms-obj mms-struct)
     (cdr (dobjs-with-mms-main-value mms-struct))
     (cdr (dobjs-with-mms-q mms-struct))
     (cdr (dobjs-with-mms-timestamp mms-struct))
     (dobjs-with-mms-others mms-struct))))

(defmethod dataset-client-find-mms-struct ((ds dataset-client) (cdc cdc))
  (with-slots (dobj-list) ds
    (find cdc dobj-list :key #'dobjs-with-mms-obj)))

(defgeneric cdc-set-value-as-supervision (cdc value q timestamp &rest other-values)
  (:documentation "this function is supposed to be used when goose update values")
  (:method ((cdc cdc-sps) value q timestamp &rest other-values)
    (declare (ignore others-values))
    (with-slots (input) cdc
      (let ((value-lisp (translate-from-foreign (mmsvalue-getboolean value) :boolean)))
        (if value-lisp
            (input-set-value-on input)
            (input-set-value-off input)))))
  (:method ((cdc cdc-ens) value q timestamp &rest other-values)
    (declare (ignore others-values))
    (with-slots (input) cdc
      (let ((value-lisp (translate-from-foreign (mmsvalue-toint32 value) :int32)))
        (input-set-value input value-lisp))))
  (:method ((cdc cdc-dpc) value q timestamp &rest other-values)
    (declare (ignore others-values))
    (with-slots (input) cdc
      (let ((value-lisp (dbpos-frommmsvalue value)))
        (input-set-value input value-lisp))))
  (:method ((cdc cdc-act) value q timestamp &rest other-values)
    (declare (ignore others-values)) ;TODO: make others values
    (with-slots (input) cdc
      (let ((value-lisp (translate-from-foreign (mmsvalue-getboolean value) :boolean)))
        (if value-lisp
            (input-set-value-on input)
            (input-set-value-off input)))))
  (:method ((cdc cdc-mv) value q timestamp &rest other-values)
    (declare (ignore others-values))
    (with-slots (input) cdc
      (let ((value-lisp (if (mv-is-float cdc)
                            (translate-from-foreign
                             (lispiec-internal::mmsvalue-todouble value) :double)
                            (translate-from-foreign
                             (lispiec-internal::mmsvalue-toint32 value) :int32))))
        (input-set-value input value-lisp)))))

(defmethod cdc-set-value-as-supervision :before ((cdc cdc) value q timestamp &rest other-values)
  (declare (ignore other-values))
  (with-slots (input) cdc
    (when q
      (setf
       (slot-value input 'forced-quality-int)
       (translate-from-foreign (mem-ref q :uint16) :uint16)))
    (let ((ts timestamp)
          (ts-int nil))
      (when ts
        (with-foreign-pointer (tm 1)
          (setf tm (timestamp-create))
          (timestamp-frommmsvalue tm timestamp)
          (setf ts-int (timestamp-gettimeinms tm))))
      (cdc-preset-timestamp cdc ts-int))))

(defmethod cdc-set-value-as-supervision :after ((cdc cdc) value q timestamp &rest other-values)
  (declare (ignore other-values))
  (cdc-update-model-force-if-disabled cdc))

(defmethod cdc-set-invalid-as-supervision ((cdc cdc-qt))
  (with-slots (input) cdc
    (site-quality-flag-default-old-data)
    (input-set-invalid input)
    (cdc-update-model-force-if-disabled cdc)
    (site-quality-flag-default-change)))


;; (defmethod dataset-client-compare-each-fcda)

(defmethod dataset-client-get-concerned-data-objects ((ds dataset-client))
  (with-slots (fcdas iec-server) ds
    (mapcar #'(lambda (r) (site-get-dataobject-by-ref (parent-site iec-server) (first r))) fcdas)))

(defmethod dataset-client-compare-old-mms ((ds dataset-client))
  (with-slots (ds-capture ds-old) ds
    (mms-compare-indexes (clientdataset-getvalues ds-capture) (clientdataset-getvalues ds-old))))

(defgeneric dataset-client-get-list-of-dobjs-changed (ds &optional force-all))
(defmethod dataset-client-get-list-of-dobjs-changed ((ds dataset-client) &optional (force-all nil))
  (with-slots (fcdas-dobjs) ds
    (let ((my-lst '())
          (compmms (dataset-client-compare-old-mms ds))
          (dobs (mapcar #'cdr fcdas-dobjs)))
      (mapcar #'(lambda (c f) (unless (and c (not force-all))
                                (pushnew f
                                         my-lst
                                         :test #'string=
                                         :key #'obj-reference-str)))
              compmms
              dobs)
      my-lst)))

(defgeneric dataset-values-update-data-objects (ds &optional force-all))
(defmethod dataset-values-update-data-objects ((ds dataset-client) &optional (force-all nil))
  (let ((my-dos (mapcar #'(lambda (r) (dataset-client-find-mms-struct ds r))
                        (dataset-client-get-list-of-dobjs-changed ds force-all))))
    (mapcar #'cdc-mms-struct-update my-dos)
    (mmsvalue-update (clientdataset-getvalues (ds-old ds))
                     (clientdataset-getvalues (ds-capture ds)))))

(defmethod dataset-values-set-invalid ((ds dataset-client))
  (with-slots (dobj-list) ds
    (mapcar #'cdc-set-invalid-as-supervision (mapcar #'dobjs-with-mms-obj dobj-list))))

(defmethod dataset-client-destroy ((ds dataset-client))
  (with-slots (ds-capture ds-capture-tmp ds-old iec-server) ds
    (when ds-capture (clientdataset-destroy ds-capture))
    (when ds-capture-tmp (clientdataset-destroy ds-capture-tmp))
    (when ds-old (clientdataset-destroy ds-old))
    (with-slots (client-ds) iec-server
      (let ((new-list (remove (ds-ref ds) client-ds :test #'string= :key #'ds-ref)))
        (setf client-ds new-list)))))
