(in-package #:lispiec)


;;;;;;;;;;;;;;;;;;;;;;;
;; generic container ;;
;;;;;;;;;;;;;;;;;;;;;;;

(defclass iec-container ()
  ((objs :accessor objs
         :initarg :objs
         :initform nil)
   (name :accessor name
         :initform ""
         :initarg :container-name
         :type string))
  (:documentation "This is a generic container class that is supposed to hold several
elements of another class object. It provides additional helpers/methods that
are designed to be useful in this application."))

(defmethod iec-container-remove ((container iec-container) (name string))
  (let ((ilist (remove name (objs container) :key #'car :test #'string=)))
    (setf (objs container) ilist)))

(defgeneric iec-container-add (container name obj)
  (:documentation "add an obj in objs alist"))

(defmethod iec-container-add ((container iec-container) (name string) obj)
  (if (null (objs container))
      (progn
        (setf (objs container) (list (cons name obj)))
        obj)
      (progn
        (iec-container-remove container name)
        (push (cons name obj) (objs container))
        obj)))

(defgeneric iec-container-get (container name)
  (:documentation "get an obj in objs alist"))

(defmethod iec-container-get ((container iec-container) (name string))
  (if (null (objs container))
      nil
      (cdr (assoc name (objs container) :test #'string=))))

(defun iec-container-get-secured (container name)
  (if (null container)
      nil
      (iec-container-get container name)))

(defmethod iec-container-get-index ((container iec-container) indx)
  (if (null (objs container))
      nil
      (cdr (nth indx (objs container)))))


(defmethod iec-container-list-names ((container iec-container))
  (let ((ilist (objs container)))
    (mapcar #'car ilist)))

(defmethod iec-container-list-objs ((container iec-container))
  (let ((ilist (objs container)))
    (mapcar #'cdr ilist)))


;;;;;;;;;;;;;;;;;;
;; Logical Node ;;
;;;;;;;;;;;;;;;;;;


(defclass iec-logical-node (iec-container)
  ())

(defmethod make-logical-node ((ln-name string))
  (make-instance 'iec-logical-node :container-name ln-name))

(defmethod iec-logical-node-add-data-object ((logical-node iec-logical-node) (obj cdc))
  (iec-container-add logical-node (obj-reference-str obj) obj))

(defmethod iec-logical-node-del-data-object ((logical-node iec-logical-node) (obj cdc))
  (iec-container-remove logical-node (obj-reference-str obj)))


;;;;;;;;;;;;;;;;;;;;
;; Logical Device ;;
;;;;;;;;;;;;;;;;;;;;

(defclass iec-logical-device (iec-container)
  ())

(defmethod make-logical-device ((ld-name string))
  (make-instance 'iec-logical-device :container-name ld-name))

(defmethod iec-logical-device-add-logical-node ((logical-device iec-logical-device) (ln-name string))
  (let ((existant-ln (iec-container-get logical-device ln-name)))
    (if (null existant-ln)
        (iec-container-add logical-device ln-name (make-logical-node ln-name))
        existant-ln)))

(defclass iec-server-container (iec-container)
  ())

(defmethod make-server-container ((ied-name string))
  (make-instance 'iec-logical-device :container-name ied-name)) ;; possible error

(defmethod iec-server-get-logical-device ((server iec-server) (ld-name string))
  (let ((icontainer (ld-container server)))
    (unless (null icontainer)
      (iec-container-get (ld-container server) ld-name))))

(defmethod iec-server-add-logical-device ((server iec-server) (ld-name string))
  (let ((container (ld-container server)))
    (when (null container)
      (setf (ld-container server) (make-server-container (iedmodel-getiedname (model server))))))
  (let ((existant-ld (iec-container-get (ld-container server) ld-name)))
    (if (null existant-ld)
        (iec-container-add (ld-container server) ld-name (make-logical-device ld-name))
        existant-ld)))

(defmethod iec-logical-device-get-dataobjects ((container iec-logical-device))
  (let ((lns (iec-container-list-objs container)))
    (iter outer (for i in lns)
      (iter (for j in (objs i))
        (in outer (collect j))))))

(defmethod iec-logical-device-get-list-alias ((container iec-logical-device))
  (mapcar #'(lambda (row)
	      (cons (car row) (alias (cdr row))))
	 (iec-logical-device-get-dataobjects container)))

(defmethod iec-server-get-dataobjects ((server iec-server))
  (unless (null (ld-container server))
    (let ((lds (iec-container-list-objs (ld-container server))))
      (iter outer (for i in lds)
        (iter (for j in (iec-logical-device-get-dataobjects i))
          (in outer (collect j)))))))

(defmethod iec-server-get-dataobjects-of-cdc ((server iec-server) cdc-type)
  (unless (null (ld-container server))
    (let ((lds (iec-container-list-objs (ld-container server))))
      (remove-if-not #'(lambda (i) (typep (cdr i) cdc-type))
                     (iter outer (for i in lds)
                       (iter (for j in (iec-logical-device-get-dataobjects i))
                         (in outer (collect j))))))))


(defmethod iec-server-add-dataobject ((server iec-server) (obj cdc))
  (let ((ldv (cdc-get-logical-device-str obj))
        (ln (cdc-get-logical-node-str obj)))
    (iec-logical-node-add-data-object
     (iec-logical-device-add-logical-node (iec-server-add-logical-device server ldv) ln) obj)))

(defmethod iec-server-del-dataobject ((server iec-server) (obj cdc))
  (let ((ldv (cdc-get-logical-device-str obj))
        (ln (cdc-get-logical-node-str obj)))
    (iec-logical-node-del-data-object
     (iec-logical-device-add-logical-node (iec-server-add-logical-device server ldv) ln) obj)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CDC instantiation function ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun  make-cdc (iec-server logicaldevice logicalnode dataobject str-cdc)
  (let ((dobj (alexandria:switch ((string-downcase str-cdc) :test #'equal)
                ("sps" (make-cdc-sps iec-server logicaldevice logicalnode dataobject))
                ("spc" (make-cdc-spc iec-server logicaldevice logicalnode dataobject))
                ("act" (make-cdc-act iec-server logicaldevice logicalnode dataobject))
                ("dpc" (make-cdc-dpc iec-server logicaldevice logicalnode dataobject))
                ("ens" (make-cdc-ens iec-server logicaldevice logicalnode dataobject))
                ("enc" (make-cdc-enc iec-server logicaldevice logicalnode dataobject))
                ("mv" (make-cdc-mv iec-server logicaldevice logicalnode dataobject))
                ("ins" (make-cdc-ins iec-server logicaldevice logicalnode dataobject))
                ("inc" (make-cdc-inc iec-server logicaldevice logicalnode dataobject))
                ("cmv" (make-cdc-cmv iec-server logicaldevice logicalnode dataobject))
                ("vss" (make-cdc-vss iec-server logicaldevice logicalnode dataobject))
                ("lpl" (make-cdc-lpl iec-server logicaldevice logicalnode dataobject)))))
    (unless (null dobj)
      (iec-server-add-dataobject iec-server dobj))
    dobj))


(defun  make-cdc-from-str (site-container iec-server-str logicaldevice logicalnode dataobject string-cdc alias-strg bay voltage-level function-level ldbay enumtype uuid)
  "This function is a helper function that facilitates the instantiation
of data objects inside a site and iec-server, organizing the
containers in a site (iec-servers containers) and in a
iec-server (logical-devices containers, logical nodes containers and
data-object containers)."
  (let ((server (iec-container-get site-container iec-server-str)))
    (unless (null server)
      (let ((dobj (make-cdc server logicaldevice logicalnode dataobject string-cdc)))
        (unless (null dobj)
          (when alias-strg
            (cdc-set-alias dobj alias-strg))
          (unless (string= bay "")
            (setf (bay dobj) bay))
          (unless (string= voltage-level "")
            (setf (voltage-level dobj) voltage-level))
          (unless (string= function-level "")
            (setf (function-level dobj) function-level))
          (unless (string= ldbay "")
            (setf (ldbay dobj) ldbay))
          (when enumtype
            (setf (enumtype dobj) enumtype))
          (when uuid
            (setf (uuid dobj) uuid)))
        dobj))))

(defun make-cdcs-from-export-csv (site-container csv-path)
  (let ((lst (get-arguments-from-export csv-path)))
    (mapcar #'(lambda (row)
                (make-cdc-from-str
                 site-container
                 (nth 0 row)
                 (nth 1 row)
                 (nth 2 row)
                 (nth 3 row)
                 (nth 4 row)
                 (nth 5 row)
                 (nth 6 row)
                 (nth 7 row)
                 (nth 8 row)
                 (nth 9 row)
                 (nth 10 row)
                 (nth 11 row)
                 ))
            lst)))

(defun make-cdcs-form-export-da-variables (site-container file-path ied-name)
  (let* ((fname (concatenate 'string ied-name ".lisp"))
         (f (probe-file (merge-pathnames fname file-path))))
    (unless (null f)
      (let ((vars (sclparser:vars-read-from-file file-path)))
        (mapcar #'(lambda (row)
                    (make-cdc-from-str
                     site-container
                     (sclparser:da-variable-ied row)
                     (sclparser:da-variable-ld row)
                     (sclparser:da-variable-ln row)
                     (sclparser:da-variable-do row)
                     (sclparser:da-variable-cdc row)
                     (sclparser:da-variable-alias row)
                     (sclparser:da-variable-bay row)
                     (sclparser:da-variable-voltage row)
                     (sclparser:da-variable-scada-function row)
                     (sclparser:da-variable-ldbay row)
                     (sclparser::da-variable-enumtype row)
                     (sclparser::da-variable-uuid row)))
                vars)))))

(defmethod iec-server-list-logical-devices ((server iec-server))
  (let ((lds (ld-container server)))
    (iec-container-list-names lds)))

(defun iec-server-get-dataobject (server ldinst lnname doname)
  (let ((ref (make-reference (model server) ldinst lnname doname)))
    (iec-container-get-secured
     (iec-container-get-secured
      (iec-server-get-logical-device
       server (ref-get-logical-device-str ref))
      lnname)
     ref)))

(defun iec-server-get-dataobject-from-ref (server ref)
  (iec-container-get-secured
   (iec-container-get-secured
    (iec-server-get-logical-device
     server (ref-get-logical-device-str ref))
    (ref-get-logical-node-str ref))
   ref))

(defun iec-server-init-dataobjects (server)
  (iedserver-lockdatamodel (server server))
  (iter (for row in (mapcar #'cdr (iec-server-get-dataobjects server)))
    (cdc-init row 't))
  (iedserver-unlockdatamodel (server server)))

(defun iec-server-invalid-dataobjects (server)
  (iter (for row in (mapcar #'cdr (iec-server-get-dataobjects server)))
    (cdc-set-invalid row)))

(defun iec-server-valid-dataobjects (server)
  (iter (for row in (mapcar #'cdr (iec-server-get-dataobjects server)))
    (cdc-set-valid row)))

(defun iec-server-set-ctlmodel (server ctlmodel)
  (iter (for row in (mapcar #'cdr (iec-server-get-dataobjects-of-cdc server 'cdc-xxc)))
    (cdc-set-ctlmodel row ctlmodel)))

(defun iec-server-free-dataobjects-ptrs (server)
  (iter (for row in (mapcar #'cdr (iec-server-get-dataobjects server)))
    (cdc-free-dobj-ref-str-ptr row)))

(defmethod destroy-iec-server ((obj iec-server))
  (unless (null (ied-name-ptr obj))
    (foreign-free (ied-name-ptr obj)))
  (iedserver-destroy (server obj))
  (iedmodel-destroy (model obj))
  (iec-server-free-dataobjects-ptrs obj))

(defun iec-server-get-dataobjects-ld-regex (server ld-regex-str)
  (let ((re1 (create-scanner ld-regex-str)))
    (remove-if-not #'(lambda (row) (scan re1 (cdc-get-logical-device-str (cdr row))))
                   (iec-server-get-dataobjects server))))

(defun iec-server-get-bay (server)
  (bay (cdr (nth 0 (iec-server-get-dataobjects server)))))

(defun iec-server-get-dataobjects-ln-regex (server ln-regex-str &optional ld-regex-str)
  (let ((re1 (create-scanner ln-regex-str))
        (lst '()))
    (if ld-regex-str
        (setf lst (iec-server-get-dataobjects-ld-regex server ld-regex-str))
        (setf lst (iec-server-get-dataobjects server)))
    (remove-if-not #'(lambda (row) (scan re1 (cdc-get-logical-node-str (cdr row))))
                   lst)))

(defun iec-server-get-dataobjects-regex (server do-regex-str &optional ln-regex-str ld-regex-str)
  (let ((re1 (create-scanner do-regex-str))
        (lst '()))
    (if ln-regex-str
        (setf lst (iec-server-get-dataobjects-ln-regex server ln-regex-str ld-regex-str))
        (if ld-regex-str
            (setf lst (iec-server-get-dataobjects-ld-regex server ld-regex-str))
            (setf lst (iec-server-get-dataobjects server))))
    (remove-if-not #'(lambda (row) (scan re1 (cdc-get-data-object-str (cdr row))))
                   lst)))

(defmethod cdc-unlock ((cdc cdc))
  (iedserver-unlockdatamodel (server (iec-server cdc)))
  (bordeaux-threads:release-lock (model-lock (iec-server cdc))))

(defun list-dataobjects-get-alias (list-dobjs)
  (mapcar #'(lambda (row)
              (cons (alias (cdr row))
                    (cdr row)))
          list-dobjs))

(defun list-dataobjects-get-bay-alias (list-dobjs)
  (mapcar #'(lambda (row)
              (cons (cdc-get-bay-alias (cdr row))
                    (obj-reference-str (cdr row))))
          (remove-if #'(lambda (row) (string= (alias (cdr row)) "")) list-dobjs)))


(defun iec-server-get-alias-regex (server alias-regex-str)
  (let ((re1 (create-scanner alias-regex-str)))
    (list-dataobjects-get-alias
     (remove-if-not #'(lambda (row) (scan re1 (alias (cdr row))))
                    (iec-server-get-dataobjects server)))))


(defun list-dataobjects-toggle (list-dobjs)
  (mapcar #'(lambda (row) (cons (car row) (cdc-toggle (cdr row))))
          list-dobjs))

(defun list-dataobjects-apply (func list-dobjs)
  (mapcar #'(lambda (row) (funcall func (cdr row)))
          list-dobjs))


(defun list-dataobjects-get-value (list-dobjs)
  (mapcar #'(lambda (row) (cons (car row) (cdc-get-value (cdr row))))
          list-dobjs))


(defun list-dataobjects-set-value (list-dobjs value)
  (mapcar #'(lambda (row) (cdc-set-value (cdr row) value))
          list-dobjs)
  (list-dataobjects-get-value list-dobjs))

(defun list-dataobjects-set-on (list-dobjs)
  (mapcar #'(lambda (row) (cdc-set-on (cdr row)))
          list-dobjs)
  (list-dataobjects-get-value list-dobjs))

(defun list-dataobjects-set-off (list-dobjs)
  (mapcar #'(lambda (row) (cdc-set-off (cdr row)))
          list-dobjs)
  (list-dataobjects-get-value list-dobjs))

(defun list-dataobjects-set-invalid (list-dobjs)
  (mapcar #'(lambda (row) (cdc-set-invalid (cdr row)))
          list-dobjs)
  (list-dataobjects-get-value list-dobjs))

(defun list-dataobjects-set-valid (list-dobjs)
  (mapcar #'(lambda (row) (cdc-set-valid (cdr row)))
          list-dobjs)
  (list-dataobjects-get-value list-dobjs))

(defun list-dataobjects-setctlmodel-direct-enhanced (list-dobjs)
  (mapcar #'(lambda (row) (cdc-set-ctlmodel (cdr row) :control-model-direct-enhanced))
          list-dobjs)
  (list-dataobjects-get-value list-dobjs))

