(in-package :lispiec)



(defgeneric cdc-client-read-value (obj &optional da-str))




;; (defmethod cdc-client-read-value ((obj cdc) &optional (da-str "stVal"))
;;   (restart-case (handler-bind ((iec-server-client-connection-nil
;;                                  (lambda (c)
;;                                    (invoke-restart 'connect-to-server (iec-server c)))))
;;                   ((make-read-value "ST")))
;;     (connect-to-server (my-server) )))

(defmethod cdc-client-read-value :before ((obj cdc) &optional da-str)
  (declare (ignore da-str))
  (with-slots (iec-server) obj
    (restart-case (progn
                    (unless (iec-server-client-test iec-server)
                      (error (make-condition
                              'iec-server-client-connection-nil
                              :iec-server iec-server))))
      (start-connection () (iec-server-client-create-connection iec-server))
      (start-connection-local () (iec-server-client-create-connection iec-server t)))))

(defmethod cdc-client-read-value ((obj cdc) &optional (da-str "stVal"))
  (with-slots (iec-server obj-reference-str) obj
    (make-read-value (client-get-da-fc (client iec-server)
                                       (concatenate
                                        'string obj-reference-str
                                        "."
                                        da-str)))))

(defmethod cdc-client-read-value ((obj cdc-mv) &optional (da-str "mag.f"))
  (with-slots (iec-server obj-reference-str) obj
    (make-read-value (client-get-da-fc (client iec-server)
                                       (concatenate
                                        'string obj-reference-str
                                        "."
                                        da-str)))))

(defmethod iec-server-client-test ((iec-server iec-server))
  (with-slots (client) iec-server
    (if client
        (with-foreign-object (err 'iedclienterror)
          (iedconnection-getserverdirectory client err nil)
          (if (eq :ied-error-ok (client-get-error err))
              t
              nil))
        nil)))

(defmethod iec-server-client-get-connection-state ((iec-server iec-server))
  (with-slots (client) iec-server
    (if client
        (iedconnection-getstate client)
        nil)))

(defmethod cdc-client-test ((obj cdc))
  (with-slots (iec-server) obj
    (iec-server-client-test iec-server)))

(defmethod cdc-client-get-connection-state ((obj cdc))
  (with-slots (iec-server) obj
    (iec-server-client-get-connection-state iec-server)))

(defmethod iec-server-client-read-value-from-ref ((iec-server iec-server) ref)
  (let ((my-client (client iec-server)))
    (if (iec-server-client-test iec-server)
        (with-foreign-object (err 'iedclienterror)
          (let* ((mms (lispiec-internal::iedconnection-readobject my-client err ref (functionalconstraint-fromstring (client-get-da-fc my-client ref))))
                 (my-error (client-get-error err))
                 (my-value (mms-to-value mms)))
            (if (eql my-error :ied-error-ok)
                (progn
                  (lispiec-internal::mmsvalue-delete mms)
                  my-value)
                my-error)))
        (error (make-condition 'iec-server-client-connection-nil :iec-server iec-server)))))

(defmethod iec-server-client-read-value-from-ref-and-connect ((iec-server iec-server) ref)
  (handler-case (iec-server-client-read-value-from-ref iec-server ref)
    (iec-server-client-connection-nil (c)
      (iec-server-client-close-connection iec-server)
      (sleep 2)
      (iec-server-client-create-connection iec-server (enable iec-server))
      (when (iec-server-client-get-connection-state iec-server)
        (iec-server-client-read-value-from-ref iec-server ref)))))

(defmethod iec-server-client-read-value ((iec-server iec-server)
                                         ld-regex-str
                                         ln-regex-str
                                         do-regex-str
                                         da-str)
  (let ((my-dos (iec-server-get-dataobjects-regex iec-server do-regex-str ln-regex-str ld-regex-str)))
    (if (null my-dos)
        (error "Data Object not found!")
        (let ((my-do (obj-reference-str (cdr (first my-dos)))))
          (iec-server-client-read-value-from-ref iec-server
                                                 (concatenate 'string my-do "." da-str))))))

(defun mms-to-value (mms-pointer)
  (when (and (pointerp mms-pointer) (not (pointer-eq mms-pointer (null-pointer))))
    (let ((my-type (mmsvalue-gettype mms-pointer)))
      (case my-type
        (:mms-integer (translate-from-foreign (lispiec-internal::mmsvalue-toint64 mms-pointer) :int64))
        (:mms-boolean (translate-from-foreign (lispiec-internal::mmsvalue-getboolean mms-pointer) :boolean))
        (:mms-float (translate-from-foreign (lispiec-internal::mmsvalue-todouble mms-pointer) :double))
        (:mms-utc-time (translate-from-foreign (lispiec-internal::mmsvalue-tounixtimestamp mms-pointer) :uint32))
        (:mms-string (translate-from-foreign (lispiec-internal::mmsvalue-tostring mms-pointer) :string))
        (:mms-octet-string (translate-from-foreign (lispiec-internal::mmsvalue-tostring mms-pointer) :string))
        (:mms-bit-string (translate-from-foreign (lispiec-internal::mmsvalue-getbitstringasinteger mms-pointer) :uint32))
        (:mms-visible-string (translate-from-foreign (lispiec-internal::mmsvalue-tostring mms-pointer) :string))
        (:mms-data-access-error :mms-data-access-error)))))

(defun value-to-mms (value)
  (cond ((subtypep (type-of value) 'boolean)
         (lispiec-internal::mmsvalue-newboolean (translate-to-foreign value :boolean)))
        ((subtypep (type-of value) 'string)
         (lispiec-internal::mmsvalue-newmmsstring (translate-to-foreign value :string)))
        ((subtypep (type-of value) 'integer)
         (lispiec-internal::mmsvalue-newintegerfromint32 (translate-to-foreign value :int32)))
        ))

(defmacro with-mms (mms value &body body)
  `(let ((,mms (value-to-mms ,value)))
     (block nil (return (progn ,@body))
            (lispiec-internal::mmsvalue-delete ,mms))))


(defmacro with-controlobjectclient-oper (ied-server reference ctlval &optional (orcat 2))
  `(let ((my-client (client ,ied-server)))
     (when (iec-server-client-test ,ied-server)
       (let ((my-oper (lispiec-internal::controlobjectclient-create ,reference my-client)))
         (unless (pointer-eq my-oper (null-pointer))
           (with-mms my-mms ,ctlval
             (lispiec-internal::controlobjectclient-setorigin my-oper "spacio" ,orcat)
             (lispiec-internal::controlobjectclient-setcommandterminationhandler my-oper
                                                                                 (oper-term-handler2 (parent-site ,ied-server)))
             (with-foreign-object (err 'iedclienterror)
               (let ((ret (lispiec-internal::controlobjectclient-operateasync my-oper err my-mms 0 (oper-term-handler (parent-site ,ied-server)) my-oper)))
                 (make-thread
                  #'(lambda ()
                      (sleep 4.0)
                      (lispiec-internal::controlobjectclient-destroy my-oper))
                  :name "operate client thread")
                 (client-get-error err)))))))))

(defgeneric cdc-client-operate (obj ctlval &optional orcat))

(defmethod  cdc-client-operate :before ((obj cdc-xxc) ctlval &optional orcat)
  (declare (ignore orcat))
  (with-slots (client-oper-t client-term-result client-term-addcause client-term-t) obj
    (setf client-term-result nil)
    (setf client-term-addcause nil)
    (setf client-term-t nil)
    (setf client-oper-t (lispiec-internal::hal-gettimeinms))))

(defmethod cdc-client-operate ((obj cdc-dpc) ctlval &optional (orcat 2))
  (with-slots (obj-reference-str iec-server) obj
    (if ctlval
        (with-controlobjectclient-oper
            iec-server
            obj-reference-str
            t
            orcat)
        (with-controlobjectclient-oper
            iec-server
            obj-reference-str
            nil
            orcat))))

(defmethod cdc-client-operate ((obj cdc-spc) ctlval &optional (orcat 2))
  (with-slots (obj-reference-str iec-server) obj
    (if ctlval
        (with-controlobjectclient-oper
            iec-server
            obj-reference-str
            t
            orcat)
        (with-controlobjectclient-oper
            iec-server
            obj-reference-str
            nil
            orcat))))

(defmethod cdc-client-operate ((obj cdc-enc) ctlval &optional (orcat 2))
  (with-slots (obj-reference-str iec-server) obj
    (when (subtypep (type-of ctlval) 'integer)
      (with-controlobjectclient-oper
          iec-server
          obj-reference-str
          ctlval
          orcat))))

(defmethod cdc-client-get-last-addcause ((obj cdc-xxc))
  (getf (slot-value obj 'client-term-addcause) 'lispiec-internal::addcause))

(defmethod cdc-client-get-last-success ((obj cdc-xxc))
   (slot-value obj 'client-term-result))

(defmethod cdc-client-get-last-term-t ((obj cdc-xxc))
  (slot-value obj 'client-term-t))

(defmethod cdc-client-get-last-oper-t ((obj cdc-xxc))
  (slot-value obj 'client-oper-t))

(defun client-set-value (client ref value)
  (with-mms my-mms value
    (client-set-value-with-mmsvalue client ref (client-get-da-fc client ref) my-mms)))

;; (defmacro make-write-value (fc-str)
;;   `(with-slots (obj-reference-str iec-server) obj
;;      (let ((my-client (client iec-server))
;;            (ref (concatenate 'string obj-reference-str "." da-str)))
;;        (if my-client
;;            (client-set-value my-client ref ,fc-str value)
;;            (error (make-condition 'iec-server-client-connection-nil :iec-server iec-server))))))

(defgeneric cdc-client-write-value (obj value &optional da-str))

(defmethod cdc-client-write-value ((obj cdc) value &optional (da-str "stVal"))
  (with-slots (obj-reference-str iec-server) obj
    (let ((my-client (client iec-server))
          (ref (concatenate 'string obj-reference-str "." da-str)))
      (if my-client
          (client-set-value my-client
                            ref
                            value)
          (error (make-condition 'iec-server-client-connection-nil
                                 :iec-server
                                 iec-server))))))

(defmethod iec-server-client-write-value ((obj iec-server) value da-ref)
  (let ((my-client (client obj)))
    (if my-client
        (client-set-value my-client
                          da-ref
                          value)
        (error (make-condition 'iec-server-client-connection-nil
                               :iec-server
                               obj)))))

(defclass site-client-goose-reciever ()
  ((site
    :initarg :site
    :reader site)
   (device-name
    :initarg :device-name
    :reader device-name)
   (goose-rec
    :reader goose-rec)
   (goose-subs
    :initform '()
    :reader goose-subs)))

(defmethod make-site-client-goose-receiver ((site site) device-name)
  (with-slots (goose-recs) site
    (let ((inst (make-instance 'site-client-goose-reciever :site site :device-name device-name)))
      (setf (slot-value inst 'goose-rec) (client-create-goose-reciever device-name))
      (unless (find device-name goose-recs :test #'string= :key #'device-name)
        (push inst goose-recs))
      inst)))

(defmethod site-client-destroy-goose-receivers ((site site))
  (with-slots (goose-recs) site
    (mapcar #'site-client-goose-reciever-stop-destroy goose-recs)))

(defmethod site-client-goose-reciever-stop-destroy ((rec site-client-goose-reciever))
  (with-slots (goose-rec goose-subs) rec
    (mapcar #'goose-client-subscriber-delete goose-subs)
    (client-goose-stop-and-destroy goose-rec)))

(defmethod site-client-goose-reciever-is-running? ((rec site-client-goose-reciever))
  (with-slots (goose-rec) rec
    (client-goose-is-running? goose-rec)))

(defmethod site-client-goose-reciever-get-nth ((site site) nth)
  (with-slots (goose-recs) site
    (nth nth goose-recs)))

(defmethod site-client-goose-reciever-start-listening ((rec site-client-goose-reciever))
  (with-slots (goose-rec) rec
    (client-goose-start-listening goose-rec)))

(defmethod site-client-goose-reciever-stop-listening ((rec site-client-goose-reciever))
  (with-slots (goose-rec) rec
    (client-goose-stop-listening goose-rec)))

(defmethod site-client-goose-receivers-start-listening ((site site))
  (with-slots (goose-recs) site
    (mapcar #'site-client-goose-reciever-start-listening goose-recs)))

(defmethod site-client-goose-receivers-stop-listening ((site site))
  (with-slots (goose-recs) site
    (mapcar #'site-client-goose-reciever-stop-listening goose-recs)))

(defmethod site-client-goose-receivers-are-running ((site site))
  (with-slots (goose-recs) site
    (mapcar #'site-client-goose-reciever-is-running? goose-recs)))

(defclass goose-client-subscriber ()
  ((receiver
    :reader receiver
    :initarg :receiver)
   (iec-server
    :reader iec-server
    :initarg :iec-server)
   (gocb-name
    :initarg :gocb-name
    :reader gocb-name)
   (gocb-sub
    :reader gocb-sub)
   (dataset
    :initarg :dataset
    :reader dataset)
   (mac-address
    :initarg :mac-address
    :reader :mac-address)
   (appid
    :initarg :appid
    :reader appid)
   (goid
    :initarg :goid
    :reader goid)
   ))

(defmethod goose-client-subscriber-is-valid ((goose-sub goose-client-subscriber))
  (lispiec-internal::goosesubscriber-isvalid (gocb-sub goose-sub)))

(defmethod make-goose-client-subscriber ((iec-server iec-server) (receiver site-client-goose-reciever) gocb-name)
  (with-slots (client client-ds parent-site) iec-server
    (unless (iec-server-client-test iec-server)
      (error 'iec-server-disabled :iec-server iec-server))
    (let ((gocbs (iec-server-client-get-goose-name iec-server)))
      (if (find gocb-name gocbs :test #'string=)
          (let* ((ds (cdr (find gocb-name
                                (iec-server-client-get-datasets-name-from-goose iec-server)
                                :test #'string= :key #'car)))
                 (dsc (let ((my-ds (find ds client-ds :test #'string= :key #'ds-ref)))
                        (if my-ds
                            my-ds
                            (iec-server-client-make-dataset iec-server ds))))
                 (mac (cdr (find gocb-name
                                 (iec-server-client-get-dstaddress-from-icd iec-server)
                                 :test #'string= :key #'car)))
                 (aid (cdr (find gocb-name
                                 (iec-server-client-get-appid-from-goose iec-server)
                                 :test #'string= :key #'car)))
                 (goid (cdr (find gocb-name
                                  (iec-server-client-get-goids-from-goose iec-server)
                                  :test #'string= :key #'car)))
                 (inst (make-instance 'goose-client-subscriber
                                      :receiver receiver
                                      :iec-server iec-server
                                      :dataset dsc
                                      :gocb-name gocb-name
                                      :mac-address mac
                                      :appid aid
                                      :goid goid)))
            (with-slots (goose-subs) receiver
              (unless (find gocb-name goose-subs :test #'string= :key #'gocb-name)
                (push inst goose-subs)))
            (with-slots (gocb-sub appid mac-address) inst
              (if parent-site
                  (setf gocb-sub (client-create-goose-subscriber
                                  parent-site
                                  (ppcre:regex-replace-all "LLN0\."
                                                           gocb-name
                                                           "LLN0$GO$")
                                  (ds-capture-tmp dsc)
                                  (ds-capture dsc)))
                  (error "standalone ied, not in a site, hence, no c subscriber is set."))
              (client-goose-subscriber-set-appid gocb-sub appid)
              (client-goose-subscriber-set-dst-mac gocb-sub
                                                   (aref mac-address 0)
                                                   (aref mac-address 1)
                                                   (aref mac-address 2)
                                                   (aref mac-address 3)
                                                   (aref mac-address 4)
                                                   (aref mac-address 5))
              (client-goose-receiver-add-subs (goose-rec receiver) gocb-sub)
              )
            inst)))))

(defmethod goose-client-subscriber-destroy ((goose-sub goose-client-subscriber))
  (let ((receiver-to-notify (receiver goose-sub))
        (gosub-to-destroy (gocb-sub goose-sub))
        (dataset-to-destroy (dataset goose-sub)))
    (dataset-client-destroy dataset-to-destroy)
    (site-client-goose-reciever-stop-listening receiver-to-notify)
    (client-goose-subscriber-destroy gosub-to-destroy)
    (with-slots (goose-subs) receiver-to-notify
      (let ((new-subs (remove (gocb-name goose-sub) goose-subs :test #'string= :key #'gocb-name)))
        (setf goose-subs new-subs)))))

(defmethod goose-client-subscriber-delete ((goose-sub goose-client-subscriber))
  (let ((receiver-to-notify (receiver goose-sub))
        (gosub-to-destroy (gocb-sub goose-sub))
        (dataset-to-destroy (dataset goose-sub)))
    ;; (dataset-client-destroy dataset-to-destroy)
    (site-client-goose-reciever-stop-listening receiver-to-notify)
    (with-slots (goose-subs) receiver-to-notify
      (let ((new-subs (remove (gocb-name goose-sub) goose-subs :test #'string= :key #'gocb-name)))
        (setf goose-subs new-subs)))))

;; (defun call-macro (r)
;;   (cl:macroexpand (with-goose-define-handler r)))

(defun search-ied-in-name (my-name str-to-search)
  (search my-name str-to-search :test #'string= :end2 (length my-name)))

(defmacro with-goose-define-handler (iec-server iec-server-name &body body)
  (let ((sym1 (gensym "goose-ev")))
    `(deeds:define-handler (,sym1 deeds::goose-received) (event message)
       :loop (event-loop (lispiec::parent-site ,iec-server))
       :filter '(lispiec::search-ied-in-name ,iec-server-name message)
       (let ((ds (find message
                       (lispiec::client-ds ,iec-server)
                       :test #'string=
                       :key #'lispiec::dataset-client-get-name-with$)))
         (when ds (dataset-values-update-data-objects ds)
               ,@body)))))

(defmacro iec-server-client-supervise-gooses (obj receiver gocb-regex)
  `(let ((lst (remove-if-not #'(lambda (r) (ppcre:scan ,gocb-regex r))
                             (lispiec::iec-server-client-get-goose-name ,obj))))
     (unless (enable ,obj)
       (let ((my-subs (mapcar #'(lambda (r) (lispiec::make-goose-client-subscriber ,obj
                                                                                   ,receiver
                                                                                   r))
                              lst)))
         (with-goose-define-handler ,obj (lispiec::iec-server-get-name ,obj))
         (mapcar #'dataset my-subs))
       (unless (site-is-goose-supervision-enabled (lispiec::parent-site ,obj))
         (site-start-client-goose-cycle (lispiec::parent-site ,obj)))
       (setf (slot-value ,obj 'supervised) t)
       t)))
