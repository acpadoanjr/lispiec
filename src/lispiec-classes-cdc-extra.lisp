(in-package :lispiec)

(defclass cdc-cmv (cdc-qt)
  ((range-ref :accessor range-ref
              :initform nil
              :initarg :range-reference)
   (range-ang-ref :accessor range-ang-ref
              :initform nil
              :initarg :range-ang-reference)
   (instmag-ref :accessor instmag-ref
                :initform nil
                :initarg :instmag-reference)
   (instmag-ang-ref :accessor instmag-ang-ref
                    :initform nil
                    :initarg :instmag-ang-reference)
   (dataattribute-ang-reference :accessor datr-ang-ref
                                :initform nil
                                :initarg :dataattribute-ang-reference)))


(defun make-cdc-cmv (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let ((inst (make-instance 'cdc-cmv
                               :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                               :dataobject-reference dobj
                               :dataattribute-reference (get-child-by-name (get-child-by-name (get-child-by-name dobj "cVal") "mag") "f")
                               :dataattribute-ang-reference (get-child-by-name (get-child-by-name (get-child-by-name dobj "cVal") "ang") "f")

                               :range-reference (get-child-by-name dobj "range")
                               :range-ang-reference (get-child-by-name dobj "rangeAng")

                               :instmag-reference (get-child-by-name (get-child-by-name (get-child-by-name dobj "instCVal") "mag") "f")
                               :instmag-ang-reference (get-child-by-name (get-child-by-name (get-child-by-name dobj "instCVal") "ang") "f")

                               :t-reference (get-child-by-name dobj "t")
                               :q-reference (get-child-by-name dobj "q")
                               :behavior-reference beh
                               :iec-server iec-server
                               :input (make-instance 'generic-input-cfloat))))
      (cdc-set-dobj-ref-str-ptr inst)
      inst)))

(defmethod cdc-get-value ((obj cdc-cmv))
  (let ((mms (datr-ref obj))
        (mms2 (datr-ang-ref obj)))
    (polar-to-complex
     (translate-from-foreign (iedserver-getfloatattributevalue (server (iec-server obj)) mms) :float)
     (if (null mms2)
         0
         (translate-from-foreign (iedserver-getfloatattributevalue (server (iec-server obj)) mms2) :float)))))


(defmethod cdc-update-model ((obj cdc-cmv) &optional unsafe force-if-disabled)
  (declare (ignore unsafe force-if-disabled))
  (let ((input-value-status (value (input obj)))
        (input-ang-status (value-ang (input obj)))
        (server (iec-server obj)))
    (unless (null (datr-ref obj))
      (iedserver-updatefloatattributevalue (server server)
                                           (datr-ref obj)
                                           (translate-to-foreign input-value-status :float)))
    (unless (null (datr-ang-ref obj))
      (iedserver-updatefloatattributevalue (server server)
                                           (datr-ang-ref obj)
                                           (translate-to-foreign input-ang-status :float)))
    (unless (null (instmag-ref obj))
      (iedserver-updatefloatattributevalue (server server)
                                           (instmag-ref obj)
                                           (translate-to-foreign input-value-status :float)))
    (unless (null (instmag-ang-ref obj))
      (iedserver-updatefloatattributevalue (server server)
                                           (instmag-ang-ref obj)
                                       (translate-to-foreign input-ang-status :float)))))

(defmethod cdc-set-value ((obj cdc-cmv) complex-value)
  (let ((my-input (input obj))
        (status (cdc-get-value obj))
        (complex-value-input (if (complexp complex-value)
                                 complex-value
                                 (complex (float complex-value)))))
    (unless (eq status complex-value)
      (progn (input-set-value my-input complex-value-input)
             (cdc-update-model-ignore-disabled obj)))))

(defmethod cdc-init ((obj cdc-cmv) &optional unsafe)
  (input-set-value (input obj) #C(0.0 0.0))
  (cdc-update-model-ignore-disabled obj unsafe))

(defmethod cdc-toggle ((obj cdc-cmv) &optional unsafe)
  (let ((my-input (input obj)))
    (if (eql 1.0 (value my-input))
        (input-set-value my-input #C(2.0 2.0))
        (input-set-value my-input 1.0))
    (cdc-update-model-ignore-disabled obj unsafe)
    (cdc-get-value obj)))

(defmethod is-sub-ena ((obj cdc-cmv))
  nil)

;helpers

(defun propagate-values (dobj-from dobj-to)
  (let ((from-value (cdc-get-value dobj-from))
        (to-value (cdc-get-value dobj-to)))
    (unless (eql from-value to-value)
      (cdc-set-value dobj-to from-value))))

(defun propagate-values-same-bay (dobj-from to-ld-regex to-ln-regex to-do-regex site)
  (let* ((possible-dobjs (site-get-dataobjects-by-regex site to-ld-regex to-ln-regex to-do-regex))
         (dbay (lispiec::bay dobj-from))
         (dobj2 (find-if #'(lambda (row)
                             (string= dbay (lispiec::bay (cdr row))))
                         possible-dobjs)))
    (unless (null dobj2)
      (propagate-values
       dobj-from
       (cdr dobj2)))))

(defun get-cdcs-same-bay (dobj-from to-ld-regex to-ln-regex to-do-regex site)
  (let* ((possible-dobjs (site-get-dataobjects-by-regex site to-ld-regex to-ln-regex to-do-regex))
         (dbay (lispiec::bay dobj-from)))
    (remove-if-not #'(lambda (row)
                 (string= dbay (lispiec::bay (cdr row))))
             possible-dobjs)))

(defun get-cdcs-same-bay-by-alias (dobj-from to-alias site)
  (let* ((possible-dobjs (site-get-dataobjects-by-alias site to-alias))
         (dbay (lispiec::bay dobj-from)))
    (find-if #'(lambda (row)
                 (string= dbay (lispiec::bay (cdr row))))
             possible-dobjs)))


(defclass cdc-act (cdc-qt)
  ((phsa-reference
    :accessor phsa-ref
    :initform nil
    :initarg :phsa-reference)
   (phsb-reference
    :accessor phsb-ref
    :initform nil
    :initarg :phsb-reference)
   (phsc-reference
    :accessor phsc-ref
    :initform nil
    :initarg :phsc-reference)
   (neut-reference
    :accessor neut-ref
    :initform nil
    :initarg :neut-reference)
   (input-phsa
    :accessor input-phsa
    :initform nil
    :initarg :input-phsa)
   (input-phsb
    :accessor input-phsb
    :initform nil
    :initarg :input-phsb)
   (input-phsc
    :accessor input-phsc
    :initform nil
    :initarg :input-phsc)
   (input-neut
    :accessor input-neut
    :initform nil
    :initarg :input-neut)))

(defun make-cdc-act (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let ((inst  (make-instance 'cdc-act
                                :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                                :dataobject-reference dobj
                                :dataattribute-reference (get-child-by-name dobj "general")
                                :phsa-reference (get-child-by-name dobj "phsA")
                                :phsb-reference (get-child-by-name dobj "phsB")
                                :phsc-reference (get-child-by-name dobj "phsC")
                                :neut-reference (get-child-by-name dobj "neut")
                                :t-reference (get-child-by-name dobj "t")
                                :q-reference (get-child-by-name dobj "q")
                                :behavior-reference beh
                                :iec-server iec-server
                                :input (make-instance 'generic-input-boolean)
                                :input-phsa (make-instance 'generic-input-boolean)
                                :input-phsb (make-instance 'generic-input-boolean)
                                :input-phsc (make-instance 'generic-input-boolean)
                                :input-neut (make-instance 'generic-input-boolean))))
      (cdc-set-dobj-ref-str-ptr inst)
      inst)))

(defmethod cdc-update-model ((obj cdc-act) &optional unsafe force-if-disabled)
  (declare (ignore unsafe force-if-disabled))
  (let ((input-value-status (value (input obj)))
        (input-value-status-phsa (value (input-phsa obj)))
        (input-value-status-phsb (value (input-phsb obj)))
        (input-value-status-phsc (value (input-phsc obj)))
        (input-value-status-neut (value (input-neut obj)))
        (server (iec-server obj)))
    (iedserver-updatebooleanattributevalue (server server)
                                           (datr-ref obj)
                                           input-value-status)
    (unless (null (phsa-ref obj))
      (iedserver-updatebooleanattributevalue (server server)
                                             (phsa-ref obj)
                                             input-value-status-phsa))
    (unless (null (phsb-ref obj))
      (iedserver-updatebooleanattributevalue (server server)
                                             (phsb-ref obj)
                                             input-value-status-phsb))
    (unless (null (phsc-ref obj))
      (iedserver-updatebooleanattributevalue (server server)
                                             (phsc-ref obj)
                                             input-value-status-phsc))
    (unless (null (neut-ref obj))
      (iedserver-updatebooleanattributevalue (server server)
                                             (neut-ref obj)
                                             input-value-status-neut))
))

(defmethod cdc-get-value ((obj cdc-act))
  (with-slots ((datr-ref dataattribute-reference)
               (phsa-ref phsa-reference)
               (phsb-ref phsb-reference)
               (phsc-ref phsc-reference)
               (neut-ref neut-reference))
      obj
    (let ((mms (iedserver-getattributevalue (server (iec-server obj)) datr-ref))
          (mmsa (when phsa-ref (iedserver-getattributevalue (server (iec-server obj)) phsa-ref)))
          (mmsb (when phsb-ref (iedserver-getattributevalue (server (iec-server obj)) phsb-ref)))
          (mmsc (when phsc-ref (iedserver-getattributevalue (server (iec-server obj)) phsc-ref)))
          (mmsn (when neut-ref (iedserver-getattributevalue (server (iec-server obj)) neut-ref))))
      (values (translate-from-foreign (mmsvalue-getboolean mms) :boolean)
              (when mmsa (translate-from-foreign (mmsvalue-getboolean mmsa) :boolean))
              (when mmsb (translate-from-foreign (mmsvalue-getboolean mmsb) :boolean))
              (when mmsc (translate-from-foreign (mmsvalue-getboolean mmsc) :boolean))
              (when mmsn (translate-from-foreign (mmsvalue-getboolean mmsn) :boolean))))))

(defmethod cdc-set-act-value ((obj cdc-act) value &optional (value-phsa nil) (value-phsb nil) (value-phsc nil) (value-neut nil))
  (with-slots (input input-phsa input-phsb input-phsc input-neut) obj
    (multiple-value-bind (st sta stb stc stn) (cdc-get-value obj)
      (unless (and (eq st value)
                   (eq sta value-phsa)
                   (eq stb value-phsb)
                   (eq stc value-phsc)
                   (eq stn value-neut))
        (if value
            (input-set-value-on input)
            (input-set-value-off input))
        (if value-phsa
            (input-set-value-on input-phsa)
            (input-set-value-off input-phsa))
        (if value-phsb
            (input-set-value-on input-phsb)
            (input-set-value-off input-phsb))
        (if value-phsc
            (input-set-value-on input-phsc)
            (input-set-value-off input-phsc))
        (if value-neut
            (input-set-value-on input-neut)
            (input-set-value-off input-neut))
        (cdc-update-model-ignore-disabled obj))
      (cdc-get-value obj))))

(defmethod cdc-set-value ((obj cdc-act) value)
  (cdc-set-act-value obj value))

(defmethod cdc-toggle ((obj cdc-act) &optional unsafe)
  (declare (ignore unsafe))
  (let ((status (cdc-get-value obj)))
    (if (eq status nil)
        (cdc-set-value obj t)
        (cdc-set-value obj nil))
    ))

(defmethod cdc-is-on ((obj cdc-act))
  (if (cdc-get-value obj)
      t
      nil))

(defmethod cdc-set-on ((obj cdc-act))
  (unless (cdc-is-on obj)
    (cdc-set-value obj t)))


(defmethod cdc-set-off ((obj cdc-act))
  (when (cdc-is-on obj)
    (cdc-set-value obj nil)))

(defmethod is-sub-ena ((obj cdc-act))
  nil)

(defgeneric cdc-set-quality-test (obj bit-value)
  (:method ((obj cdc-qt) bit-value)
    (let ((quality (cdc-get-q obj))
          (srv (server (iec-server obj))))
      (if (and bit-value
               (not (and (typep obj 'cdc-ens)
                         (or (string= "Beh" (cdc-get-data-object-str obj))
                             (string= "Mod" (cdc-get-data-object-str obj))
                             (string= "Health" (cdc-get-data-object-str obj))))))
          (iedserver-set-quality-flag-test srv quality)
          (iedserver-unset-quality-flag-test srv quality)))))


(defclass cdc-lpl (cdc)
  ((valrev-ref
    :accessor valrev-ref
    :initarg :valrev-ref
    :initform nil)
   (input-valrev
    :accessor input-valrev
    :initarg :input-valrev
    :initform nil)
   (vendor-ref
    :initarg :vendor
    :accessor vendor-ref)
   (swrev-ref
    :initarg :swrev
    :accessor swrev-ref)))

(defun make-cdc-lpl (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let ((inst  (make-instance 'cdc-lpl
                                :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                                :dataobject-reference dobj
                                :dataattribute-reference (get-child-by-name dobj "paramRev")
                                :valrev-ref (get-child-by-name dobj "valRev")
                                :behavior-reference beh
                                :iec-server iec-server
                                :input (make-instance 'generic-input-int)
                                :input-valrev (make-instance 'generic-input-int)
                                :vendor (get-child-by-name dobj "vendor")
                                :swrev (get-child-by-name dobj "swRev"))))
      (cdc-set-dobj-ref-str-ptr inst)
      inst)))

(defmethod cdc-get-value ((obj cdc-lpl))
  (if (and (datr-ref obj) (valrev-ref obj))
      (let ((mmsparam (iedserver-getattributevalue (server (iec-server obj)) (datr-ref obj)))
            (mmsval (iedserver-getattributevalue (server (iec-server obj)) (valrev-ref obj)))
            (mmsvendor (iedserver-getattributevalue (server (iec-server obj)) (vendor-ref obj)))
            (mmsswrev (iedserver-getattributevalue (server (iec-server obj)) (swrev-ref obj))))
        (values (cons "paramRev" (translate-from-foreign (mmsvalue-toint32 mmsparam) :int32))
                (cons "valRev" (translate-from-foreign (mmsvalue-toint32 mmsval) :int32))
                (cons "vendor" (translate-from-foreign (mmsvalue-tostring mmsvendor) :string))
                (cons "swRev" (translate-from-foreign (mmsvalue-tostring mmsswrev) :string))))
      nil))

(defmethod cdc-set-value ((obj cdc-lpl) int-value)
  (let ((my-input (input obj))
        (status (cdc-get-value obj)))
    (when status
      (unless (eq status int-value)
        (input-set-value my-input int-value)
        (cdc-update-model-ignore-disabled obj)
        (cdc-get-value obj)))))

(defmethod lpl-set-valrev ((obj cdc-lpl) int-value)
  (let ((my-input (input-valrev obj)))
    (when (cdc-get-value obj)
      (input-set-value my-input int-value)
      (cdc-update-model-ignore-disabled obj)
      (cdc-get-value obj))))

                                        ; non sense toggle, only to change values between 1 and 2
(defmethod cdc-toggle ((obj cdc-lpl) &optional unsafe)
  (let ((my-input (input obj)))
    (if (eql 1 (value my-input))
        (progn (input-set-value my-input 2)
               (input-set-value (input-valrev obj) 2))
        (progn (input-set-value my-input 1)
               (input-set-value (input-valrev obj) 1)))
    (cdc-update-model-ignore-disabled obj unsafe)
    (cdc-get-value obj)))

(defmethod cdc-update-model ((obj cdc-lpl) &optional unsafe force-if-disabled)
  (declare (ignore force-if-disabled unsafe))
  (with-lock-held ((model-lock (iec-server obj)))
    (let ((param (input obj))
          (val (input-valrev obj)))
      (iedserver-updateint32attributevalue (server (iec-server obj))
                                           (datr-ref obj)
                                           (translate-to-foreign (value param) :int32))
      (iedserver-updateint32attributevalue (server (iec-server obj))
                                           (valrev-ref obj)
                                           (translate-to-foreign (value val) :int32)))))

(defmethod cdc-init ((obj cdc-lpl) &optional unsafe)
  (declare (ignore obj unsabe)))

(defmethod is-sub-ena ((obj cdc-lpl))
  nil)


(defclass cdc-vss (cdc-qt) ())

(defmethod cdc-get-value ((obj cdc-vss))
  (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (datr-ref obj))))
    (translate-from-foreign (mmsvalue-tostring mms) :string)))

(defmethod cdc-get-status ((obj cdc-vss))
  (get-child-by-name (dobj-ref obj) "stVal"))


(defmethod cdc-set-value ((obj cdc-vss) string-value)
  (let ((my-input (input obj))
        (status (cdc-get-value obj)))
    (unless (string= status string-value)
      (progn (input-set-value my-input string-value)
             (cdc-update-model-ignore-disabled obj)))))

(defun make-cdc-vss (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let ((inst (make-instance 'cdc-vss
                               :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                                :dataobject-reference dobj
                                :dataattribute-reference (get-child-by-name dobj "stVal")
                                :t-reference (get-child-by-name dobj "t")
                                :q-reference (get-child-by-name dobj "q")
                                :behavior-reference beh
                                :iec-server iec-server
                                :input (make-instance 'generic-input-string))))
      (cdc-set-dobj-ref-str-ptr inst)
      inst)))

(defmethod cdc-update-model ((obj cdc-vss) &optional unsafe force-if-disabled)
  (declare (ignore unsafe force-if-disabled))
  (let ((input-value-status (value (input obj)))
        (server (iec-server obj)))
    (iedserver-updatevisiblestringattributevalue (server server)
                      (datr-ref obj)
                      (translate-to-foreign input-value-status :string))))

(defmethod cdc-init ((obj cdc-vss) &optional unsafe)
  (declare (ignore obj unsabe)))

(defmethod cdc-toggle ((obj cdc-vss) &optional unsafe)
  (let ((my-input (input obj)))
    (if (string= "1" (value my-input))
        (input-set-value my-input "2")
        (input-set-value my-input "1"))
    (cdc-update-model-ignore-disabled obj unsafe)
    (cdc-get-value obj)))


(defmethod is-sub-ena ((obj cdc-vss))
  nil)

(defclass cdc-org (cdc) ())


(defun make-cdc-org (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let ((inst (make-instance 'cdc-org
                               :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                               :dataobject-reference dobj
                               :dataattribute-reference (get-child-by-name dobj "setSrcRef")
                               :behavior-reference beh
                               :iec-server iec-server
                               :input (make-instance 'generic-input-string))))
      (cdc-set-dobj-ref-str-ptr inst)
      (iec-server-add-dataobject iec-server inst)
      inst)))

(defmethod cdc-get-value ((obj cdc-org))
  (if (datr-ref obj)
      (let ((vl (lispiec-internal::iedserver-getstringattributevalue (server (iec-server obj)) (datr-ref obj))))
        vl)))

(defmethod cdc-get-purpose ((obj cdc))
  (with-slots (dataobject-reference) obj
    (let ((pur (get-child-by-name dataobject-reference "purpose")))
      (if pur
        (let ((vl (lispiec-internal::iedserver-getstringattributevalue (server (iec-server obj)) pur)))
          vl)
        nil))))

(defmethod cdc-get-d ((obj cdc))
  (with-slots (dataobject-reference) obj
    (let ((dd (get-child-by-name dataobject-reference "d")))
      (if dd
          (let ((vl (lispiec-internal::iedserver-getstringattributevalue (server (iec-server obj)) dd)))
            vl)
          nil))))

(defmethod is-sub-ena ((obj cdc-org))
  nil)

(defmethod cdc-update-model ((obj cdc-org) &optional unsafe force-if-disabled)
  (declare (ignore force-if-disabled unsafe))
  (with-lock-held ((model-lock (iec-server obj)))
    (let ((src (input obj)))
      (iedserver-updatevisiblestringattributevalue (server (iec-server obj))
                                                   (datr-ref obj)
                                                   (translate-to-foreign (value src) :string))
      )))

(defmethod cdc-set-value ((obj cdc-org) string-value)
  (let ((my-input (input obj))
        (status (cdc-get-value obj)))
    (unless (string= status string-value)
      (progn (input-set-value my-input string-value)
             (cdc-update-model-ignore-disabled obj)))))

(defmethod cdc-init ((obj cdc-org) &optional unsafe)
  (declare (ignore obj unsabe)))

(defmethod cdc-toggle ((obj cdc-org) &optional unsafe)
  (let ((my-input (input obj)))
    (if (string= "1" (value my-input))
        (input-set-value my-input "2")
        (input-set-value my-input "1"))
    (cdc-update-model-ignore-disabled obj unsafe)
    (cdc-get-value obj)))
