(in-package :lispiec)


 ;;;;;;;;;;
 ;; cdc  ;;
 ;;;;;;;;;;

(defclass cdc ()
  ((input
    :accessor input
    :initform nil
    :initarg :input
    :documentation
    "This is an input object representing user level input Status, input
Validity and input Timestamp. When cdc-update method is called, the
input is considered in the calcultation of IEC61850
status (stVal,mag,..), q and t.")
   (obj-reference-str
    :accessor obj-reference-str
    :initarg :obj-reference-str
    :documentation
    "This slot represents the IEC61850 data object product address in
format IedNameLDInst/LN.DO")
   (obj-reference-str-ptr
    :accessor obj-reference-str-ptr
    :documentation
    "This slot represents a C char* variable containing the data object
product address as the previous slot. To be used with libiec6150 lib
when needed.")
   (uuid
    :accessor uuid
    :initarg :uuid
    :initform ""
    :type string
    :documentation
    "This is an user difined uuid to the data object. When the data object
is created automatically it will be associated with Rte UUID Code, if
present in SCD file.")
   (alias
    :accessor alias
    :initarg :alias
    :initform ""
    :type string
    :documentation
    "This is an user difined alias to the data object. When the data object
is created automatically it will be associated with Rte RISA Code, if
present in SCD file.")
   (bay
    :accessor bay
    :initarg :bay
    :initform ""
    :type string
    :documentation
    "This is the functional data object's Bay, which may be different from
the Logical Device's Bay. This is necessary to conform to Rte modeling principles.")
   (ldbay
    :accessor ldbay
    :initarg :ldbay
    :initform ""
    :type string
    :documentation
    "This is the data object's Logical Device's Bay.")
   (voltage-level
    :accessor voltage-level
    :initarg :voltage-level
    :initform ""
    :type string
    :documentation
    "This is the data object's Voltage Level.")
   (function-level
    :accessor function-level
    :initarg :function-level
    :initform ""
    :type string
    :documentation
    "This is the data object's Functional Level which makes sense only in
Rte Modelisation. Can be ignored othewise")
   (dataobject-reference
    :accessor dobj-ref
    :initform nil
    :initarg :dataobject-reference
    :documentation
    "This is a C pointer to the libiec61850's data object model being
referenced by this lisp object inside an instantiated ied server.")
   (behavior-reference
    :accessor beh-ref
    :initform nil
    :initarg :behavior-reference
    :documentation
    "This is a C pointer to the libiec61850 data object corresponding
Behavior data object's model inside an instantiated ied server.")
   (iec-server
    :accessor iec-server
    :initform nil
    :initarg :iec-server
    :documentation
    "This is a C pointer to the libiec61850's data object iec server.")
   (dataattribute-reference
    :accessor datr-ref
    :initform nil
    :initarg :dataattribute-reference
    :documentation
    "This is a C pointer to the libiec61850's main status data attribute
model inside an instantiated ied server (for instance, stVal or mag."))
  (:documentation "Base class for all Data Objects."))

(defclass cdc-qt (cdc)
  ((q-reference
    :accessor q-ref
    :initform nil
    :initarg :q-reference
    :documentation
    "This is a C pointer to the libiec61850's data object quality model.")
   (t-reference
    :accessor t-ref
    :initform nil
    :initarg :t-reference
    :documentation
    "This is a C pointer to the libiec61850's data object timestamp model."))
  (:documentation "Base class for all Data Objects containing q and t data attributes."))

(defclass cdc-sub ()
  ((subena-reference
    :accessor subena-ref
    :initform nil
    :documentation
    "This is a C pointer to the libiec61850's data attribute subena.")
   (subval-reference
    :accessor subval-ref
    :initform nil
    :documentation
    "This is a C pointer to the libiec61850's data attribute subval.")
   (subq-reference
    :accessor subq-ref
    :initform nil
    :documentation
    "This is a C pointer to the libiec61850's data attribute subq.")
   (is-sub
    :accessor is-sub
    :initform nil
    :documentation
    "This is a boolean representing the internal status of subena
libiec61850 data attribute."))
  (:documentation "Base class for all Data Objects containing substitution services."))

(defmethod cdc-get-main-attribute-triggerops ((obj cdc))
  "Methode utilisé pour inspection des triggerops de le DA dedans
spacio. Pas trop utilisé"
  (let ((da (datr-ref obj)))
    (dataattribute-gettrgops da)))

(defmethod make-subs ((obj cdc-sub))
  "This method is supposed to be called during the creation of a
cdc-sub. It looks por the substitution data attributs in the
libiec61850 model and create handler on subena mms write."
  (let ((subena (get-child-by-name (dobj-ref obj) "subEna"))
        (subval (get-child-by-name (dobj-ref obj) "subVal"))
        (subq (get-child-by-name (dobj-ref obj) "subQ")))
    (unless (null subena)
      (setf (is-sub obj) t)
      (setf (subena-ref obj) subena)
      (setf (subval-ref obj) subval)
      (setf (subq-ref obj) subq)
      (when (null (sub-handler (iec-server obj)))
        (sleep 2.0)) ;; creation handler not synchro ??.
      (iedserver-handlewriteaccess (server (iec-server obj)) subena (sub-handler (iec-server obj)) (obj-reference-str-ptr obj)))))

 (defmethod is-sub-ena ((obj cdc-sub))
   "Returns true if subEna is true on libiec61850 iec-server"
   (if (is-sub obj)
       (mmsvalue-getboolean (iedserver-getattributevalue (server (iec-server obj)) (subena-ref obj)))
       nil))

(defun make-reference (model-reference logicaldevice logicalnode dataobject)
  "Append strings to build a correct reference to be used in libiec61850."
  (let ((iedname (iedmodel-getiedname model-reference)))
    (concatenate 'string iedname logicaldevice "/" logicalnode "." dataobject)))

(defmethod cdc-set-dobj-ref-str-ptr ((obj cdc))
  (let ((ref (obj-reference-str obj)))
    (setf (obj-reference-str-ptr obj) (foreign-alloc :string :initial-element ref))))

(defmethod cdc-free-dobj-ref-str-ptr ((obj cdc))
  (foreign-free (obj-reference-str-ptr obj)))

(defmethod cdc-get-dobj-ref-str-ptr-value ((obj cdc))
  (get-string-from-pointer (obj-reference-str-ptr obj)))

(defun get-string-from-pointer (ptr)
  (translate-from-foreign (mem-ref ptr :string) :string))

(defmethod cdc-set-alias ((obj cdc) (name string))
  (setf (alias obj) name))

(defmethod cdc-get-bay-alias ((obj cdc))
  (let ((bay (bay obj))
        (al (alias obj)))
    (concatenate 'string bay "--> " al)))


(defgeneric cdc-get-value (obj)
  (:documentation "Get Data Object Value"))

(defgeneric cdc-get-status (obj)
  (:documentation "get status mms value"))

(defgeneric cdc-get-q (obj)
  (:documentation "get quality mms value"))

(defgeneric cdc-get-t (obj)
  (:documentation "get timestamp mms value"))

(defgeneric cdc-is-on (obj)
  (:documentation "Check is status is on, on depends on cdc"))


(defgeneric cdc-is-test (obj)
  (:documentation "Check is beh is test or test-bloked, on depends on cdc"))

(defgeneric cdc-get-beh (obj)
  (:documentation "Check is beh is test or test-bloked, on depends on cdc"))

(defmethod cdc-get-beh ((obj cdc))
  (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (get-child-by-name (beh-ref obj) "stVal"))))
    (translate-from-foreign (mmsvalue-toint32 mms) :int32)))

(defmethod cdc-is-test ((obj cdc))
  (let ((beh (cdc-get-beh obj)))
    (if (or (eql beh 3) (eql beh 4))
        t
        nil)))

(defmethod cdc-get-q ((obj cdc-qt))
  (get-child-by-name (dobj-ref obj) "q"))

(defmethod cdc-get-q-mms ((obj cdc-qt))
  (iedserver-getattributevalue (server (iec-server obj)) (cdc-get-q obj)))

(defmethod cdc-get-t ((obj cdc-qt))
  (get-child-by-name (dobj-ref obj) "t"))

(defvar *ref-regex* (create-scanner "^(\\w+)/(\\w+)\.([\\w\.]+)"))

(defun ref-split-references (ref)
  (multiple-value-bind (total vec)
      (scan-to-strings *ref-regex* ref)
    (declare (ignore total))
    vec))

(defmethod cdc-split-references ((obj cdc))
  (let ((ref (obj-reference-str obj)))
    (ref-split-references ref)))

(defun ref-get-logical-device-str (ref)
  (aref (ref-split-references ref) 0))

(defmethod cdc-get-logical-device-str ((obj cdc))
  (aref (cdc-split-references obj) 0))

;; (defmethod cdc-get-logical-device-inst-str ((obj cdc))
;;   (let* ((ref (cdc-get-logical-device-str obj))
;;          (ied (iec-server-get-name (iec-server obj)))
;;          (regname (concatenate 'string "^" ied "(\\w+)")))
;;     (multiple-value-bind (total vec)
;;         (scan-to-strings regname ref)
;;       (aref vec 0)))) ; too long and complicated

(defmethod cdc-get-logical-device-inst-str ((obj cdc))
  (let* ((ref (cdc-get-logical-device-str obj))
         (ied (iec-server-get-name (iec-server obj)))
         (len (length ied)))
    (subseq ref len nil)))


(defun ref-get-logical-node-str (ref)
  (aref (ref-split-references ref) 1))

(defun ref-get-data-object-str (ref)
  (aref (ref-split-references ref) 2))

(defmethod cdc-get-logical-node-str ((obj cdc))
  (aref (cdc-split-references obj) 1))

(defmethod cdc-get-data-object-str ((obj cdc))
  (aref (cdc-split-references obj) 2))


(defgeneric cdc-update-model (obj &optional unsafe force-if-disabled)
  (:documentation "update iec server model using input"))

(defgeneric cdc-toggle (obj &optional unsafe)
  (:documentation "toggle cdc-value"))

(defgeneric cdc-init (obj &optional unsafe)
  (:documentation "init cdc-value"))

(defgeneric cdc-compare-ctlval (obj ctlval-ptr)
  (:documentation "compare if command ctlval represents already the data object's state.
 To be used mainly on oper callbacks"))

(defgeneric cdc-set-output-ctlval (obj ctlval-ptr)
  (:documentation "set output from ctlval
 To be used mainly on oper callbacks"))


(defmethod cdc-set-on ((obj cdc))
  (declare (ignore obj)))
(defmethod cdc-set-off ((obj cdc))
  (declare (ignore obj)))


(defmethod cdc-toggle ((obj cdc) &optional unsafe)
  (declare (ignore obj unsafe)))


(defmethod cdc-set-value ((obj cdc) int-value)
  (declare (ignore obj int-value)))


(defmethod cdc-compare-ctlval ((obj cdc) ctlval-ptr)
  (declare (ignore obj ctlval-ptr)))

(defmethod cdc-set-output-ctlval ((obj cdc) ctlval-ptr)
  (declare (ignore obj ctlval-ptr)))

(defparameter day-names
  '("Monday" "Tuesday" "Wednesday"
    "Thursday" "Friday" "Saturday"
    "Sunday"))

(defconstant base-to-unix (encode-universal-time 0 0 0 1 1 1970 0))

(defun timestamp-decode (unix-time-with-mili)
  (let* ((unix-time (floor unix-time-with-mili 1000))
         (mili (- unix-time-with-mili (* 1000 unix-time))))
    (multiple-value-bind
          (second minute hour date month year day-of-week dst-p tz)
        (decode-universal-time (+ unix-time base-to-unix))
      (declare (ignore dst-p))
      (format nil "~2,'0d:~2,'0d:~2,'0d.~3,'0d of ~a, ~d/~2,'0d/~d (GMT~@d)"
              hour
              minute
              second
              mili
              (nth day-of-week day-names)
              month
              date
              year
              (- tz)))))

(defgeneric cdc-get-timestamp (obj &optional decode)
  (:documentation "get timestamp in miliseconds")
  (:method ((obj cdc-qt) &optional (decode nil))
    (let ((ts (t-ref obj)))
      (with-foreign-pointer (tm 1)
        (setf tm (timestamp-create))
        (timestamp-frommmsvalue tm (foreign-slot-value ts '(:struct dataattribute) 'mmsvalue))
        (if decode
            (timestamp-decode (timestamp-gettimeinms tm))
            (timestamp-gettimeinms tm)))))
  (:method ((obj cdc) &optional (decode nil))
    (if decode
        "this object does not contain timestamp"
        0)))


(defgeneric cdc-set-invalid (obj)
  (:method ((obj cdc-qt))
    (let ((my-input (input obj))
          (quality (cdc-get-q obj)))
      (unless (eq 2 (quality-getvalidity quality))
        (progn (input-set-invalid my-input)
               (cdc-update-model-ignore-disabled obj)))))
  (:method ((obj cdc))
    (declare (ignore obj))))


(defgeneric cdc-set-valid (obj)
  (:method ((obj cdc-qt))
           (let ((my-input (input obj))
                 (quality (cdc-get-q obj)))
             (unless (eq 0 (quality-getvalidity quality))
               (progn (input-set-valid my-input)
                      (cdc-update-model-ignore-disabled obj)))))
  (:method ((obj cdc))
    (declare (ignore obj))))

(defun mms-quality-get-validity (mms)
  (logand #b11 (quality-frommmsvalue mms)))

(defgeneric cdc-get-validity (obj)
  (:method ((obj cdc-qt))
    (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (q-ref obj))))
      (mms-quality-get-validity mms)))
  (:method ((obj cdc))
    3))

(defun mms-quality-is-q-sub (mms)
  (case (logand #b0010000000000 (quality-frommmsvalue mms))
    (1024 t)
    (t nil)))

(defgeneric cdc-is-q-sub (obj)
  (:method ((obj cdc-qt))
    (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (q-ref obj))))
      (mms-quality-is-q-sub mms)))
  (:method ((obj cdc))
    nil))

(defun mms-quality-is-q-test (mms)
  (case (logand #b0100000000000 (quality-frommmsvalue mms))
    (2048 t)
    (t nil)))

(defgeneric cdc-is-q-test (obj)
  (:method ((obj cdc-qt))
    (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (q-ref obj))))
      (mms-quality-is-q-test mms)))
  (:method ((obj cdc))
    nil))

(defun mms-quality-is-q-op-blocked (mms)
  (case (logand #b1000000000000 (quality-frommmsvalue mms))
    (4096 t)
    (t nil)))

(defgeneric cdc-is-q-op-blocked (obj)
  (:method ((obj cdc-qt))
    (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (q-ref obj))))
      (mms-quality-is-q-op-blocked mms)))
  (:method ((obj cdc))
    nil))

(defun mms-quality-get-q-detail (mms)
  (let ((masked-bits (logand #b0001111111100 (quality-frommmsvalue mms))))
    (case masked-bits
      (512 :inaccurate)
      (256 :inconsistent)
      (128 :olddata)
      (64 :failure)
      (32 :oscillatory)
      (16 :bad-reference)
      (8 :out-of-range)
      (4 :overflow)
      (t :none))))

(defgeneric cdc-get-q-detail (obj)
  (:method ((obj cdc-qt))
    (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (q-ref obj))))
      (mms-quality-get-q-detail mms))))

(defun mms-quality-get-q-detail-bit-number (mms)
  (let ((masked-bits (logand #b0001111111100 (quality-frommmsvalue mms))))
    (case masked-bits
      (512 masked-bits)
      (256 masked-bits)
      (128 masked-bits)
      (64 masked-bits)
      (32 masked-bits)
      (16 masked-bits)
      (8 masked-bits)
      (4 masked-bits)
      (t nil))))

(defgeneric cdc-get-q-detail-bit-number (obj)
  (:method ((obj cdc-qt))
    (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (q-ref obj))))
      (mms-quality-get-q-detail-bit-number mms))))

(defgeneric cdc-is-valid (obj)
  (:method ((obj cdc-qt))
    (eql (cdc-get-validity obj) 0))
  (:method ((obj cdc))
    (declare (ignore obj))
    t))

(defgeneric cdc-set-invalid (obj)
  (:method  ((obj cdc-qt))
    (let ((my-input (input obj))
          (quality (cdc-get-q obj)))
      (unless (eq 2 (quality-getvalidity quality))
        (progn (input-set-invalid my-input)
               (cdc-update-model-ignore-disabled obj)))))
  (:method ((obj cdc))
    (declare (ignore obj))))

(defmethod cdc-update-model :before ((obj cdc-qt) &optional unsafe force-if-disabled)
  (let ((mlocker (model-lock (iec-server obj)))
        (server (iec-server obj)))
    (unless (or (enable server) force-if-disabled)
      (error (make-condition 'iec-server-disabled :iec-server server)))
    (when (and (timestamp (input obj))
               (eql (timestamp (input obj)) (cdc-get-timestamp obj))) ;; to be used in future (goose updated)
      (error (make-condition 'cdc-warning-timestamps-the-same :dobj obj)))
    (unless (acquire-lock mlocker t)
      (error "aquire-locker timeout for cdc-update-model"))
    (unless unsafe
      (iedserver-lockdatamodel (server server)))))

;; (defmethod cdc-update-model :after ((obj cdc))
;;   (let ((mlocker (model-lock (iec-server obj)))
;;         (server (iec-server obj)))
;;     (iedserver-unlockdatamodel (server server))
;;     (release-lock mlocker)))

(defparameter *default-quality-flag* quality_detail_failure)

(defun site-quality-flag-default-change (&optional (quality-detail-type quality_detail_failure))
  (setf *default-quality-flag* quality-detail-type))

(defun site-quality-flag-default-change-overflow ()
  (site-quality-flag-default-change quality_detail_overflow))

(defun site-quality-flag-default-change-oper-blocked ()
  (site-quality-flag-default-change quality_operator_blocked))

(defun site-quality-flag-default-old-data ()
  (site-quality-flag-default-change quality_detail_old_data))


(defmethod cdc-update-model :after ((obj cdc-qt) &optional unsafe force-if-disabled)
  (declare (ignore force-if-disabled))
  (with-slots (input iec-server q-reference t-reference) obj
    (let ((input-value-v (validity input))
          (input-value-t (timestamp input))
          (input-value-q-detail (forced-q-detail input))
          (input-value-q-test (forced-q-test input))
          (input-value-q-sub (forced-q-sub input))
          ;; (input-value-op-bk (forced-op-bk input))
          (input-value-q-int (forced-quality-int input))
          (server (server iec-server))
          (mlocker (model-lock iec-server))
                                        ;(dobj (dobj-ref obj))
          )
      (if input-value-q-int ;; force
          (lispiec-internal::iedserver-set-quality-from-int server
                                                            q-reference
                                                            input-value-q-int)
          (progn (if (or (is-sub-ena obj) input-value-q-sub)
                     (iedserver-set-quality-flag-substitute server q-reference)
                     (if input-value-v
                         (iedserver-set-quality server q-reference quality_validity_good)
                         (iedserver-set-quality-flag-with-invalid server
                                                                  q-reference
                                                                  (if input-value-q-detail
                                                                      input-value-q-detail
                                                                      *default-quality-flag*))))
                 (cdc-set-quality-test obj (if input-value-q-test
                                               t
                                               (cdc-is-test obj)))))

      (if input-value-t        ;TODO treat input not 0
          (iedserver-update-timestamp server t-reference input-value-t)
          (iedserver-update-timestamp-to-now server t-reference))
      (input-unforce input)
      (unless unsafe (iedserver-unlockdatamodel server))
      (release-lock mlocker))))

(defgeneric cdc-update-model-ignore-disabled (obj &optional unsafe)
  (:documentation "if ied is disabled, do not proceed with update.")
  (:method ((obj cdc) &optional unsafe)
    (handler-case (cdc-update-model obj unsafe)
      (iec-server-disabled (c)
        (format t "INFO: cdc-update on ~a not triggered because ied ~a is disabled."
                (obj-reference-str obj)
                (iec-server-get-name (iec-server c)))))))

(defgeneric cdc-update-model-force-if-disabled (obj &optional unsafe)
  (:documentation "if ied is disabled, the update will be done anyway.")
  (:method ((obj cdc) &optional unsafe)
    (handler-case (cdc-update-model obj unsafe t)
      (cdc-warning-timestamps-the-same (c)
        ;; (format t "INFO: GOOSE not updated due timestamp the same.")
        ))))

(defmethod cdc-init ((obj cdc-qt) &optional unsafe)
  (with-slots (iec-server) obj
    (when (enable iec-server))
    (cdc-update-model-ignore-disabled obj unsafe)))

(defgeneric cdc-preset-timestamp (obj &optional timestamp)
  (:documentation "it will probably be used in with the next cdc-update-model. Be careful.")
  (:method ((obj cdc-qt) &optional (timestamp nil))
    (with-slots (input) obj
      (if timestamp
          (input-set-timestamp input timestamp)
          (input-set-timestamp-now input)))))

(defclass cdc-xxc ()
  ((handler
    :accessor handler :initform nil
    :documentation
    "This is the handler to treat an Oper request with Termination")
   (perf-handler
    :accessor perf-handler :initform nil
    :documentation
    "This is the handler to treat an Oper request and give a oper+ or oper-
response.")
   (output :accessor output :initform nil)
   (ctlmodel-ref
    :accessor ctlmodel-ref :initarg :ctlmodel-reference :initform nil
    :documentation
    "This is a pointer to ctlModel data attribute in libiec61850 model.")
   (is-in-operation
    :accessor is-in-operation :initform nil
    :documentation
    "Returns true if Command Termination timeout handler is not yet expired")
   (operation-timeout-sec
    :accessor operation-timeout-sec :initform 2
    :documentation
    "Command Termination timeout. Can be changed dynamically.")
   (operation-accepted-timestamp
    :accessor operation-accepted-timestamp
    :documentation "Timestamp recorded for each oper received and accepted")
   (simulated-reaction-time-sec
    :accessor simulated-reaction-time-sec :initform 0.05
    :documentation
    "This is a data object simulated reaction time, whenever an oper
service is accepted, the data object will automatically be set to
ctlVal, but only after this reaction time has expired.")
   (client-term-addcause :reader client-term-addcause :initform nil)
   (client-term-result :reader client-term-result :initform nil)
   (client-term-t :reader client-term-t :initform nil)
   (client-oper-t :reader client-oper-t :initform nil))
  (:documentation
   "This is the base class to be used to all controlable data-object/"))

(defgeneric cdc-is-locked (obj)
  (:documentation "is object locked for control")
  (:method ((obj cdc-xxc))
    (declare (ignore obj))
    nil))

(defgeneric cdc-compare-output-value (obj)
  (:documentation "compare if objects value and output are the same.
 To be used mainly on oper callbacks")
  (:method ((obj cdc-xxc))
    (eql (output obj) (cdc-get-value obj))))

(defgeneric cdc-set-ctlmodel (obj ctlmodel)
  (:documentation "update ctlmodel"))

(defgeneric cdc-get-ctlmodel (obj)
  (:documentation "update ctlmodel"))


(defmethod cdc-set-ctlmodel ((obj cdc-xxc) ctlmodel)
  (iedserver-updateint32attributevalue
   (server (iec-server obj))
   (ctlmodel-ref obj) (foreign-enum-value 'controlmodel ctlmodel)))

(defmethod cdc-get-ctlmodel ((obj cdc-xxc))
  (mmsvalue-toint32 (iedserver-getattributevalue
                     (server (iec-server obj))
                     (ctlmodel-ref obj))))

(defmacro make-if-controlable (obj &body body)
  `(let ((my-ctlmodel (cdc-get-ctlmodel ,obj)))
     (when (or (eql my-ctlmodel 3)
               (eql my-ctlmodel 1))
       (progn ,@body))))
