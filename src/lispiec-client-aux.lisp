(in-package :lispiec)

(defun client-create ()
  (iedconnection-create))

(defun client-get-error (iedclienterror-ptr)
  (mem-ref iedclienterror-ptr 'iedclienterror))

(defun client-connect (client ip tcpport)
  (declare (number tcpport) (type string ip))
  (with-foreign-object (err 'iedclienterror)
    (iedconnection-connect client err ip tcpport)
    (mem-ref err 'iedclienterror)))


(defun client-close-and-destroy (client)
  (when (eql (iedconnection-getstate client) :ied-state-connected)
    (iedconnection-close client))
  (iedconnection-destroy client))

(defparameter *fc-reg* (create-scanner "^([\\w/\\.]+)\\[(\\w+)\\]"))
(defparameter *dobj-da-reg* (create-scanner "^(\\w+/\\w+\\.\\w+)\\.([\\w]+)"))

(defun client-list-dataset-fcdas-as-list (client dataset-id)
  (declare (type string dataset-id))
  (with-foreign-object (err 'iedclienterror)
    (let ((dir (iedconnection-getdatasetdirectory client err dataset-id (null-pointer))))
      (if (eq :ied-error-ok (client-get-error err))
          (mapcar #'(lambda (row) (multiple-value-bind (total vec) (scan-to-strings *fc-reg* row)
                                    (declare (ignore total))
                                    (multiple-value-bind (total2 vec2) (scan-to-strings *dobj-da-reg* (aref vec 0))
                                      (if total2
                                          (list (aref vec2 0) (aref vec2 1) (aref vec 1))
                                          (list (aref vec 0) nil (aref vec 1))))))
                  (linkedlist-string-parse dir))
          (client-get-error err)))))

(defun client-list-dataset-fcdas (client dataset-id)
  (declare (type string dataset-id))
  (with-foreign-object (err 'iedclienterror)
    (let ((dir (iedconnection-getdatasetdirectory client err dataset-id (null-pointer))))
      (if (eq :ied-error-ok (client-get-error err))
          (mapcar #'(lambda (row) (multiple-value-bind (total vec) (scan-to-strings *fc-reg* row)
                                    (declare (ignore total))
                                    vec))
                  (linkedlist-string-parse dir))
          (client-get-error err)))))


(defun client-get-var-spec (client add-without-fc fc-str)
  (declare (type string add-without-fc fc-str))
  (with-foreign-object (err 'iedclienterror)
    (let* ((fco (functionalconstraint-fromstring fc-str))
           (var (iedconnection-getvariablespecification client err add-without-fc fco)))
      (if (eq :ied-error-ok (client-get-error err))
          var
          nil))))

(defun client-get-logical-devices (client)
  (with-foreign-object (err 'iedclienterror)
    (let ((dir (iedconnection-getserverdirectory client err nil)))
      (if (eq :ied-error-ok (client-get-error err))
          (linkedlist-string-parse dir)
          (client-get-error err)))))

(defun client-get-logical-device-content (client ld)
  (with-foreign-object (err 'iedclienterror)
    (let ((dir (iedconnection-getlogicaldevicedirectory client err ld)))
      (if (eq :ied-error-ok (client-get-error err))
          (linkedlist-string-parse dir)
          (client-get-error err)))))

(defun client-get-ln-dataset (client ln-ref)
  (with-foreign-object (err 'iedclienterror)
    (let ((dir (iedconnection-getlogicalnodedirectory client err ln-ref :acsi-class-data-set)))
      (if (eq :ied-error-ok (client-get-error err))
          (linkedlist-string-parse dir)
          (client-get-error err)))))

(defun client-get-ln-dobjs (client ln-ref)
  (with-foreign-object (err 'iedclienterror)
    (let ((dir (iedconnection-getlogicalnodedirectory client err ln-ref :acsi-class-data-object)))
      (if (eq :ied-error-ok (client-get-error err))
          (linkedlist-string-parse dir)
          (client-get-error err)))))

(defun client-get-dobj-das (client do-ref)
  (with-foreign-object (err 'iedclienterror)
    (let ((dir (lispiec-internal::iedconnection-getdatadirectoryfc client err do-ref)))
      (if (eq :ied-error-ok (client-get-error err))
          (linkedlist-string-parse dir)
          (client-get-error err)))))

(defun client-get-da-fc (client da-ref)
  (let* ((reg-result (multiple-value-bind (total vec) (scan-to-strings *dobj-da-reg* da-ref)
                       (declare (ignore total))
                       vec))
         (da-upstream (aref reg-result 0))
         (da-downstream (aref reg-result 1)))
    (with-foreign-object (err 'iedclienterror)
      (let ((dir (lispiec-internal::iedconnection-getdatadirectoryfc client err da-upstream)))
        (if (eq :ied-error-ok (client-get-error err))
            (let ((my-atts (mapcar #'(lambda (row)
                                       (multiple-value-bind (total vec)
                                           (scan-to-strings *fc-reg* row)
                                         (declare (ignore total))
                                         (cons (aref vec 0) (aref vec 1))))
                                   (linkedlist-string-parse dir))))
              (cdr (find da-downstream my-atts :test #'string= :key #'car)))
            (client-get-error err))))))

(defun client-get-ln-goose (client ln-ref)
  (with-foreign-object (err 'iedclienterror)
    (let ((dir (iedconnection-getlogicalnodedirectory client err ln-ref :acsi-class-gocb)))
      (if (eq :ied-error-ok (client-get-error err))
          (linkedlist-string-parse dir)
          (client-get-error err)))))


(defun client-find-logical-device-from-str (client ld-str)
  (let ((lst (client-get-logical-devices client)))
    (find-if #'(lambda (row) (search ld-str row)) lst)))

(defun mmsvariablespecification-collect-str-recursively (self &optional (lst '()) (basename ""))
  (let ((selfname (regex-replace
                   "\\.$"
                   (concatenate 'string basename "." (mmsvariablespecification-getname self))
                   "")))
    (if (or (eq (mmsvariablespecification-gettype self) :mms-array)
            (eq (mmsvariablespecification-gettype self) :mms-structure))
        (iter (for i from 0 below (mmsvariablespecification-getsize self))
          (setf lst (mmsvariablespecification-collect-str-recursively
                     (mmsvariablespecification-getchildspecificationbyindex self i)
                     lst selfname)))
        (setf lst (push selfname lst))))
  lst)

(defun flatten (x)
  (labels ((rec (x acc)
             (cond ((null x) acc)
                   ((atom x) (cons x acc))
                   (t (rec (car x) (rec (cdr x) acc))))))
    (rec x nil)))

(defun client-collect-var-spec-str-recursively (client add-without-fc fc-str &optional (lst '()))
  (declare (type string add-without-fc fc-str))
  (let ((var (client-get-var-spec client add-without-fc fc-str)))
    (when var
      (reverse (mmsvariablespecification-collect-str-recursively var lst add-without-fc)))))

(defun client-collect-var-spec-str-from-dataset (client dataset-id)
  (declare (type string dataset-id))
  (let ((fcdas (client-list-dataset-fcdas client dataset-id)))
    (mapcar #'(lambda (row) (client-collect-var-spec-str-recursively client (aref row 0) (aref row 1))) fcdas)))

(defun client-read-ds (client dataset-id &optional (read-dataset-obj (null-pointer)))
  (with-foreign-object (err 'iedclienterror)
    (let ((lst (iedconnection-readdatasetvalues client err dataset-id read-dataset-obj)))
      (if (eq :ied-error-ok (client-get-error err))
          lst
          nil))))

(defun mms-listify (mms-values &optional (lst '()))
  (if (or (eq :mms-array (mmsvalue-gettype mms-values))
          (eq :mms-structure (mmsvalue-gettype mms-values)))
      (let ((num (mmsvalue-getarraysize mms-values)))
        (iter (for i from 0 below num)
          (let ((item (mmsvalue-getelement mms-values i)))
            (setf lst (mms-listify item lst)))))
      (setf lst (push mms-values lst)))
  lst)

(defun mms-get-index (mms-values index)
  (if (or (eq :mms-array (mmsvalue-gettype mms-values))
          (eq :mms-structure (mmsvalue-gettype mms-values)))
      (mmsvalue-getelement mms-values index)))

(defun mms-compare-indexes (mms-values mms-values-ref)
  (if (or (eq :mms-array (mmsvalue-gettype mms-values))
          (eq :mms-structure (mmsvalue-gettype mms-values)))
      (let ((num (mmsvalue-getarraysize mms-values)))
        (iter (for i from 0 below num)
          (let ((item (mmsvalue-getelement mms-values i))
                (item-ref (mmsvalue-getelement mms-values-ref i)))
            (collect (mmsvalue-equals item item-ref)))))))

(defun dataset-listify (datasetvalues)
  (when (eq :mms-array (mmsvalue-gettype datasetvalues))
    (let ((num (mmsvalue-getarraysize datasetvalues)))
      (iter (for i from 0 below num)
        (let ((item (mmsvalue-getelement datasetvalues i)))
          (collect (reverse (mms-listify item))))))))

(defun client-create-goose-reciever (interfaceid)
  (let ((rec (goosereceiver-create)))
    (goosereceiver-setinterfaceid rec interfaceid)
    rec))

(defun client-create-goose-subscriber (site gocbref clientdataset-subscriber clientdataset-copie)
  (let ((subsc (goosesubscriber-create gocbref (clientdataset-getvalues clientdataset-subscriber))))
    (goosesubscriber-setlistener subsc (goose-handler site) (clientdataset-getvalues clientdataset-copie))
    subsc))

(defun client-goose-set-listener (subscriber listener)
  (goosesubscriber-setlistener subscriber listener (null-pointer)))

(defun client-goose-receiver-add-subs (goose-receiver goose-subscriber)
  (goosereceiver-addsubscriber goose-receiver goose-subscriber))

(defun client-goose-subscriber-set-dst-mac (subscriber add0 add1 add2 add3 add4 add5)
  (with-foreign-object (mac :uint8 6)
    (setf (mem-aref mac :uint8 0) add0)
    (setf (mem-aref mac :uint8 1) add1)
    (setf (mem-aref mac :uint8 2) add2)
    (setf (mem-aref mac :uint8 3) add3)
    (setf (mem-aref mac :uint8 4) add4)
    (setf (mem-aref mac :uint8 5) add5)
    (goosesubscriber-setdstmac subscriber mac))
  (client-goose-subscriber-get-dst-mac subscriber))

(defun client-goose-subscriber-get-dst-mac (subscriber)
  (with-foreign-object (mac :uint8 6)
    (goosesubscriber-getdstmac subscriber mac)
    (foreign-array-to-lisp mac '(:array :uint8 6))))

(defun client-goose-subscriber-set-appid (subscriber appid)
  (goosesubscriber-setappid subscriber (translate-to-foreign appid :uint16))
  (goosesubscriber-getappid subscriber))

(defun client-goose-is-running? (reciever)
  (goosereceiver-isrunning reciever))

(defun client-goose-start-listening (reciever)
  (goosereceiver-start reciever)
  (client-goose-is-running? reciever))

(defun client-goose-stop-listening (reciever)
  (when (and reciever (client-goose-is-running? reciever))
    (goosereceiver-stop reciever)
    (client-goose-is-running? reciever)))

(defun client-goose-subscriber-destroy (subscriber)
  (goosesubscriber-destroy subscriber))

(defun client-goose-stop-and-destroy (reciever)
  (client-goose-stop-listening reciever)
  (goosereceiver-destroy reciever)) ;destroy subscribers

(defun client-set-value-with-mmsvalue (client ref fc-str mmsvalue-ptr)
  (with-foreign-object (err 'iedclienterror)
    (let ((fc (functionalconstraint-fromstring fc-str)))
      (lispiec-internal::iedconnection-writeobject client err ref fc mmsvalue-ptr)
      (lispiec-internal::mmsvalue-delete mmsvalue-ptr)
      (client-get-error err))))

(define-condition iec-server-client-connection-nil (error)
  ((iec-server
    :initarg :iec-server
    :initform nil
    :reader iec-server))
  (:documentation "Custom error when iec-61850 connection is not yet created on server")
  (:report (lambda (condition stream)
             (format stream
                     "iec-61850 connection is not yet created on server ~a"
                     (iec-server-get-name (iec-server condition))))))

(defmacro make-read-value (fc-str)
  `(with-slots (obj-reference-str iec-server) obj
     (let ((my-client (client iec-server))
           (ref (concatenate 'string obj-reference-str "." da-str)))
       (if my-client
           (with-foreign-object (err 'iedclienterror)
             (let* ((mms (lispiec-internal::iedconnection-readobject my-client err ref (functionalconstraint-fromstring ,fc-str)))
                    (my-error (client-get-error err))
                    (my-value (mms-to-value mms)))
               (if (eql my-error :ied-error-ok)
                   (progn
                     (lispiec-internal::mmsvalue-delete mms)
                     my-value)
                   my-error)))
           (error (make-condition 'iec-server-client-connection-nil :iec-server iec-server))))))
