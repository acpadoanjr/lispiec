(in-package #:lispiec-callbacks)


(defun look-for-do (str str-obj)
  (search str str-obj))


(defmacro make-callback-basic-perform (sym obj isloc)
  `(defcallback ,sym checkhandlerresult
       ((action :pointer) (parameter :pointer)
        (value :pointer) (test :boolean) (interlockcheck :bool))
     (declare (ignore test value))
     (let ((origin (translate-from-foreign (controlaction-getorcat action) :int))
           (allowed-orcat (lispiec::orcat (lispiec:iec-server ,obj))))
       (if (or ,isloc (eq origin allowed-orcat))
           (progn (if (lispiec::is-in-operation ,obj)
                      (progn (controlaction-setaddcause action :add-cause-command-already-in-execution)
                             (controlaction-seterror action :control-error-no_error)
                             :control-denied)
                      (if (and interlockcheck (lispiec::cdc-is-locked ,obj))
                          (progn (controlaction-setaddcause action :add-cause-blocked-by-interlocking)
                                 (controlaction-seterror action :control-error-no_error)
                                 :control-denied)
                          :control-ok)))
           (progn (controlaction-setaddcause action :add-cause-blocked-by-switching-hierarchy)
                  (controlaction-seterror action :control-error-no_error)
                  :control-denied)))))



(defmacro make-callback-check (obj event-loop &optional (isloc nil))
  (let ((sym1 (gensym "callback"))
        (sym2 (gensym "callback")))
    `(progn (defcallback ,sym1 controlhandlerresult
                ((action :pointer) (parameter :pointer)
                 (ctlval :pointer) (test :boolean))
              (declare (ignore parameter test))
              (if (lispiec::is-in-operation ,obj)
                  (if (lispiec::cdc-compare-output-value ,obj)
                      (progn
                        (setf (lispiec::is-in-operation ,obj) nil)
                        :control-result-ok)
                      (let ((deltatime (- (get-universal-time) (lispiec::operation-accepted-timestamp ,obj))))
                        (if (> deltatime (lispiec::operation-timeout-sec ,obj))
                            (progn
                              (setf (lispiec::is-in-operation ,obj) nil)
                              (controlaction-setaddcause action :add-cause-time-limit-over)
                              (controlaction-seterror action :control-error-no_error)
                              :control-result-failed)
                            :control-result-waiting)))
                  (if (lispiec:cdc-compare-ctlval ,obj ctlval)
                      (progn (controlaction-setaddcause action :add-cause-position-reached)
                             (controlaction-seterror action :control-error-no_error)
                             (setf (lispiec::is-in-operation ,obj) nil)
                             :control-result-failed)
                      (progn
                        (lispiec::cdc-set-output-ctlval ,obj ctlval)
                        (setf (lispiec::is-in-operation ,obj) t)
                        (setf (lispiec::operation-accepted-timestamp ,obj) (get-universal-time))
                        (deeds:do-issue deeds::wants-oper-event :payload ,obj :loop ,event-loop)
                        :control-result-waiting))))
            (make-callback-basic-perform ,sym2 ,obj ,isloc)
            (setf (lispiec::handler ,obj) (callback ,sym1))
            (setf (lispiec::perf-handler ,obj) (callback ,sym2)))))


(defmacro make-callback-sub (ied event-loop)
  (let ((sym1 (gensym "callback")))
    `(progn (defcallback ,sym1 mmsdataaccesserror ((dat :pointer) (value :pointer) (client :pointer) (param :pointer))
              (declare (ignore client))
              (let ((ref (lispiec::get-string-from-pointer param)))
                (iedserver-updatebooleanattributevalue (lispiec:server ,ied) dat value)
                (deeds:do-issue deeds::substitution :message ref :loop ,event-loop)
                :data-access-error-success))
            (setf (lispiec::sub-handler ,ied) (callback ,sym1)))))

(defmacro make-callback-goose (site event-loop)
  (let ((sym1 (gensym "callback-goose")))
    `(progn (defcallback ,sym1 :void ((subscriber goosesubscriber) (param :pointer))
              (let ((snum (goosesubscriber-getsqnum subscriber))
                    (val (goosesubscriber-isvalid subscriber)))
                (when (and val (oddp snum))
                  (let ((mmsvls (goosesubscriber-getdatasetvalues subscriber))
                        (dsname (goosesubscriber-getdataset subscriber)))
                    (unless (mmsvalue-equals mmsvls param)
                      (mmsvalue-update param mmsvls)
                      (deeds:do-issue deeds::goose-received :message dsname :loop ,event-loop))))))
            (setf (lispiec::goose-handler ,site) (callback ,sym1)))))

(defmacro set-signal-handler (signo &body body)
  (let ((handler (gensym "sighandler")))
    `(progn (cffi:defcallback ,handler :void ((signo :int))
              (declare (ignore signo))
              ,@body)
            (foreign-funcall "signal" :int ,signo :pointer (cffi:callback ,handler)))))

(defmacro make-active-sg-changed (setting-group event-loop)
  (let ((sym1 (gensym "callback-sg")))
    `(progn (defcallback ,sym1 :bool ((parameter :pointer)
                                      (sgcb :pointer)
                                      (newactsg :uint8)
                                      (connection :pointer))
              (declare (ignore parameter sgcb connection))
              (deeds:do-issue deeds::sgcb-received
                :payload (cons ,setting-group (cffi:convert-from-foreign newactsg :uint8))
                :loop ,event-loop)
              (translate-to-foreign t :boolean))
            (setf (lispiec::setting-group-handler ,setting-group) (cffi:callback ,sym1)))))

(defmacro make-edit-sg-changed (setting-group event-loop)
  (let ((sym1 (gensym "callback-sg")))
    `(progn (defcallback ,sym1 :bool ((parameter :pointer)
                                      (sgcb :pointer)
                                      (neweditsg :uint8)
                                      (connection :pointer))
              (declare (ignore parameter sgcb connection))
              (deeds:do-issue deeds::sgcb-edit-changed
                :payload (cons ,setting-group (cffi:convert-from-foreign neweditsg :uint8))
                :loop ,event-loop)
              (translate-to-foreign t :boolean))
            (setf (lispiec::setting-group-handler-edit ,setting-group) (cffi:callback ,sym1)))))

(defmacro make-edit-sg-confirmation (setting-group event-loop)
  (let ((sym1 (gensym "callback-sg")))
    `(progn (defcallback ,sym1 :bool ((parameter :pointer)
                                      (sgcb :pointer)
                                      (editsg :uint8))
              (declare (ignore parameter sgcb))
              (deeds:do-issue deeds::sgcb-edit-confirmation
                :payload (cons ,setting-group (cffi:convert-from-foreign editsg :uint8))
                :loop ,event-loop)
              (translate-to-foreign t :boolean))
            (setf (lispiec::setting-group-handler-edit-confirm ,setting-group) (cffi:callback ,sym1)))))

(defstruct termination-client-info ref addcause timestamp)
(defmacro make-callback-oper-termination-client (site event-loop)
  (let ((sym1 (gensym "callback-oper-term"))
        (sym2 (gensym "callback-oper-term")))
    `(progn (defcallback ,sym1 :void ((invokeid :uint32)
                                      (parameter :pointer)
                                      (err iedclienterror)
                                      (type lispiec-internal::controlactiontype)
                                      (success :boolean))
              (deeds:do-issue deeds::oper-termination
                :loop ,event-loop
                :payload (make-termination-client-info
                          :ref (lispiec-internal::controlobjectclient-getobjectreference parameter)
                          :addcause (lispiec-internal::controlobjectclient-getlastapplerror parameter)
                          :timestamp (lispiec-internal::hal-gettimeinms))))
            (defcallback ,sym2 :void ((parameter :pointer)
                                      (controlobjectclient :pointer))
              (deeds:do-issue deeds::oper-termination
                :loop ,event-loop
                :payload (make-termination-client-info
                          :ref (lispiec-internal::controlobjectclient-getobjectreference controlobjectclient)
                          :addcause (lispiec-internal::controlobjectclient-getlastapplerror controlobjectclient)
                          :timestamp (lispiec-internal::hal-gettimeinms))))
            (setf (lispiec::oper-term-handler ,site) (cffi:callback ,sym1))
            (setf (lispiec::oper-term-handler2 ,site) (cffi:callback ,sym2)))))
