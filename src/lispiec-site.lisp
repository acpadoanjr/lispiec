(in-package #:lispiec)

;;;;;;;;;;
;; Site ;;
;;;;;;;;;;


(defclass site (iec-container)
  ((slynk-servers
    :accessor slynk-servers
    :initform '()
    :documentation
    "This is a lisp server useful to connect to the simulator remotely and
interact with it.")
   (name
    :accessor name
    :initform "site"
    :documentation "Name of the site to be simulated")
   (event-loop
    :accessor event-loop
    :initform (deeds:start (make-instance 'deeds:compiled-event-loop))
    :documentation "This is main event loop to be used by all site (deeds event library).")
   (goose-handler
    :accessor goose-handler
    :initform nil
    :documentation "Handler to be used with goose subscription. Common to all site.")
   (oper-term-handler
    :accessor oper-term-handler
    :initform nil
    :documentation "Handler to be used to oper term handler. Common to all site")
   (oper-term-handler2
    :accessor oper-term-handler2
    :initform nil
    :documentation "Handler to be used to oper term handler. Common to all site")
   (handler-locker
    :accessor handler-locker
    :initform (make-lock "handler-lock")
    :documentation "Locker to be used in the scope the site.")
   (conf-path
    :reader conf-path
    :initarg :path
    :initform nil)
   (goose-recs
    :initform nil
    :reader goose-recs
    :documentation "This is a list of all Goose receiviers affectet to this site. Usually
we have one goose receiver by network interface being used to
listening to goose. Only used if we want to listeng to goose as
subscribers. The network interface is used for all site.")
   (substation-xml
    :initform nil
    :reader substation-xml
    :documentation "This is a parsed xml instanced of the substation section found in scd
file being used. Useful to find scd information in runtime."))
  (:documentation "This class represents a container of all iec-servers belonging to
site. Usually all ieds found in a SCD file. It provides also means to
manage these iec-servers together and provides means to address issues
on the level of a site."))

;; start and stop servers on multiple ports as needed.
(defmethod site-slynk-start-server ((site site) port)
  (unless (member port (slynk-servers site))
    (progn
      (setf slynk:*use-dedicated-output-stream* nil)
      (slynk:create-server :port port :dont-close t)
                          (push port (slynk-servers site)))))

(defun make-site (name cfg-path start-port &optional (ds-regex ""))
  "Constructor of a site instance. It initializes most slots and handlers."
  (declare (type string name cfg-path)
           (type integer start-port))
  (let ((inst (make-instance 'site :path cfg-path)))
    (setf (name inst) name)
    (define-site-operate-handler inst)
    ;;(define-default-dobj-changed inst)
    (define-substitution-handler inst)
    (make-callback-goose inst (event-loop inst))
    (site-slynk-start-server inst start-port)
    (site-add-ieds-from-dir inst cfg-path start-port ds-regex)
    (site-make-routing-script inst (merge-pathnames "route.sh" cfg-path))
    (site-make-docker-script inst (merge-pathnames "spacio_docker.sh" cfg-path))
    (site-make-ip-script inst (merge-pathnames "spacio_ip.sh" cfg-path))
    (with-site-define-goose-client-handler inst goose-super-check)
    (make-callback-oper-termination-client inst (event-loop inst))
    (with-site-define-oper-termination inst oper-term)
    inst))

(defun make-site-config-and-quit (cfg-path start-port &optional (ds-regex "(GSE|DQCE|DQPO|CYPO|DQGW)$"))
  "Constructor of a site instance. It initializes most slots and
handlers, but it is supposed to be used only to create configuration
files when a new scd is used. after creating configuration files, it
stops and exit."
  (declare (type string cfg-path)
           (type integer start-port))
  (let ((inst (make-instance 'site :path cfg-path)))
    (site-add-ieds-from-dir inst cfg-path start-port ds-regex)
    (site-make-routing-script inst (merge-pathnames "route.sh" cfg-path))
    (site-make-docker-script inst (merge-pathnames "spacio_docker.sh" cfg-path))
    (site-make-ip-script inst (merge-pathnames "spacio_ip.sh" cfg-path))
    (uiop:quit)))

(defmethod site-load-substation ((site site))
  (with-slots (substation-xml conf-path) site
    (setf substation-xml
          (stp:find-child t (cxml:parse
                             (merge-pathnames "substation.xml" conf-path)
                             (stp:make-builder))
                          :key (stp:of-name "SCL" sclparser::scl-uri)))
    t))

(defmethod site-find-bay-longlabel ((site site) bay-name)
  (with-slots (substation-xml) site
    (unless substation-xml
      (site-load-substation site))
    (let ((bay (stp:filter-recursively (sclparser::of-el-name-and-att-value
                                        "Bay"
                                        "name"
                                        (substitute #\. #\_ bay-name)
                                        sclparser::scl-uri)
                                       substation-xml)))
      (stp:with-attributes ((l "MainLongLabel"))
                           (stp:nth-child
                            1
                            (stp:find-child-if (sclparser::of-el-name-and-att-value
                                                "Private"
                                                "type"
                                                "COMPAS-Bay"
                                                sclparser::scl-uri)
                                               (first bay)))
        l))))

(defmethod site-slynk-stop-server ((site site) &optional port) ()
  (if port
      (progn (slynk:stop-server port)
             (setf (slynk-servers site) (remove port (slynk-servers site))))
      (let ((p (pop (slynk-servers site))))
        (if p
            (progn (slynk:stop-server p))))))

(defvar *ips-regex* (create-scanner "name=\"(\\w+)\""))

(defun unflat-ips (listfile)
  (let ((ip nil)
        (name nil)
        (lsp '()))
    (iter (for i in listfile)
      (if (null ip)
          (setf ip i)
          (progn (setf name (multiple-value-bind (total vec)
                                (scan-to-strings *ips-regex* i)
                              (declare (ignore total)) vec))
                 (push (cons (aref name 0) ip) lsp)
                 (setf ip nil))))
    lsp))


(defun get-ied-ip-from-list (iedname lst)
  (cdr (assoc iedname lst :test #'string=)))


(defvar *ip-regex* (create-scanner "(\\w+)\\.(\\w+)\\.(\\w+)\\.(\\w+)"))
(defun site-add-ied (site-container cfgfile init-tcp-port ip-address enum)
  "This function populates iec-servers in a site container. It is used
mainly while instantiation a site"
  (let* ((ied (ignore-errors (make-iec-server cfgfile)))
         (iedname (when ied (iedmodel-getiedname (model ied)))))
    (if ied
        (progn
          (iec-container-add site-container iedname ied)
          (setf (parent-site ied) site-container)
          (make-callback-sub ied (event-loop site-container))
          (iec-server-set-ip-address ied ip-address)
          (with-slots (enumtypes) ied (setf enumtypes enum))
          (setf (tcp-port ied)
                (find-available-port site-container init-tcp-port ip-address)
                ;; (ppcre:register-groups-bind (s1 s2 s3 s4) (*ip-regex* ip-address)
                ;;   (+  (* (parse-integer s3) 256)
                ;;       (parse-integer s4)
                ;;       init-;TODO: cp-port))
                ))
        (warn "Warning: configuration file error on ~a~%" cfgfile))
    ied))


(defun find-available-port (site-container init-tcp-port ip-address &optional (delta 0))
  "Do the best to find an available port trying first to use init-tcp-port + s3*256 + s4"
  (declare (type (string) ip-address))
  (let ((ports (mapcar #'cdr (site-list-ieds-port site-container)))
        (possible-port (+ delta (ppcre:register-groups-bind (s1 s2 s3 s4) (*ip-regex* ip-address)
                                  (declare (ignore s1 s2))
                                  (+  (* (parse-integer s3) 256)
                                      (parse-integer s4)
                                      init-tcp-port)))))
    (if (find possible-port ports)
        (find-available-port site-container init-tcp-port ip-address (incf delta))
        possible-port)))

(defun site-start-ieds (site-container)
  "Start all enabled ieds and make useful shell scripts to set a route
table to map ieds simulated to the ips found in scd file."
  (mapcar #'(lambda (row)
              (iec-server-start row nil :random-wait-start t :init-dataobjects t))
          (mapcar #'cdr (objs site-container)))
  (site-make-routing-script site-container
                            (merge-pathnames "route.sh"
                                             (conf-path site-container)))
  (site-make-ip-script site-container
                       (merge-pathnames "spacio_ip.sh"
                                        (conf-path site-container)))
  (site-make-docker-script site-container
                           (merge-pathnames "spacio_docker.sh"
                                            (conf-path site-container)))
  (site-list-ieds-running site-container))

(defun site-make-subs (site-container)
  "Function probabely not used anymore"
  (let ((ieds (mapcar #'cdr (objs site-container))))
    (iter:iter (iter:for ied iter:in ieds)
      (let ((dos (iec-server-get-dataobjects-of-cdc ied 'cdc-sub)))
        (iter:iter (iter:for doj iter:in dos)
          (when (> (length (alias (cdr doj))) 1))
          (make-subs (cdr doj)))))))

(defun site-restart-ieds (site-container &optional sleeptime)
  (mapcar #'(lambda (ied)  (if sleeptime
                               (iec-server-restart ied sleeptime)
                               (iec-server-restart ied)))
          (mapcar #'cdr (objs site-container)))
  (site-list-ieds-running site-container))


(defun site-stop-ieds (site-container)
  (when site-container
    (mapcar #'iec-server-stop (mapcar #'cdr (objs site-container)))
    (site-list-ieds-running site-container)))

(defun site-get-ieds-enabled (site)
  (let ((ieds (site-get-ieds site)))
    (remove nil ieds :key #'enable)))

(defun site-get-ieds-disabled (site)
  (let ((ieds (site-get-ieds site)))
    (remove t ieds :key #'enable)))

(defun site-get-ieds-name-enabled (site)
  (let ((ieds (site-get-ieds-enabled site)))
    (mapcar #'iec-server-get-name ieds)))

(defun site-get-ieds-name-disabled (site)
  (let ((ieds (site-get-ieds-disabled site)))
    (mapcar #'iec-server-get-name ieds)))

(defun site-client-get-connection-state (site-container)
  (mapcar #'iec-server-client-get-connection-state (mapcar #'cdr (objs site-container))))

(defun site-client-stop-ieds (site-container)
  (mapcar #'iec-server-client-close-connection (mapcar #'cdr (objs site-container)))
  (site-client-get-connection-state site-container))

(defun site-get-ieds-name (site-container)
  (iec-container-list-names site-container))

(defun site-start-ieds-goose (site-container interface)
  "Start goose publishing in all enabled ieds in a site."
  (mapcar #'(lambda (row)
              (iec-server-start-goose-on-interface row interface :random-wait-start t))
          (mapcar #'cdr (objs site-container)))
  (site-list-ieds-running site-container))

(defun site-stop-ieds-goose (site-container)
  "Stop goose publishing in all enabled ieds in a site."
  (mapcar #'(lambda (row)
              (iec-server-stop-goose-on-interface row))
          (mapcar #'cdr (objs site-container)))
  (site-list-ieds-running site-container))

(defun site-get-ieds (site-container)
  (iec-container-list-objs site-container))

(defun site-change-orcat-local (site-container)
  (mapcar #'iec-server-change-orcat-local (mapcar #'cdr (objs site-container))))

(defun site-change-orcat-remote (site-container)
  (mapcar #'iec-server-change-orcat-remote (mapcar #'cdr (objs site-container))))

(defun site-destroy (site-container)
  "Try to gracefully destroy a site instance releasing all c memory
allocated by libiec61850."
  (site-clear-all-handlers site-container)
  (when (site-client-goose-receivers-are-running site-container)
    (mapcar #'site-client-goose-reciever-stop-destroy (goose-recs site-container)))
  (mapcar #'destroy-iec-server (mapcar #'cdr (objs site-container)))
  (setf (objs site-container) nil)
  (site-slynk-stop-server site-container))

(defun site-list-ieds-port (site)
  (reverse (mapcar #'cons (iec-container-list-names site) (mapcar #'tcp-port (iec-container-list-objs site)))))

(defun site-list-ieds-ip (site &optional (only-enabled nil))
   (reverse
    (remove-if #'(lambda (r) (and only-enabled
                                  (not (enable (site-get-ied-by-name site (car r))))))
               (mapcar
                #'cons
                (iec-container-list-names site)
                (mapcar #'ip-address (iec-container-list-objs site))))))

(defun site-list-ieds-running (site)
  (reverse (mapcar #'cons (iec-container-list-names site) (mapcar #'iec-server-is-running-str (iec-container-list-objs site)))))


(defun is-site-running (site)
  (every #'identity (mapcar #'iec-server-is-running (site-get-ieds-enabled site))))

(defun site-get-ied-by-index (site indx)
  (iec-container-get-index site indx))

(defun site-get-ied-by-name (site name)
  (let ((my-name (find-ied-fullname site name)))
    (if my-name
        (iec-container-get site my-name)
        nil)))

(defmethod site-is-goose-supervision-enabled ((site site))
  (let ((ieds (iec-container-list-objs site)))
    (not (every #'not (mapcar #'supervised ieds)))))

(defmethod site-start-client-goose-cycle ((site site))
  (bt:make-thread (lambda ()
                    (loop while (site-is-goose-supervision-enabled site) do
                      (progn
                        (deeds:do-issue deeds::goose-client-cycle
                          :message "new cycle"
                          :loop (event-loop site))
                        (sleep 60))))
                  :name "goose-cycle-supervision"))


;; (defun site-get-ied-by-bay-and-name-regex (site bay name)
;;   (iec-container-get site name))

(defun find-ied-fullname (site partial-name)
  (find-if #'(lambda (r) (search partial-name r)) (site-get-ieds-name site)))

(defun find-ied-list-by-name (site partial-name)
  (let ((ied-list-str
          (remove-if-not #'(lambda (r) (search partial-name r)) (site-get-ieds-name site))))
    (mapcar #'(lambda (r) (site-get-ied-by-name site r)) ied-list-str)))


(defun site-get-dataobjects-by-regex (site ld-regex-str ln-regex-str do-regex-str)
  (let ((lst '()))
    (iter (for ied in (iec-container-list-objs site))
      (setf lst (nconc lst (iec-server-get-dataobjects-regex ied do-regex-str ln-regex-str ld-regex-str))))
    lst))

(defun site-get-dataobject-by-regex (site ld-regex-str ln-regex-str do-regex-str)
  (let ((lst '()))
    (iter (for ied in (iec-container-list-objs site))
      (setf lst (nconc lst (iec-server-get-dataobjects-regex ied do-regex-str ln-regex-str ld-regex-str))))
    (cdr (pop lst))))

(defun site-get-dataobject-by-ref (site ref)
  (let ((ld (ref-get-logical-device-str ref))
        (ieds (objs site)))
    (let ((myied (find-if #'(lambda (lied) (search (car lied) ld :test #'string=)) ieds)))
      (when myied
        (iec-server-get-dataobject-from-ref (cdr myied) ref)))))

(defun site-get-dataobjects-by-alias (site alias-regex-str)
  (let ((lst '()))
    (iter (for ied in (iec-container-list-objs site))
      (setf lst (nconc lst (iec-server-get-alias-regex ied alias-regex-str))))
                                        ;(list-dataobjects-get-bay-alias lst)
    lst))

(defun site-get-dataobject-by-alias (site alias-regex-str bay)
  (let ((lst (site-get-dataobjects-by-alias site alias-regex-str)))
    (cdr (find bay lst :key #'(lambda (r) (bay (cdr r))) :test 'string=))))

(defun site-get-dataobjects-of-cdc (site cdc-type)
  (let ((lst '()))
    (iter (for ied in (iec-container-list-objs site))
      (setf lst (nconc lst (iec-server-get-dataobjects-of-cdc ied cdc-type))))
    lst))

;; http://lispcookbook.github.io/cl-cookbook/files.html

(defun site-add-ieds-from-dir (site dir-path &optional (start-port 10000) (ds-regex ""))
  "Scan all cfg files from a directory and populate site with new
iec-servers. We can set slynk to a different listening port, not the standard
10000. This port is used to listen for lisp remote connections).

In addition we can specify a dataset regex that is used to scan
datasets in each iec-server matching it. These datasets will in
a second step be used to scan for data objects that are actually
instantiated. This argument only make sense if no configuration files
are yet provided (when a new scd file is given). Use it if you want be
precise on which data objects must be manipulated. Data objects not
instantiated will be visible on iec server but will not be treated by
spacio high-level functions."
  (let* ((cfgs (directory (merge-pathnames (uiop:native-namestring dir-path) "*.cfg")))
         (scd (probe-file
               (first (directory (merge-pathnames (uiop:native-namestring dir-path) "*.scd")))))
         (ips "0.0.0.0")
         (leng (cl:length cfgs))
         (enum nil))
    (when scd
      (sclparser::load-and-save-site-with-confs scd nil ds-regex))
    (iter (for i from 0 to (decf leng))
          (let* ((cfgf (namestring (nth i cfgs)))
                 (cdc-file (probe-file
                            (ppcre:regex-replace "cfg$" cfgf "lisp")))
                 (ip-file (probe-file
                           (ppcre:regex-replace "\\.cfg$" cfgf "-ip.lisp")))
                 (enum-file (probe-file
                             (ppcre:regex-replace "\\.cfg$" cfgf "-enums.lisp"))))
            (when ip-file
              (setf ips (cdr (sclparser::ied-ip-read-from-file ip-file))))
            (when enum-file
              (setf enum (sclparser::ied-enum-read-from-file enum-file)))
            (let ((iedser (site-add-ied site cfgf start-port ips enum)))
              (when (and cdc-file iedser)
                (make-cdcs-form-export-da-variables site cdc-file (iec-server-get-name iedser))))))))



(defun site-make-routing-script (site file-path)
"This function creates a routing nat script that can be used to map all
running iec-server's port to an ip address matching what is found on
scd file. This script is supposed to be launch in parallel to the
simulator."
  (with-open-file (str file-path :direction :output
                                 :if-exists :supersede
                                 :if-does-not-exist :create)
    (format str "#!/bin/sh~%")
    (let ((ips (mapcar #'cdr (site-list-ieds-ip site t))))
      (if (eql (length ips)
               (length (delete-duplicates ips :test #'string=)))
        (mapcar #'(lambda (ied)
                    (format
                     str
                     "~aiptables -t nat -A PREROUTING -p tcp --dport 102 -d ~a -j REDIRECT --to-ports ~d~%"
                     (if (enable ied) "" "#")
                     (ip-address ied)
                     (tcp-port ied))
                    (format
                     str
                     "~aiptables -t nat -A POSTROUTING -p tcp --sport ~d -j SNAT --to-source ~a:102~%"
                     (if (enable ied) "" "#")
                     (tcp-port ied)
                     (ip-address ied)))
                (iec-container-list-objs site))
        (format str "echo Attention : duplicate ips in script! ~%")
        ))
    ))

(defun site-execute-routing-script (site)
  (let ((my-file (merge-pathnames "route.sh"
                                  (conf-path site))))
    (sb-ext:run-program "/bin/sh" (list (namestring my-file)) :output uiop:*stdout*)))

(defun site-make-ip-script (site file-path)
  "This function assings ip address that can be used to map all
running iec-server's to a network interface. This script is supposed to
be launch in parallel to the simulator and together with the routing
script, if necessary. The name of the network interface is fixed to PRD."
  (with-open-file (str file-path :direction :output
                       :if-exists :supersede
                       :if-does-not-exist :create)
                  (format str "#!/bin/sh~%")
                  (let ((ips (mapcar #'cdr (site-list-ieds-ip site t))))
                    (if (eql (length ips)
                             (length (delete-duplicates ips :test #'string=)))
                        (progn
                          (mapcar #'(lambda (ied)
                                      (format
                                       str
                                       "~aip address add ~a dev PRD~%"
                                       (if (enable ied) "" "#")
                                       (ip-address ied)))
                                  (iec-container-list-objs site)))
                      (format str "echo Attention : duplicate ips in script! ~%")))))

(defun site-make-docker-script (site file-path)
  "If lispiec is containerized in a docker, this functions creater a
helper scrip to launch the simulator from cli."
  (with-open-file (str file-path :direction :output
                                 :if-exists :supersede
                                 :if-does-not-exist :create)
    (format str "#!/bin/sh~%")
    (let ((ips (mapcar #'cdr (site-list-ieds-ip site t))))
      (if (eql (length ips)
               (length (delete-duplicates ips :test #'string=)))
          (progn
            (format str "docker run --rm -ti -v /root:/root --cap-add sys_admin --cap-add net_admin \\~%")
            (format str "        --net=host --name spacio_container lispiec:latest /bin/spaciod -d /root/ieds/ --additional-script=/root/start_rspace.lisp"))
          (format str "echo Attention : duplicate ips in script! ~%")
          ))))
