(in-package #:lispiec)


;;;;;;;;;;;;
;; inputs ;;
;;;;;;;;;;;;

(defclass generic-input ()
  ((timestamp
    :reader timestamp
    :initarg :timestamp
    :initform nil
    :documentation "Desired timestamp in unix format. If nil, timestamp will be calculated
on each cdc-update (set to current time).")
   (validity
    :accessor validity
    :initarg :validity
    :initform t
    :documentation "Desired validity in true (ok) or false (alarm). This boolean is a
simplification of the true quality in IEC 61850. The objetive is to
help the manipulation of the quality.")
   (forced-q-detail
    :reader forced-q-detail
    :initform nil
    :documentation "If the simplified validity as a boolean is not sufficient, the user
can force the desired q-detail using a integer as input. It will be ignored if nil")
   (forced-q-test
    :reader forced-q-test
    :initform nil
    :documentation "If the simplified validity as a boolean is not sufficient, the user
can force the desired test bit using a boolean as input. It will be ignored if nil")
   (forced-q-sub
    :reader forced-q-sub
    :initform nil
    :documentation "If the simplified validity as a boolean is not sufficient, the user
can force the desired quality substitution bit using a boolean as input. It will be ignored if nil")
   (forced-op-bk
    :reader forced-op-bk
    :initform nil
    :documentation "If the simplified validity as a boolean is not sufficient, the user
can force the desired op-blk bit using a boolean as input. It will be ignored if nil")
   (forced-quality-int
    :reader forced-quality-int
    :initform nil
    :documentation "If the simplified validity as a boolean is not sufficient, the user
can force the desired entire quality using a integer as input. It will be ignored if nil"))
  (:documentation
   "Inputs are instatiated on every cdc depending on the CDC. They are
high-level input that can be used to set a desired status, timestamp
and quality, this before doing a cdc-update, when these inputs will be
actually considered in the calculation of IEC 61850 data
attributes. These data attributes are supposed to mirror the input
values after an update. Exeptions are when substitution and oper
services are received."))

(defgeneric input-set-valid (obj)
  (:documentation "set to valid"))

(defmethod input-set-valid ((obj generic-input))
  (setf (validity obj) t))

(defmethod input-set-invalid ((obj generic-input))
  (setf (validity obj) nil))

(defmethod input-set-timestamp ((obj generic-input) time)
  (when (and (integerp time) (> time -1))
    (setf (slot-value obj 'timestamp) time)))

(defmethod input-set-timestamp-now ((obj generic-input))
  (setf (slot-value obj 'timestamp) (translate-from-foreign (hal-gettimeinms) :uint64)))

(defmethod input-unforce ((obj generic-input))
  (with-slots (timestamp
               forced-q-detail
               forced-q-test
               forced-q-sub
               forced-op-bk
               forced-quality-int)
      obj
    (setf timestamp nil)
    (setf forced-q-detail nil)
    (setf forced-q-sub nil)
    (setf forced-q-test nil)
    (setf forced-op-bk nil)
    (setf forced-quality-int nil)))

(defclass generic-input-boolean (generic-input)
  ((input-value :accessor value :initarg :value :initform nil
                :documentation "The desired status as a boolean."))
  (:documentation "boolean input"))

(defmethod input-set-value-on ((obj generic-input-boolean))
  (setf (value obj) t))

(defmethod input-set-value-off ((obj generic-input-boolean))
  (setf (value obj) nil))

(defclass generic-input-int (generic-input)
  ((input-value :accessor value :initarg :value :initform 0
                :documentation "The desired status as an integer."))
  (:documentation "int input"))

(defclass generic-input-float (generic-input)
  ((input-value :accessor value :initarg :value :initform 0.0
                :documentation "The desired status as a float."))
  (:documentation "float input"))

(defclass generic-input-cfloat (generic-input)
  ((input-value :accessor value :initarg :value :initform 0.0
                :documentation "The desired status as a float.")
   (input-value-ang :accessor value-ang :initarg :value-ang :initform 0.0
                    :documentation "The desired status angle as a float."))
  (:documentation "complex float input"))


(defclass generic-input-dbpos (generic-input)
  ((input-value :accessor value :initarg :value :initform nil
                :documentation "The desired status as a enumeration."))
  (:documentation "dpc input"))

(defclass generic-input-string (generic-input)
  ((input-value :accessor value :initarg :value :initform ""
                :documentation "The desired status as a string."))
  (:documentation "string input"))

(defmethod input-set-value-on ((obj generic-input-dbpos))
  (setf (value obj) :dbpos-on))

(defmethod input-set-value-off ((obj generic-input-dbpos))
  (setf (value obj) :dbpos-off))


(defmethod input-set-value ((obj generic-input-int) int-value)
  (setf (value obj) int-value))

(defmethod input-set-value ((obj generic-input-dbpos) dbpos-value)
  (setf (value obj) dbpos-value))

(defmethod input-set-value ((obj generic-input-float) float-value)
  (setf (value obj) (float float-value)))

(defmethod input-set-value ((obj generic-input-string) string-value)
  (setf (value obj) (if (stringp string-value)
                        string-value
                        (write-to-string string-value))))

(defparameter angle-ratio (/ 180 pi))

(defun polar-to-complex (mag degre)
  (let ((ang (/ degre angle-ratio)))
    (let ((imag (* (sin ang) mag))
          (reap (* (cos ang) mag)))
      (complex (coerce reap 'single-float) (coerce imag 'single-float)))))

(defun complex-get-degre (cvalue)
  (float (* angle-ratio (phase cvalue)) 0.0))

(defmethod input-set-value ((obj generic-input-cfloat) complex-value)
  (setf (value obj) (abs complex-value))
  (setf (value-ang obj) (complex-get-degre complex-value)))
