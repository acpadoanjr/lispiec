(in-package #:lispiec)

;;;;;;;;;;;;;;;;
;; iec-server ;;
;;;;;;;;;;;;;;;;

(defclass iec-server ()
  ((model
    :accessor model
    :initarg :model
    :initform nil
    :documentation
    "This slot is used to store the libiec61850 ied model structure pointer.")
   (model-path-str
    :accessor model-path-str
    :initform nil)
   (server
    :accessor server
    :initarg :server
    :initform nil
    :documentation
    "This slot is used to store the libiec61850 iec-server pointer.")
   (mms-server
    :accessor mms-server
    :initarg :mms-server
    :initform nil)
   (model-lock
    :accessor model-lock
    :initarg :model-lock
    :initform nil
    :documentation
    "This is a high-level locker used to lock iec-server C model.")
   (ied-name-ptr
    :accessor ied-name-ptr
    :initform nil
    :initarg :ied-name-ptr)
   (tcp-port
    :accessor tcp-port
    :initform 10000)
   (ip-address
    :accessor ip-address
    :initform nil)
   (logical-device-container
    :accessor ld-container
    :initform nil)
   (orcat
    :accessor orcat
    :initform control_orcat_station_control
    :documentation
    "Oper services having this orcat level in origin will be accepted.")
   (parent-site
    :accessor parent-site
    :initform nil)
   (sub-handler :accessor sub-handler :initform nil)
   (enable
    :reader enable
    :initform t
    :documentation
    "Set this to false to be sure that this ied will never start and also
not be included in the creation of network configuration bash
scpripts. It means this ied will never be used as an IEC61850
server (mms or goose). This slot prevents that this happens by
accident.")
   (client
    :reader client
    :initform nil
    :documentation
    "This slot will hold an libiec61850 client object when requested. Otherwise it will be nil (none).")
   (client-ds
    :reader client-ds
    :initform '())
   (supervised
    :reader supervised
    :initform nil
    :documentation
    "This slot will return true if this ied is being used in mirroring
mode (to be explained yet).")
   (filebasepath
    :reader filebasepath
    :initform nil
    :documentation
    "Base directory to be used with mms get-file services. It is supposed
to have Comtrade files.")
   (goose-eth
    :reader goose-eth
    :initform nil
    :documentation
    "Returns the network card that is being used with goose.")
   (goose-sim
    :accessor goose-sim
    :initform nil
    :documentation
    "Returns true if goose will be publish with sim bit true in next goose start.")
   (inrefs
    :initform nil
    :documentation
    "This slot is supposed to host a list of all inrefs data objects (cdc org).")
   (enumtypes
    :reader enumtypes
    :initform nil)
   ))

(defun make-iec-server-from-model (model &optional ptr-name)
  "Constructor for iec-server having a libiec61850 model as argument. We
can change the name of the iec-server giving as input a C char* ptr-name."
  (let ((mconf (iedserverconfig-create)))
    (lispiec-internal::iedserverconfig-enableresvtmsforsgcb mconf t) ;; utiliser cela pour SGCB édition 1 (nil) out 2 (t)
    (lispiec-internal::iedserverconfig-setedition mconf 1) ;; for edition 2.0 use 1, for edition 2.1 use 2
    (let* ((lserver (lispiec-internal::iedserver-createwithconfig model (null-pointer) mconf))
           (mserver (iedserver-getmmsserver lserver))
           (inst (if (null ptr-name)
                     (make-instance 'iec-server
                                    :model model
                                    :server lserver
                                    :mms-server mserver
                                    :model-lock (make-lock "model-lock"))
                     (make-instance 'iec-server
                                    :model model
                                    :server lserver
                                    :mms-server mserver
                                    :model-lock (make-lock "model-lock")
                                    :ied-name-ptr ptr-name))))
      (iedserverconfig-destroy mconf)
      inst)))

(defun make-iec-server (model-path &optional iedname)
  "Constructor for iec-server having as argument the path to libiec61850
model file. We can change the name of the iec-server using the iedname
argument as a lisp string. This is a higher level constructor for
iec-server when comparing with make-iec-server-from-model"
  (when (probe-file model-path)
    (let ((model (configfileparser-createmodelfromconfigfileex model-path))
          (myptr nil))
      (when (pointer-eq model (null-pointer))
        (error "IED Model null...  code c will crash, better abort now!"))
      (unless (null iedname)
        (progn
          (setf myptr (foreign-alloc :string :initial-element iedname))
          (iedmodel-setiedname model (mem-ref myptr :pointer))))
      (let ((this-server (make-iec-server-from-model model myptr)))
        (setf (model-path-str this-server) model-path)
        this-server))))

(defgeneric iec-server-start (obj &optional port &key random-wait-start init-dataobjects)
  (:documentation
   "This function start mms server - start listening for connections. The
listening port can be set using port argument. In addition we can set
a random wait delay to actually start the server if randon-wait-start
argument is true. This is usefull if we want to start several
iec-servers at once but with a small random delay between them in order to
avoid that all cyclic reports be sent synchronized."))

(defmethod iec-server-start ((obj iec-server) &optional (port nil) &key (random-wait-start nil) (init-dataobjects t))
  (unless (null port)
    (setf (tcp-port obj) port))
  (when init-dataobjects (iec-server-init-dataobjects obj))
  (when (enable obj)
    (when random-wait-start
      (sleep (random 1.3)))
    (iedserver-start (server obj) (tcp-port obj))))

(defmethod iec-server-is-running ((obj iec-server))
  (translate-from-foreign (iedserver-isrunning (server obj)) :boolean))

(defmethod iec-server-is-running-str ((obj iec-server))
  (if (iec-server-is-running obj)
      "Running"
      "Stopped"))

(define-condition iec-server-disabled (error)
  ((iec-server
    :initarg :iec-server
    :initform nil
    :reader iec-server))
  (:documentation "Custom error when trying to manipulate a disabled ied.")
  (:report (lambda (condition stream)
             (format stream
                     "Server ~a is disabled! Rethink what are you trying achieve."
                     (iec-server-get-name (iec-server condition))))))

(define-condition cdc-warning-timestamps-the-same (warning)
  ((dobj
    :initarg :dobj
    :initform nil
    :reader dobj))
  (:documentation "Custom error when trying to manipulate a cdc using the same timestamp
as before. This should be avoided.")
  (:report (lambda (condition stream)
             (format stream
                     "Data Object ~a is being updated with same timestamp as before."
                     (obj-reference-str (dobj condition))))))

(defmethod iec-server-disable ((obj iec-server))
  (with-slots (enable) obj
    (unless (iec-server-is-running obj)
      (setf enable nil))))

(defmethod iec-server-enable ((obj iec-server))
  (with-slots (enable) obj
    (unless (iec-server-is-running obj)
      (setf enable t))))

(defmethod iec-server-get-name ((obj iec-server))
  (iedmodel-getiedname (model obj)))

(defmethod iec-server-stop ((obj iec-server))
  (when (enable obj)
    (iec-server-stop-goose-on-interface obj)
    (iedserver-stop (server obj))))

(defgeneric iec-server-restart (obj &optional sleeptime)
  (:documentation "restart the iec-server and can wait to start using sleeptime argument
as seconds."))

(defmethod iec-server-restart ((obj iec-server) &optional (sleeptime 35.0))
  (iec-server-set-invalid obj)
  (sleep 3)
  (with-lock-held ((model-lock obj))
    (when (iec-server-is-running obj)
      (iedserver-stop (server obj)))
    ;; (iedserver-destroy (server obj))
    ;; (let ((this-model (configfileparser-createmodelfromconfigfileex (model-path-str obj))))
    ;;   (setf (server obj) (iedserver-create this-model))
    ;;   (setf (model obj) this-model))
    (let ((thr (make-thread  #'(lambda ()
                                 (when sleeptime (sleep sleeptime))
                                 (iec-server-start obj (tcp-port obj))))))
      (join-thread thr)))
  (iec-server-set-valid obj)
  (iec-server-set-ctlmodel obj :control-model-direct-enhanced))


(defmethod iec-server-set-ip-address ((obj iec-server) local-ip-address)
  ;; not going to use this anymore : (iedserver-setlocalipaddress (server obj) local-ip-address)
  (setf (ip-address obj) local-ip-address))

(defgeneric iec-server-start-file-server (obj file-server-path)
  (:documentation
   "This method is necessary if we want to use mms file transfert
service. We need to set a local path were to store files to be shared."))

(defmethod iec-server-start-file-server ((obj iec-server) file-server-path)
  (let ((dir-path (uiop:native-namestring file-server-path)))
    (unless (iec-server-is-running obj) (iedserver-setfilestorebasepath (server obj) dir-path))))

(defgeneric iec-server-change-orcat-local (obj)
  (:documentation "This sets the server to only accept oper controls with orCat 2.")
  (:method ((obj iec-server))
    (setf (orcat obj) control_orcat_station_control)))

(defgeneric iec-server-change-orcat-remote (obj)
  (:documentation "This sets the server to only accept oper controls with orCat 3.")
  (:method ((obj iec-server))
    (setf (orcat obj) control_orcat_remote_control)))

(defmethod iec-server-set-invalid ((obj iec-server))
  (let ((lst (iec-server-get-dataobjects obj)))
    (list-dataobjects-set-invalid lst)))

(defmethod iec-server-set-valid ((obj iec-server))
  (let ((lst (iec-server-get-dataobjects obj)))
    (list-dataobjects-set-valid lst)))

(defgeneric iec-server-start-goose-on-interface (obj interface &key random-wait-start)
  (:documentation "Starts publishing Goose in network devive giving by interface
argument. In addition we can set a random wait delay to actually start
the goose publishig if randon-wait-start argument is true. This is
usefull if we want to start goose on several iec-servers at once but
with a small random delay between them in order to avoid that all
goose messages be sent synchronized."))

(defmethod iec-server-start-goose-on-interface ((obj iec-server) interface &key (random-wait-start nil))
  (with-slots (server goose-eth goose-sim) obj
    (declare (type string interface))
    (unless goose-eth
      (setf goose-eth interface)
      (iedserver-setgooseinterfaceid server interface))
    (when (enable obj)
      (when random-wait-start
        (sleep (random 1.0)))
      (iedserver-enablegoosepublishing server)
      (iec-server-set-goose-sim obj goose-sim))))

(defmethod iec-server-stop-goose-on-interface ((obj iec-server))
  (when (enable obj)
    (iedserver-disablegoosepublishing (server obj))))


(defgeneric iec-server-set-goose-sim (obj activation)
  (:documentation "Use activation true to make all goose messages to be published with
simulation bit true.")
  (:method ((obj iec-server) activation)
    (let* ((mapp (cffi:foreign-slot-value (server obj) '(:struct lispiec-internal::siedserver) 'lispiec-internal::mmsmapping))
           (ggs (cffi:foreign-slot-value mapp '(:struct lispiec-internal::mmsmapping) 'lispiec-internal::gsecontrols))
           (ggssize (lispiec-internal:linkedlist-size ggs))
           (ggss (mapcar #'(lambda (r) (lispiec-internal::linkedlist-get ggs r)) (alexandria:iota ggssize)))
           (ggsv (mapcar #'lispiec-internal::linkedlist-getdata ggss))
           (pubs (mapcar #'(lambda (r) (cffi:foreign-slot-value r 'lispiec-internal::mmsgoosecontolblock 'lispiec-internal::publisher)) ggsv)))
      (mapcar #'(lambda (r) (lispiec-internal::goosepublisher-setsimulation r activation)) pubs))))


(defgeneric iec-server-client-create-connection (obj &optional use-local-address force-ip force-port)
  (:documentation
   "This methods starts a libiec61850 client connection to an IEC61850
server. This client connection is supposed to be used to connect to
the server represented by this obj iec-server. There are some
use-cases imagined here:

1. This iec-server is disabled (not being simulated) and we want to
connect to the real ied. In this case we only need to use obj
argument. The ip address used for the connection will be infered from
ip-address slot.

2. We want to create a client connection to the ied being simulated,
in case the iec-server is enabled and running. This is useful to make
some tests or whatever. This can be easyly achieved usind
use-local-address to true. In this case the connection will be
achieved using localhost and port defined by slot tcp-port.

3. We want to connect to any iec-server we want. Even if it is an
abuse of this method's objective, we can force the connection using
arguments force-ip and force-port."))

(defmethod iec-server-client-create-connection ((obj iec-server) &optional
                                                            (use-local-address nil)
                                                            (force-ip nil)
                                                            (force-port nil))
  (with-slots (client server ip-address tcp-port) obj
    (let ((my-ip (if use-local-address "127.0.0.1"
                     (if force-ip force-ip ip-address)))
          (my-port (if use-local-address tcp-port
                       (if force-port force-port 102))))
      (unless client
        (setf client (client-create)))
      (unless (iec-server-client-test obj)
        (client-connect client my-ip my-port)))))

(defmethod iec-server-client-close-connection ((obj iec-server))
  (with-slots (client server ip-address tcp-port client-ds) obj
    (mapcar #'dataset-client-destroy client-ds)
    (setf client-ds '())
    (when client
      (client-close-and-destroy client)
      (setf client nil))))


(defmethod iec-server-client-get-datasets-name ((obj iec-server))
  (with-slots (client server) obj
    (when (iec-server-client-test obj)
      (let ((lds (client-get-logical-devices client)))
        (iter outer (for ld in lds)
          (let ((dss (client-get-ln-dataset
                      client
                      (concatenate 'string
                                   ld "/LLN0"))))
            (iter (for ds in dss)
              (in outer (iter:collect (concatenate 'string
                                                   ld "/LLN0."
                                                   ds))))))))))

(defmethod iec-server-client-get-goose-name ((obj iec-server))
  (with-slots (client server) obj
    (when (iec-server-client-test obj)
      (let ((lds (client-get-logical-devices client)))
        (iter outer (for ld in lds)
          (let ((dss (client-get-ln-goose
                      client
                      (concatenate 'string
                                   ld "/LLN0"))))
            (iter (for ds in dss)
              (in outer (iter:collect (concatenate 'string
                                                   ld "/LLN0."
                                                   ds))))))))))

(defmethod iec-server-set-filebasepath ((obj iec-server) path)
  (when (probe-file path)
    (with-slots (server filebasepath) obj
      (lispiec-internal::iedserver-setfilestorebasepath server path)
      (setf filebasepath (probe-file path)))))

(defgeneric iec-server-create-sample-file (obj &optional file-name-base)
  (:documentation "This method can create new comtrade files based on a pre-existing
template comtrade cfg and dat files. All this method does is copying
template.cfg and template.dat files changing only internal timestamps
in order to make this copied files seems new comtrades.")
  (:method ((obj iec-server) &optional (file-name-base "file-"))
    (with-slots (server filebasepath) obj
      (when filebasepath
        (let* ((cfg-template (merge-pathnames "template.cfg" filebasepath))
               (dat-template (merge-pathnames "template.dat" filebasepath))
               (now (local-time:now))
               (my-cfg (merge-pathnames
                        (concatenate
                         'string file-name-base
                         (write-to-string
                          (local-time:timestamp-to-unix now))
                         ".cfg")
                        filebasepath))
               (my-dat (merge-pathnames
                        (concatenate
                         'string file-name-base
                         (write-to-string
                          (local-time:timestamp-to-unix now))
                         ".dat")
                        filebasepath)))
          (when (and (probe-file cfg-template) (probe-file dat-template))
            (let ((cfg-str (uiop:read-file-lines cfg-template))
                  (ts (with-output-to-string (out)
                        (local-time:format-timestring
                         out
                         now
                         :format
                         '(:day #\/ :month #\/ :year #\, :hour #\: :min #\: :sec #\. :usec))
                        out)))
              (uiop:copy-file dat-template my-dat)
              (with-open-file (ss my-cfg :direction :output :if-exists :supersede)
                (dotimes (itr (length cfg-str))
                  (when itr
                    (if (eql itr (- (length cfg-str) 4))
                        (format ss "~a~%" ts)
                        (format ss "~a~%" (nth itr cfg-str)))))))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Handlers to help create goose subscribers, like mms client functions but with goose ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun make-goose-name-with-fc (goose-name)
  (concatenate 'string
               (ref-get-logical-device-str goose-name)
               "/"
               (ref-get-logical-node-str goose-name)
               "$GO$"
               (ref-get-data-object-str goose-name)))

(defmethod iec-server-client-get-gocb ((obj iec-server) gocb-name)
  (with-slots (client) obj
    (with-foreign-object (err 'iedclienterror)
      (let ((my-gobc (lispiec-internal::iedconnection-getgocbvalues client
                                                                    err
                                                                    gocb-name
                                                                    (null-pointer))))
        (if (eq :ied-error-ok (client-get-error err))
            my-gobc
            nil)))))

(defun gocb-destroy (gocb)
  (lispiec-internal::clientgoosecontrolblock-destroy gocb))

(defmacro make-iec-server-client-gocb-get-fun (func)
  `(with-slots (client) obj
     (when (iec-server-client-test obj)
       (let ((gooses (iec-server-client-get-goose-name obj)))
         (mapcar #'(lambda (r)
                     (let ((cb (iec-server-client-get-gocb obj r)))
                       (when cb (let ((ret (cons r (,func cb))))
                                  (gocb-destroy cb)
                                  ret))))
                 gooses)))))

(defmacro make-iec-server-client-gocb-get-dstadd-slot (func)
  `(with-slots (client) obj
     (when (iec-server-client-test obj)
       (let ((gooses (iec-server-client-get-goose-name obj))
             )
         (mapcar #'(lambda (r)
                     (let ((cb (iec-server-client-get-gocb obj r)))
                       (when cb
                         (let ((ret (cons r
                                          (handler-case
                                              (getf
                                               (let ((py (lispiec-internal::clientgoosecontrolblock-getdstaddress cb)))
                                                 (if (pointerp py)
                                                     (error "is it a pointer? must be plist")
                                                     py))
                                               ,func)
                                            (error (c) (format nil "conversion error: ~a" c)))
                                          )))
                           (gocb-destroy cb)
                           ret))))
                 gooses)))))

(defmacro make-iec-server-client-gocb-get-dstadd-dstadd ()
  `(with-slots (client) obj
     (when (iec-server-client-test obj)
       (let ((gooses (iec-server-client-get-goose-name obj))
             )
         (mapcar #'(lambda (r)
                     (let ((cb (iec-server-client-get-gocb obj r)))
                       (when cb
                         (let ((ret (cons r
                                          (handler-case
                                              (foreign-array-to-lisp
                                               (getf
                                                (let ((py (lispiec-internal::clientgoosecontrolblock-getdstaddress cb)))
                                                  (if (pointerp py)
                                                      (error "is it a pointer? must be plist")
                                                      py))
                                                'lispiec-internal::dstaddress)
                                               '(:array :uint8 6)
                                               )
                                            (error (c) (format nil "conversion error: ~a" c)))
                                             )))
                           (gocb-destroy cb)
                           ret))))
                 gooses)))))


(defmethod iec-server-client-get-datasets-name-from-goose ((obj iec-server))
  (mapcar #'(lambda (r) (cons (car r) (substitute #\. #\$ (cdr r))))
          (make-iec-server-client-gocb-get-fun lispiec-internal::clientgoosecontrolblock-getdatset)))

(defmethod iec-server-client-get-goids-from-goose ((obj iec-server))
  (make-iec-server-client-gocb-get-fun lispiec-internal::clientgoosecontrolblock-getgoid))

(defmethod iec-server-client-get-vlan-from-goose ((obj iec-server))
  (make-iec-server-client-gocb-get-dstadd-slot 'lispiec-internal::vlanid))

(defmethod iec-server-client-get-dstadd-from-goose ((obj iec-server))
  (make-iec-server-client-gocb-get-fun lispiec-internal::clientgoosecontrolblock-getdstaddress))

(defmethod iec-server-client-get-appid-from-goose ((obj iec-server))
  (make-iec-server-client-gocb-get-dstadd-slot 'lispiec-internal::appid))

(defmethod iec-server-client-get-dstaddres-from-goose ((obj iec-server))
  (make-iec-server-client-gocb-get-dstadd-dstadd))


;; (defmethod iec-server-client-get-dstaddress-from-icd ((obj iec-server))
;;   (with-slots (client) obj
;;     (let ((gooses-full (iec-server-client-get-goose-name obj))
;;           (gooses (mapcar #'(lambda (r) (car (last (ppcre:split "\\." r))))
;;                           (iec-server-client-get-goose-name obj)))
;;           (icd (iec-server-get-icd obj)))
;;       (flet ((mlist-to-array (mlist)
;;                (make-array (length mlist)
;;                            :initial-contents mlist))
;;              (parse-hex (my-s) (parse-integer my-s :radix 16))
;;              (remove-empty-str (my-l) (remove "" my-l :test #'string=)))
;;         (mapcar #'cons
;;                 gooses-full
;;                 (mapcar #'mlist-to-array
;;                         (mapcar #'(lambda (r)
;;                                     (mapcar #'parse-hex
;;                                             (remove-empty-str
;;                                              (ppcre:split
;;                                               "(\\s+)|(-)"
;;                                               (stp:data
;;                                                (stp:first-child
;;                                                 (first
;;                                                  (stp:filter-recursively
;;                                                   (sclparser::of-el-name-and-att-value
;;                                                    "P"
;;                                                    "type"
;;                                                    "MAC-Address"
;;                                                    sclparser::scl-uri)
;;                                                   (first (stp:filter-recursively
;;                                                           (sclparser::of-el-name-and-att-value
;;                                                            "GSE"
;;                                                            "cbName"
;;                                                            r
;;                                                            sclparser::scl-uri)
;;                                                           icd))))))))))
;;                                 gooses)))))))


(defgeneric iec-server-get-inrefs (obj)
  (:documentation "This method instantiates and returns all inrefs available in
iec-server as a list of data objects of cdc ORG (only InRefs). Attentiion: these
data-objects can be garbaged collected and another call will
instantiate new objects again.")
  (:method ((obj iec-server))
    (with-slots (logical-device-container server model inrefs) obj
      (if inrefs
          inrefs
          (setf inrefs
                (let* ((iednam (iec-server-get-name obj))
                       (ln0s (mapcar #'(lambda (r) (concatenate 'string r "/LLN0"))
                                     (iec-container-list-names logical-device-container)))
                       (models (mapcar #'cons
                                       (mapcar #'(lambda (r) (subseq r (length iednam)))
                                               (iec-container-list-names logical-device-container))
                                       (mapcar #'(lambda (r) (iedmodel-getmodelnodebyobjectreference model r)) ln0s)))
                       (dobs (mapcar #'(lambda (r) (cons
                                                    (car r)
                                                    (remove-if-not #'(lambda (r)
                                                                       (search "InRef" r
                                                                               :test #'string=))
                                                                   (mapcar #'get-obj-name (get-obj-children (cdr r))))))
                                     models)))
                  (mapcar #'(lambda (r) (cons (obj-reference-str r) r))
                          (flatten
                           (mapcar #'(lambda (r) (let ((ld (car r))
                                                       (dobs2 (cdr r)))
                                                   (mapcar #'(lambda (s)
                                                               (when s (make-cdc-org obj ld "LLN0" s)))
                                                           dobs2)))
                                   dobs)))
                  ))))))

(defgeneric iec-server-clean-inrefs (obj)
  (:method ((obj iec-server))
    (when (iec-server-is-running obj)
      (mapcar #'(lambda (r) (when r (lispiec::iec-server-del-dataobject obj r))) (mapcar #'cdr (iec-server-get-inrefs obj)))
      (with-slots (lispiec::inrefs) obj (setf lispiec::inrefs nil)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                                        ;           ICD xml helpers                                                   ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defgeneric iec-server-get-icd (obj)
  (:documentation "make a spt xml parser and parse iec-server icd file from configuration
directory.")
  (:method ((obj iec-server))
    (with-slots (parent-site) obj
      (stp:find-child t (cxml:parse
                         (merge-pathnames (concatenate 'string
                                                       (iec-server-get-name obj)
                                                       ".icd")
                                          (conf-path parent-site))
                         (stp:make-builder))
                      :key (stp:of-name "SCL" sclparser::scl-uri)))))

(defgeneric iec-server-client-get-dstaddress-from-icd (obj)
  (:documentation "This method gets the goose dstaddress not directely from the
libiec61850 model, but from the icd file instead. It seems there is a
bug in libiec61850 and in some cases the dstaddress in the model
diverges with the one defined in icd file. The last one is supposed to
be the correct.")
  (:method ((obj iec-server))
    (with-slots (client) obj
      (let ((gooses-full (iec-server-client-get-goose-name obj))
            (gooses (mapcar #'(lambda (r) (car (last (ppcre:split "\\." r))))
                            (iec-server-client-get-goose-name obj)))
            (icd (iec-server-get-icd obj)))
        (flet ((mlist-to-array (mlist)
                 (make-array (length mlist)
                             :initial-contents mlist))
               (parse-hex (my-s) (parse-integer my-s :radix 16))
               (remove-empty-str (my-l) (remove "" my-l :test #'string=)))
          (mapcar #'cons
                  gooses-full
                  (mapcar #'mlist-to-array
                          (mapcar #'(lambda (r)
                                      (mapcar #'parse-hex
                                              (remove-empty-str
                                               (ppcre:split
                                                "(\\s+)|(-)"
                                                (stp:data
                                                 (stp:first-child
                                                  (first
                                                   (stp:filter-recursively
                                                    (sclparser::of-el-name-and-att-value
                                                     "P"
                                                     "type"
                                                     "MAC-Address"
                                                     sclparser::scl-uri)
                                                    (first (stp:filter-recursively
                                                            (sclparser::of-el-name-and-att-value
                                                             "GSE"
                                                             "cbName"
                                                             r
                                                             sclparser::scl-uri)
                                                            icd))))))))))
                                  gooses))))))))
