(in-package #:lispiec-csv)

(defun load-export-file (csv-file-path)
  (let ((pth (probe-file csv-file-path)))
    (read-csv pth :separator #\,
              )))

(defun contains (item sequence) (if (member item sequence :test #'string=) t nil))

(defun filter-export-file-supervised (csv-file-path)
  (remove-if-not #'(lambda (row)
                     (string= (nth 11 row) "True"))
                 (load-export-file csv-file-path)))

(defun filter-export-file-cdcs (csv-file-path)
  (remove-if-not #'(lambda (row)
                     (contains (string-downcase (nth 6 row)) '("dpc" "ens" "enc" "sps" "spc" "mv" "cmv")))
                 (filter-export-file-supervised csv-file-path)))

(defun filter-export-file-risa (csv-file-path)
  (remove-if #'(lambda (row)
                     (string= "xxx" (nth 12 row)))
                 (filter-export-file-cdcs csv-file-path)))

(defun save-short-csv (csv-file-path new-file-path)
  (with-open-file (str new-file-path :direction :output :if-exists :supersede)
    (write-csv (filter-export-file-risa csv-file-path) :stream str)))

(defun get-arguments-from-export (csv-file-path)
  (mapcar #'(lambda (row)
              (list (nth 0 row)
                    (nth 2 row)
                    (nth 4 row)
                    (nth 5 row)
                    (string-downcase (nth 6 row))
                    (nth 12 row)
                    (nth 1 row)
                    (nth 15 row)
                    (nth 16 row)
                    (nth 17 row)))
          (filter-export-file-risa csv-file-path)))
