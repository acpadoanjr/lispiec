
(in-package #:lispiec)


(defclass setting-group ()
  ((server-name :accessor setting-group-server-name :initarg :server-name)
   (server :accessor setting-group-server :initform nil)
   (logical-device-inst :accessor setting-group-logical-device-inst :initarg :ld-inst)
   (sg-ref :accessor setting-group-sg-ref :initform nil)
   (handler :accessor setting-group-handler :initform nil)
   (handler-edit :accessor setting-group-handler-edit :initform nil)
   (handler-edit-confirm :accessor setting-group-handler-edit-confirm :initform nil)))

(defmethod setting-group-get-name ((sgcb setting-group))
  (concatenate 'string
               (setting-group-server-name sgcb)
               (setting-group-logical-device-inst sgcb)))

(defmethod setting-group-get-numofsg ((sgcb setting-group))
  (cffi:foreign-slot-value (setting-group-sg-ref sgcb)
                           '(:struct lispiec-internal::settinggroupcontrolblock)
                           'lispiec-internal::numofsgs))

(defmethod setting-group-set-active ((sgcb setting-group) value)
  (let ((maxvalue (setting-group-get-numofsg sgcb)))
    (when (>= maxvalue value)
      (lispiec-internal::iedserver-changeactivesettinggroup (server
                                                             (setting-group-server sgcb))
                                                            (setting-group-sg-ref sgcb)
                                                            value))
    (setting-group-get-active sgcb)))

(defmethod setting-group-get-active ((sgcb setting-group))
  (lispiec-internal::iedserver-getactivesettinggroup (server
                                                      (setting-group-server sgcb))
                                                     (setting-group-sg-ref sgcb)))



(defun make-setting-group (site server-name logical-device-inst)
  (let* ((server (site-get-ied-by-name site server-name))
         (ld (if server
                 (lispiec-internal::iedmodel-getdevicebyinst
                  (model server)
                  logical-device-inst)
                 nil)))
    (when ld
      (let ((sg (make-instance 'setting-group
                               :server-name server-name
                               :ld-inst logical-device-inst))
            (sg-ref (lispiec-internal::logicaldevice-getsettinggroupcontrolblock ld)))
        (when sg-ref
          (setf (setting-group-server sg) server)
          (setf (setting-group-sg-ref sg) sg-ref)
          (make-active-sg-changed sg (event-loop site))
          (make-edit-sg-changed sg (event-loop site))
          (make-edit-sg-confirmation sg (event-loop site))
          (lispiec-internal::iedserver-setactivesettinggroupchangedhandler (server server)
                                                                           sg-ref
                                                                           (setting-group-handler sg)
                                                                           (cffi::null-pointer))
          (lispiec-internal::iedserver-seteditsettinggroupchangedhandler (server server)
                                                                         sg-ref
                                                                         (setting-group-handler-edit sg)
                                                                         (cffi:null-pointer))
          (lispiec-internal::iedserver-seteditsettinggroupconfirmationhandler (server server)
                                                                              sg-ref
                                                                              (setting-group-handler-edit-confirm sg)
                                                                              (cffi:null-pointer))
          sg)))))


