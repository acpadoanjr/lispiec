(in-package :lispiec)

;; sps ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass cdc-sps (cdc-qt cdc-sub) ())

(defun make-cdc-sps (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let ((inst  (make-instance 'cdc-sps
                                :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                                :dataobject-reference dobj
                                :dataattribute-reference (get-child-by-name dobj "stVal")
                                :t-reference (get-child-by-name dobj "t")
                                :q-reference (get-child-by-name dobj "q")
                                :behavior-reference beh
                                :iec-server iec-server
                                :input (make-instance 'generic-input-boolean))))
      (cdc-set-dobj-ref-str-ptr inst)
      (make-subs inst)
      inst)))


(defmethod cdc-update-model ((obj cdc-sps) &optional unsafe force-if-disabled)
  (declare (ignore unsafe force-if-disabled))
  (let ((input-value-status (value (input obj)))
        (server (iec-server obj)))
    (if (is-sub-ena obj)
        (iedserver-updatebooleanattributevalue (server server)
                                               (datr-ref obj)
                                               (cdc-get-subvalue obj))
        (iedserver-updatebooleanattributevalue (server server)
                                               (datr-ref obj)
                                               input-value-status))))

(defmethod cdc-get-value ((obj cdc-sps))
  (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (datr-ref obj))))
    (translate-from-foreign (mmsvalue-getboolean mms) :boolean)))

(defmethod cdc-is-on ((obj cdc-sps))
  (let ((vl (cdc-get-value obj)))
    (if (eql vl nil)
        nil
        t)))

(defmethod cdc-get-subvalue ((obj cdc-sps))
  (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (subval-ref obj))))
    (translate-from-foreign (mmsvalue-getboolean mms) :boolean)))


;; (defmethod cdc-get-status ((obj cdc-sps))
;;   (get-child-by-name (dobj-ref obj) "stVal"))

(defmethod cdc-set-value ((obj cdc-sps) value)
  (let ((status (cdc-get-value obj)))
    (unless (eq status value)
      (cdc-toggle obj))
    (cdc-get-value obj)))


(defmethod cdc-toggle ((obj cdc-sps) &optional unsafe)
  (let ((my-input (input obj))
        (status (cdc-get-value obj)))
    (if (eq status nil)
        (input-set-value-on my-input)
        (input-set-value-off my-input))
    (cdc-update-model-ignore-disabled obj unsafe)
    (cdc-get-value obj)))

(defmethod cdc-init ((obj cdc-sps) &optional unsafe)
  (with-slots (iec-server) obj
    (when (enable iec-server)
      (input-set-value-off (input obj))
      (cdc-update-model-ignore-disabled obj unsafe))))


(defmethod cdc-set-on ((obj cdc-sps))
  (let ((my-input (input obj))
        (status (cdc-get-value obj)))
    (unless (eq status t)
      (progn (input-set-value-on my-input)
             (cdc-update-model-ignore-disabled obj)))
    (cdc-get-value obj)))

(defmethod cdc-set-off ((obj cdc-sps))
  (let ((my-input (input obj))
        (status (cdc-get-value obj)))
    (unless (eq status nil)
      (progn (input-set-value-off my-input)
             (cdc-update-model-ignore-disabled obj)))
    (cdc-get-value obj)))



;;spc;;;;;;;;;;;;;;;;;;;;;;

(defclass cdc-spc (cdc-sps cdc-xxc) ())

(defun islocsta (dobj)
  (search "LocSta" (obj-reference-str dobj)))

(defun make-cdc-spc (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let ((inst (make-instance 'cdc-spc
                               :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                               :dataobject-reference dobj
                               :dataattribute-reference (get-child-by-name dobj "stVal")
                               :t-reference (get-child-by-name dobj "t")
                               :q-reference (get-child-by-name dobj "q")
                               :ctlmodel-reference (get-child-by-name dobj "ctlModel")
                               :behavior-reference beh
                               :iec-server iec-server
                               :input (make-instance 'generic-input-boolean))))
      ;(setf (perf-handler inst) (perf-handler iec-server))
      ;(setf (handler inst) (handler iec-server))
      (cdc-set-dobj-ref-str-ptr inst)
      (make-subs inst)
      (make-if-controlable inst
        (make-callback-check inst (event-loop (parent-site iec-server)) (islocsta inst))
        (with-lock-held ((model-lock iec-server))
          (iedserver-setcontrolhandler (server iec-server) (dobj-ref inst) (handler inst) (obj-reference-str-ptr inst))
          (iedserver-setperformcheckhandler (server iec-server) (dobj-ref inst) (perf-handler inst) (obj-reference-str-ptr inst))))
      inst)))

(defun myxor (a b)
  (or (and a (not b)) (and (not a) b)))


(defmethod cdc-compare-ctlval ((obj cdc-spc) ctlval-ptr)
  (if (myxor (cdc-get-value obj) (mmsvalue-getboolean ctlval-ptr))
      nil
      t))

(defmethod cdc-set-output-ctlval ((obj cdc-spc) ctlval-ptr)
  (setf (output obj) (translate-from-foreign (mmsvalue-getboolean ctlval-ptr) :boolean)))


;;ens ;;;;;;;;;;;;;;;;;;;;;

(defclass cdc-ens (cdc-qt cdc-sub)
  ((old-value
    :accessor old-value
    :initform nil)
   (enumtype
    :accessor enumtype
    :initform nil)))

(defmethod cdc-get-value ((obj cdc-ens))
  (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (datr-ref obj))))
    (translate-from-foreign (mmsvalue-toint32 mms) :int32)))

(defmethod cdc-get-subvalue ((obj cdc-ens))
  (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (subval-ref obj))))
    (translate-from-foreign (mmsvalue-toint32 mms) :int32)))


(defmethod cdc-get-status ((obj cdc-ens))
  (get-child-by-name (dobj-ref obj) "stVal"))

(defmethod cdc-set-value ((obj cdc-ens) int-value)
  (let ((my-input (input obj))
        (status (cdc-get-value obj)))
    (unless (eq status int-value)
      (progn (input-set-value my-input int-value)
             (cdc-update-model-ignore-disabled obj)))))


(defun make-cdc-ens (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let ((inst (make-instance 'cdc-ens
                               :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                                :dataobject-reference dobj
                                :dataattribute-reference (get-child-by-name dobj "stVal")
                                :t-reference (get-child-by-name dobj "t")
                                :q-reference (get-child-by-name dobj "q")
                                :behavior-reference beh
                                :iec-server iec-server
                                :input (make-instance 'generic-input-int))))
      (cdc-set-dobj-ref-str-ptr inst)
      (make-subs inst)
      inst)))

(defmethod cdc-update-model ((obj cdc-ens) &optional unsafe force-if-disabled)
  (declare (ignore unsafe force-if-disabled))
  (let ((input-value-status (value (input obj)))
        (server (iec-server obj))) ;IedServer_updateInt32AttributeValue
    (setf (old-value obj) (cdc-get-value obj))
    (if (is-sub-ena obj)
        (iedserver-updateint32attributevalue (server server)
                                             (datr-ref obj)
                                             (translate-to-foreign (cdc-get-subvalue obj) :int32))
        (iedserver-updateint32attributevalue (server server)
                                             (datr-ref obj)
                                             (translate-to-foreign input-value-status :int32)))))

(defmethod cdc-init ((obj cdc-ens) &optional unsafe)
  (with-slots (iec-server) obj
    (when (enable iec-server)
      (let ((first-enum (car (first (cdc-get-enums obj)))))
        (input-set-value (input obj) (if first-enum first-enum 1))
        (cdc-update-model-ignore-disabled obj unsafe)))))


; increment the value using valid values (enum type), return to first after the last. Move to off is not possible
(defmethod cdc-toggle ((obj cdc-ens) &optional unsafe)
  (let* ((enums (cdc-get-enums obj))
         (my-input (input obj))
         (value (cdc-get-value obj))
         (my-position (position (cdc-get-enum obj) enums))
         (next-enum (when my-position
                      (nth (1+ my-position) enums)))
         (first-enum (first enums)))
    (if (and next-enum
             t ;; (string/= "off" (cdr next-enum))
             )
        (input-set-value my-input (car next-enum))
        (if first-enum
            (input-set-value my-input (car first-enum))
            (if (eql 1 value)
                (input-set-value my-input 2)
                (input-set-value my-input 1))))
    (unless (eql value (value my-input))
      (cdc-update-model-ignore-disabled obj unsafe))
    (cdc-get-value obj)))

(defmethod cdc-get-enums ((obj cdc-ens))
  (let ((id (enumtype obj))
        (enums (enumtypes (iec-server obj))))
    (cdr (find id enums :key #'car :test #'string=))))

(defmethod cdc-get-enum ((obj cdc-ens))
  (let ((v (cdc-get-value obj))
        (enums (cdc-get-enums obj)))
    (find v enums :key #'car)))

;;ins;;;;;;;;;;;;;

(defclass cdc-ins (cdc-ens) ())

(defun make-cdc-ins (iec-server logicaldevice logicalnode dataobject)
    (let ((dobj (iedmodel-getmodelnodebyobjectreference
                 (model iec-server)
                 (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
          (beh (iedmodel-getmodelnodebyobjectreference
                (model iec-server)
                (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
      (let ((inst (make-instance 'cdc-ins
                                 :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                                 :dataobject-reference dobj
                                 :dataattribute-reference (get-child-by-name dobj "stVal")
                                 :t-reference (get-child-by-name dobj "t")
                                 :q-reference (get-child-by-name dobj "q")
                                 :behavior-reference beh
                                 :iec-server iec-server
                                 :input (make-instance 'generic-input-int))))
        (cdc-set-dobj-ref-str-ptr inst)
        (make-subs inst)
        inst)))


;;enc;;;;;;;;;;;;;;;;;;;;;;

(defclass cdc-enc (cdc-ens cdc-xxc) ())

(defun make-cdc-enc (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let ((inst (make-instance 'cdc-enc
                               :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                           :dataobject-reference dobj
                           :dataattribute-reference (get-child-by-name dobj "stVal")
                           :t-reference (get-child-by-name dobj "t")
                           :q-reference (get-child-by-name dobj "q")
                           :ctlmodel-reference (get-child-by-name dobj "ctlModel")
                           :behavior-reference beh
                           :iec-server iec-server
                           :input (make-instance 'generic-input-int))))
      ;(setf (perf-handler inst) (perf-handler iec-server))
      ;(setf (handler inst) (handler iec-server))
      (cdc-set-dobj-ref-str-ptr inst)
      (make-subs inst)
      (make-if-controlable inst
        (make-callback-check inst (event-loop (parent-site iec-server)))
        (with-lock-held ((model-lock iec-server))
          (iedserver-setcontrolhandler (server iec-server) (dobj-ref inst) (handler inst) (obj-reference-str-ptr inst))
          (iedserver-setperformcheckhandler (server iec-server) (dobj-ref inst) (perf-handler inst) (obj-reference-str-ptr inst))))
      inst)))


(defmethod cdc-compare-ctlval ((obj cdc-enc) ctlval-ptr)
  (if (eq (cdc-get-value obj) (translate-from-foreign (mmsvalue-toint32 ctlval-ptr) :int32))
   t
   nil))

(defmethod cdc-set-output-ctlval ((obj cdc-enc) ctlval-ptr)
  (setf (output obj) (translate-from-foreign (mmsvalue-toint32 ctlval-ptr) :int32)))

;;inc ;;;;;;;;;;;;;;;

(defclass cdc-inc (cdc-enc) ())
(defun make-cdc-inc (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let ((inst (make-instance 'cdc-inc
                               :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                           :dataobject-reference dobj
                           :dataattribute-reference (get-child-by-name dobj "stVal")
                           :t-reference (get-child-by-name dobj "t")
                           :q-reference (get-child-by-name dobj "q")
                           :ctlmodel-reference (get-child-by-name dobj "ctlModel")
                           :behavior-reference beh
                           :iec-server iec-server
                           :input (make-instance 'generic-input-int))))
      ;(setf (perf-handler inst) (perf-handler iec-server))
      ;(setf (handler inst) (handler iec-server))
      (cdc-set-dobj-ref-str-ptr inst)
      (make-subs inst)
      (make-if-controlable inst
        (make-callback-check inst (event-loop (parent-site iec-server)))
        (with-lock-held ((model-lock iec-server))
          (iedserver-setcontrolhandler (server iec-server) (dobj-ref inst) (handler inst) (obj-reference-str-ptr inst))
          (iedserver-setperformcheckhandler (server iec-server) (dobj-ref inst) (perf-handler inst) (obj-reference-str-ptr inst))))
      inst)))
;;dpc ;;;;;;;;;;;;;;;;;;;;;

(defclass cdc-dpc (cdc-qt cdc-xxc cdc-sub)
  ((enaopen :accessor enaopen :initform t)
   (enaclose :accessor enaclose :initform t)))

(defmethod cdc-get-value ((obj cdc-dpc))
  (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (datr-ref obj))))
     (dbpos-frommmsvalue mms)))

(defmethod cdc-is-on ((obj cdc-dpc))
  (let ((vl (cdc-get-value obj)))
    (if (eql vl :dbpos-on)
        t
        nil)))


(defmethod cdc-get-subvalue ((obj cdc-dpc))
  (let ((mms (iedserver-getattributevalue (server (iec-server obj)) (subval-ref obj))))
    (dbpos-frommmsvalue mms)))


(defmethod cdc-get-status ((obj cdc-dpc))
  (get-child-by-name (dobj-ref obj) "stVal"))

(defmethod cdc-set-value ((obj cdc-dpc) dbpos-value)
  (let ((my-input (input obj))
        (status (cdc-get-value obj)))
    (unless (eq status dbpos-value)
      (progn (input-set-value my-input dbpos-value)
             (cdc-update-model-ignore-disabled obj)))))


(defun make-cdc-dpc (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let ((inst (make-instance 'cdc-dpc
                               :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                               :dataobject-reference dobj
                               :dataattribute-reference (get-child-by-name dobj "stVal")
                               :t-reference (get-child-by-name dobj "t")
                               :q-reference (get-child-by-name dobj "q")
                               :ctlmodel-reference (get-child-by-name dobj "ctlModel")
                               :behavior-reference beh
                               :iec-server iec-server
                               :input (make-instance 'generic-input-dbpos))))
      ;(setf (perf-handler inst) (perf-handler iec-server))
      ;(setf (handler inst) (handler iec-server))
      (cdc-set-dobj-ref-str-ptr inst)
      (make-subs inst)
      (make-if-controlable inst
        (make-callback-check inst (event-loop (parent-site iec-server)))
        (with-lock-held ((model-lock iec-server))
          (iedserver-setcontrolhandler (server iec-server) (dobj-ref inst) (handler inst) (obj-reference-str-ptr inst) )
          (iedserver-setperformcheckhandler (server iec-server) (dobj-ref inst) (perf-handler inst) (cdc-set-dobj-ref-str-ptr inst))))
      inst)))

(defmethod cdc-update-model ((obj cdc-dpc) &optional unsafe force-if-disabled)
  (declare (ignore unsafe force-if-disabled))
  (let ((input-value-status (value (input obj)))
        (server (iec-server obj)))
    (if (is-sub-ena obj)
        (iedserver-updatedbposvalue (server server)
                                    (datr-ref obj)
                                    (cdc-get-subvalue obj))
        (iedserver-updatedbposvalue (server server)
                                    (datr-ref obj)
                                    input-value-status))))

(defmethod cdc-set-on ((obj cdc-dpc))
  (let ((my-input (input obj))
        (status (cdc-get-value obj)))
    (unless (eq status :dbpos-on)
      (progn (input-set-value-on my-input)
             (cdc-update-model-ignore-disabled obj)))
    (cdc-get-value obj)))

(defmethod cdc-set-off ((obj cdc-dpc))
  (let ((my-input (input obj))
        (status (cdc-get-value obj)))
    (unless (eq status :dbpos-off)
      (progn (input-set-value-off my-input)
             (cdc-update-model-ignore-disabled obj)))
    (cdc-get-value obj)))

(defmethod cdc-init ((obj cdc-dpc) &optional unsafe)
  (with-slots (iec-server) obj
    (when (enable iec-server)
      (input-set-value-off (input obj))
      (cdc-update-model-ignore-disabled obj unsafe))))


(defmethod cdc-toggle ((obj cdc-dpc) &optional unsafe)
  (let ((my-input (input obj))
        (status (cdc-get-value obj)))
    (if (eq status :dbpos-off)
        (input-set-value-on my-input)
        (input-set-value-off my-input))
    (cdc-update-model-ignore-disabled obj unsafe)
    (cdc-get-value obj)))

(defmethod dpc-is-on ((obj cdc-dpc))
  (if (eq (cdc-get-value obj) :dbpos-on)
      t
      nil))

(defmethod dpc-is-off ((obj cdc-dpc))
  (if (eq (cdc-get-value obj) :dbpos-off)
      t
      nil))


(defmethod cdc-compare-ctlval ((obj cdc-dpc) ctlval-ptr)
  (if (myxor (dpc-is-on obj) (mmsvalue-getboolean ctlval-ptr))
      nil
      t))

(defmethod cdc-set-output-ctlval ((obj cdc-dpc) ctlval-ptr)
  (setf (output obj) (translate-from-foreign (mmsvalue-getboolean ctlval-ptr) :boolean)))


(defmethod cdc-is-locked ((obj cdc-dpc))
  (when (dpc-is-on obj)
    (if (enaopen obj)
        nil
        t))
  (unless (dpc-is-on obj)
    (if (enaclose obj)
        nil
        t)))

(defmethod cdc-compare-output-value ((obj cdc-dpc))
  (if (and (dpc-is-on obj) (output obj))
      t
      (if (and (dpc-is-off obj) (not (output obj)))
          t
          nil)))


;;mv ;;;;;;;;;;;;;;;;;;;;;

(defclass cdc-mv (cdc-qt)
  ((range-ref :accessor range-ref
              :initform nil
              :initarg :range-reference)
   (instmag-ref :accessor instmag-ref
                :initform nil
                :initarg :instmag-reference)
   (is-float :accessor mv-is-float
             :initform t
             :initarg :is-float)))


(defun make-cdc-mv (iec-server logicaldevice logicalnode dataobject)
  (let ((dobj (iedmodel-getmodelnodebyobjectreference
               (model iec-server)
               (make-reference (model iec-server) logicaldevice logicalnode dataobject)))
        (beh (iedmodel-getmodelnodebyobjectreference
              (model iec-server)
              (make-reference (model iec-server) logicaldevice "LLN0" "Beh"))))
    (let* ((main-float-mag (get-child-by-name (get-child-by-name dobj "mag") "f"))
           (main-int-mag (get-child-by-name (get-child-by-name dobj "mag") "i"))
           (main-float-instmag (get-child-by-name (get-child-by-name dobj "instMag") "f"))
           (main-int-instmag (get-child-by-name (get-child-by-name dobj "instMag") "i"))
           (inst (make-instance 'cdc-mv
                                :obj-reference-str (make-reference (model iec-server) logicaldevice logicalnode dataobject)
                                :dataobject-reference dobj
                                :dataattribute-reference (if main-float-mag main-float-mag main-int-mag)
                                :range-reference (get-child-by-name dobj "range")
                                :instmag-reference (if main-float-instmag main-float-instmag main-int-instmag)
                                :is-float (if main-float-mag t nil)
                                :t-reference (get-child-by-name dobj "t")
                                :q-reference (get-child-by-name dobj "q")
                                :behavior-reference beh
                                :iec-server iec-server
                                :input (make-instance 'generic-input-float))))
      (cdc-set-dobj-ref-str-ptr inst)
      inst)))

(defmethod cdc-get-value ((obj cdc-mv))
  (let ((mms (datr-ref obj)))
    (if (mv-is-float obj)
        (translate-from-foreign (iedserver-getfloatattributevalue (server (iec-server obj)) mms) :float)
        (float (translate-from-foreign (iedserver-getint32attributevalue (server (iec-server obj)) mms) :int32)))))


(defmethod cdc-update-model ((obj cdc-mv) &optional unsafe force-if-disabled)
  (declare (ignore unsafe force-if-disabled))
  (let ((input-value-status (value (input obj)))
        (server (iec-server obj)))
    (if (mv-is-float obj)
        (progn (iedserver-updatefloatattributevalue (server server)
                                                    (datr-ref obj)
                                                    (translate-to-foreign input-value-status :float))
               (unless (null (instmag-ref obj))
                 (iedserver-updatefloatattributevalue (server server)
                                                      (instmag-ref obj)
                                                      (translate-to-foreign input-value-status :float))))
        (progn (iedserver-updateint32attributevalue (server server)
                                                    (datr-ref obj)
                                                    (translate-to-foreign (round input-value-status) :int32))
               (unless (null (instmag-ref obj))
                 (iedserver-updateint32attributevalue (server server)
                                                      (instmag-ref obj)
                                                      (translate-to-foreign (round input-value-status) :int32)))))))


(defmethod cdc-set-value ((obj cdc-mv) float-value)
  (let ((my-input (input obj))
        (status (cdc-get-value obj))
        (float-value-input (if (floatp float-value)
                               float-value
                               (float float-value))))
    (unless (eq status float-value-input)
      (progn (input-set-value my-input float-value-input)
             (cdc-update-model-ignore-disabled obj)
             (cdc-get-value obj)))))


(defmethod cdc-init ((obj cdc-mv) &optional unsafe)
  (with-slots (iec-server) obj
    (when (enable iec-server)
      (input-set-value (input obj) 0.0)
      (cdc-update-model-ignore-disabled obj unsafe))))

;; non sense toggle, only to change values between 1 and 2
(defmethod cdc-toggle ((obj cdc-mv) &optional unsafe)
  (let ((my-input (input obj)))
    (if (eql 1.0 (value my-input))
        (input-set-value my-input 2.0)
        (input-set-value my-input 1.0))
    (cdc-update-model-ignore-disabled obj unsafe)
    (cdc-get-value obj)))

(defmethod is-sub-ena ((obj cdc-mv))
  nil)
