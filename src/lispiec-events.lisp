;;;;;;;;;;;;;;;;;;;;
;; event handling ;;
;;;;;;;;;;;;;;;;;;;;

(in-package :deeds)

(deeds:define-event deeds::data-object-changed ()
  ((message :initarg :message :reader message)
   (data-object :initarg :data-object :reader data-object)
   (value :initarg :value :reader value)
   (validity :initarg :validity :reader validity)
   (timestamp :initarg :timestamp :reader timestamp)
   (quality :initarg :quality :reader quality))
  (:default-initargs
   :message (error "Message required")
   :data-object (error "data-object required.")
   :value nil
   :validity nil
   :timestamp nil
   :quality nil))


(in-package :lispiec)

(defparameter log-mode t)

(deeds:define-event deeds::wants-oper-event (deeds:payload-event)
  ())

(deeds:define-event deeds::act-changed (deeds::data-object-changed)
  ())

(deeds:define-event deeds::substitution (deeds:message-event)
  ())

(deeds:define-event deeds::cycle-trigger (deeds:message-event)
  ())

(deeds:define-event deeds::goose-received (deeds:message-event)
  ())

(deeds:define-event deeds::goose-client-cycle (deeds:message-event)
  ())

(deeds:define-event deeds::sgcb-received (deeds:payload-event)
  ())

(deeds:define-event deeds::sgcb-edit-changed (deeds:payload-event)
  ())

(deeds:define-event deeds::sgcb-edit-confirmation (deeds:payload-event)
  ())

(deeds:define-event deeds::oper-termination (deeds:payload-event)
  ())

(deeds:stop deeds:*standard-event-loop*)

(defmethod cdc-update-model :after ((obj cdc-act) &optional unsafe force-if-disabled)
  (declare (ignore unsafe force-if-disabled))
  (deeds:do-issue deeds::act-changed
    :message (obj-reference-str obj)
    :loop (event-loop (parent-site (iec-server obj)))
    :data-object obj))

(defmethod cdc-update-model :after ((obj cdc) &optional unsafe force-if-disabled)
  (declare (ignore unsafe force-if-disabled))
  (deeds:do-issue deeds::data-object-changed
    :message (obj-reference-str obj)
    :loop (event-loop (parent-site (iec-server obj)))
    :data-object obj))


(defun data-object-compare-regex (obj ld-regex-str ln-regex-str do-regex-str)
  (let ((doreg (create-scanner do-regex-str))
        (lnreg (create-scanner ln-regex-str))
        (ldreg (create-scanner ld-regex-str)))
    (if (scan doreg (cdc-get-data-object-str obj))
        (if (scan lnreg (cdc-get-logical-node-str obj))
            (if (scan ldreg (cdc-get-logical-device-str obj))
                t
                nil)
            nil)
        nil)))

(defmacro create-data-object-compare-regex-function (function-name ld-regex-str ln-regex-str do-regex-str)
  (let ((doregs (gensym "doreg"))
        (lnregs (gensym "lnreg"))
        (ldregs (gensym "ldreg")))
    `(progn
       (setf ,doregs (create-scanner ,do-regex-str))
       (setf ,lnregs (create-scanner ,ln-regex-str))
       (setf ,ldregs (create-scanner ,ld-regex-str))
       (defun ,function-name (obj)
         (if (scan ,doregs (cdc-get-data-object-str obj))
             (if (scan ,lnregs (cdc-get-logical-node-str obj))
                 (if (scan ,ldregs (cdc-get-logical-device-str obj))
                     t
                     nil)
                 nil)
             nil)))))

(defmacro create-data-object-ref-compare-regex-function (function-name ld-regex-str ln-regex-str do-regex-str)
  (let ((doregs (gensym "doreg"))
        (lnregs (gensym "lnreg"))
        (ldregs (gensym "ldreg")))
    `(progn
       (setf ,doregs (create-scanner ,do-regex-str))
       (setf ,lnregs (create-scanner ,ln-regex-str))
       (setf ,ldregs (create-scanner ,ld-regex-str))
       (defun ,function-name (ref)
         (if (scan ,doregs (ref-get-data-object-str ref))
             (if (scan ,lnregs (ref-get-logical-node-str ref))
                 (if (scan ,ldregs (ref-get-logical-device-str ref))
                     t
                     nil)
                 nil)
             nil)))))


(defun data-object-compare-text-ref (ref ld-str ln-str do-str)
  (if (search do-str (ref-get-data-object-str ref))
      (if (search ln-str (ref-get-logical-node-str ref))
          (if (search ld-str (ref-get-logical-device-str ref))
              t
              nil)
          nil)
      nil))

(defun data-object-compare-text (obj ld-str ln-str do-str)
  (if (search do-str (cdc-get-data-object-str obj))
      (if (search ln-str (cdc-get-logical-node-str obj))
          (if (search ld-str (cdc-get-logical-device-str obj))
              t
              nil)
          nil)
      nil))

(defun data-object-compare-text-alias (obj alias-str)
  (if (search alias-str (alias obj))
      t
      nil))

;; Not used anymore
(defmacro define-default-dobj-changed (site)
  `(deeds:define-handler (default-dobj-changed-handler deeds::data-object-changed) (event)
     :loop (event-loop ,site)
     ;; (format T "~&INFO: ~a changed.~%" (deeds:message event))
     ))

(defmacro define-substitution-handler (site)
  `(deeds:define-handler (default-substitution-handler deeds::substitution) (events)
     :loop (event-loop ,site)
                                        ;(format T "~&INFO: ~a~%" message)
     (let ((obj (site-get-dataobject-by-regex
                 ,site
                 (concatenate 'string "^" (ref-get-logical-device-str (deeds:message events))
                              "$")
                 (concatenate 'string "^" (ref-get-logical-node-str (deeds:message events))
                              "$")
                 (concatenate 'string "^" (ref-get-data-object-str (deeds:message events))
                              "$"))))
       (cdc-update-model-ignore-disabled obj))))

(defmacro define-site-operate-handler (site)
  `(deeds:define-handler (wants-operate-handler deeds::wants-oper-event) (evento)
     :loop (event-loop ,site)
     :class 'deeds:parallel-handler
     (let ((obj (deeds:payload evento)))
       (sleep (simulated-reaction-time-sec obj))
       (if (typep obj (find-class 'cdc-enc))
           (cdc-set-value obj (output obj))
           (cdc-toggle obj))
       (deeds:cancel evento))))

(defmacro with-dobj-define-handler (dobj site handler-name ld-str ln-str do-str &body body)
  (let ((event-var (gensym "envent"))
        )
    ;; (setf (symbol-value event-var) nil)
    `(deeds:define-handler (,handler-name deeds::data-object-changed) (,event-var message)
       :loop (event-loop ,site)
       :filter '(data-object-compare-text-ref message ,ld-str ,ln-str ,do-str)
       :class 'deeds:queued-handler
       (let ((,dobj (deeds::data-object ,event-var)))
         (when lispiec::log-mode (format T "~&INFO: ~a handler on ~a.~%" (string ',handler-name) (deeds:message ,event-var)))
         ,@body))))

(defmacro with-act-define-transient (site handler-name &optional (transient-time-s 0.1))
  (let ((event-var (gensym "eventact"))
        )
    `(deeds:define-handler (,handler-name deeds::act-changed) (,event-var message)
       :loop (event-loop ,site)
       :class 'deeds:queued-handler
       (let ((dobj (deeds::data-object ,event-var)))
         ;; (format T "~&INFO: ~a handler on ~a.~%" (string ',handler-name) (deeds:message ,event-var))
         (when (cdc-is-on dobj)
           (bt2:make-thread
            (lambda ()
              (sleep ,transient-time-s)
              (cdc-set-off lispiec::dobj))))))))

(defmacro with-dobj-regex-define-handler (dobj site handler-name ld-regex-str ln-regex-str do-regex-str &body body)
  (let ((filterfun (gensym "handler"))
        (event-var (gensym "envent")))
    `(progn
       (create-data-object-ref-compare-regex-function ,filterfun ,ld-regex-str ,ln-regex-str ,do-regex-str)
       (deeds:define-handler (,handler-name deeds::data-object-changed) (,event-var message)
         :loop (event-loop ,site)
         :filter '(,filterfun message)
         :class 'deeds:queued-handler
         (let ((,dobj (deeds::data-object ,event-var)))
           ;; (format T "~&INFO: ~a regex handler on ~a.~%" (string ',handler-name) (deeds:message ,event-var))
           ,@body)))))

(defmacro with-dobj-alias-define-handler (dobj site handler-name alias-str bay &body body)
  (let ((mobj (lispiec:site-get-dataobject-by-alias site alias-str bay)))
    (when mobj
      (let ((dd (lispiec::obj-reference-str mobj))
            (event-var (gensym "event")))
        `(deeds:define-handler (,handler-name deeds::data-object-changed) (,event-var message)
           :loop (event-loop ,site)
           :filter '(string= message ,dd)
           :class 'deeds:queued-handler
           (let ((,dobj (deeds::data-object ,event-var)))
             ,@body))))))

(defmacro with-dobj-alias-define-transient (site handler-name alias-str bay &optional (transient-time-s 0.1))
  (let ((mobj (lispiec:site-get-dataobject-by-alias site alias-str bay)))
    (when mobj
      (let ((dd (lispiec::obj-reference-str mobj))
            (event-var (gensym "event")))
        `(deeds:define-handler (,handler-name deeds::data-object-changed) (,event-var message)
           :loop (event-loop ,site)
           :filter '(string= message ,dd)
           :class 'deeds:queued-handler
           (let ((dobj (deeds::data-object ,event-var)))
             (when (cdc-is-on dobj)
               (bt2:make-thread
                (lambda ()
                  (sleep ,transient-time-s)
                  (cdc-set-off dobj))))))))))

(defmacro with-site-define-cycle-handler (site handler-name message-filter &body body)
  `(deeds:define-handler (,handler-name deeds::cycle-trigger) (eventcy message)
     :loop (event-loop ,site)
     :filter '(search ,message-filter message)
     ,@body))

(defmacro with-dobj-define-transient (site handler-name ld-str ln-str do-str &optional (transient-time-s 0.1))
  `(with-dobj-define-handler lispiec::dobj ,site ,handler-name ,ld-str ,ln-str ,do-str
    (when (cdc-is-on lispiec::dobj)
      (bt2:make-thread
       (lambda ()
         (sleep ,transient-time-s)
         (cdc-set-off lispiec::dobj))))))

(defmacro with-dobj-define-inv-transient (site handler-name ld-str ln-str do-str &optional (transient-time-s 1.0))
  `(with-dobj-define-handler lispiec::dobj ,site ,handler-name ,ld-str ,ln-str ,do-str
     (unless (cdc-is-on lispiec::dobj)
       (bt2:make-thread
        (lambda () (sleep ,transient-time-s)
          (cdc-set-on lispiec::dobj))))))


(defmacro with-site-define-goose-client-handler (site handler-name)
  `(deeds:define-handler (,handler-name deeds::goose-client-cycle) (event)
     :loop (event-loop ,site)
     (with-slots (goose-recs) ,site
       (iter:iter (for r iter:in goose-recs)
         (with-slots (goose-subs) r
           (mapcar #'(lambda (ds) (dataset-values-update-data-objects ds t))
                   (mapcar #'dataset goose-subs))
           ;; (mapcar #'(lambda (v ds) (if v
           ;;                              (progn
           ;;                                ;; (dataset-client-update-mms-by-pool ds)
           ;;                                (dataset-values-update-data-objects ds t))
           ;;                              (dataset-values-set-invalid ds)))
           ;;         (mapcar #'goose-client-subscriber-is-valid goose-subs)
           ;;         (mapcar #'dataset goose-subs))

           ))))) ;TODO: is-valid does not work. why?

(defmacro with-site-define-oper-termination (site handler-name)
  `(deeds:define-handler (,handler-name deeds::oper-termination) (event)
     :loop (event-loop ,site)
     (let ((ref (termination-client-info-ref (deeds:payload event)))
           (addc (termination-client-info-addcause (deeds:payload event)))
           (tt (termination-client-info-timestamp (deeds:payload event))))
       (let ((my-dobj (site-get-dataobject-by-ref ,site ref))
             (my-add (getf addc 'lispiec-internal::addcause)))
         (with-slots (client-term-t client-term-addcause client-term-result) my-dobj
           (setf client-term-t tt)
           (setf client-term-result (if (eql :add-cause-unknown my-add) t nil))
           (setf client-term-addcause addc))))))

(defmacro with-sgcb-define-handler (sgcb setpoint site handler-name ld-str &body body)
  `(deeds:define-handler (,handler-name deeds::sgcb-received) (event)
     :loop (event-loop ,site)
     (let* ((,sgcb (car (deeds:payload event)))
            (,setpoint (cdr (deeds:payload event)))
            (sgcb-name (lispiec::setting-group-get-name ,sgcb)))
       (when (search ,ld-str sgcb-name)
         (format T "~&INFO: ~a settinggroug handler on ~a.~%"
                 (string ',handler-name)
                 sgcb-name)
         (lispiec::setting-group-set-active ,sgcb ,setpoint)
         ,@body))))

(defmacro with-sgcb-define-handler-edit (sgcb setpoint site handler-name ld-str &body body)
  `(deeds:define-handler (,handler-name deeds::sgcb-edit-changed) (event)
     :loop (event-loop ,site)
     (let* ((,sgcb (car (deeds:payload event)))
            (,setpoint (cdr (deeds:payload event)))
            (sgcb-name (lispiec::setting-group-get-name ,sgcb)))
       (when (search ,ld-str sgcb-name)
         (format T "~&INFO: ~a settinggroug edition handler on ~a.~%"
                 (string ',handler-name)
                 sgcb-name)
         ,@body))))

(defmacro with-sgcb-define-handler-edit-confirm (sgcb setpoint site handler-name ld-str &body body)
  `(deeds:define-handler (,handler-name deeds::sgcb-edit-confirmation) (event)
     :loop (event-loop ,site)
     (let* ((,sgcb (car (deeds:payload event)))
            (,setpoint (cdr (deeds:payload event)))
            (sgcb-name (lispiec::setting-group-get-name ,sgcb)))
       (when (search ,ld-str sgcb-name)
         (format T "~&INFO: ~a settinggroug edition confirmation handler on ~a.~%"
                 (string ',handler-name)
                 sgcb-name)
         ,@body))))


(defun hash-table-keys (ht)
  (let ((keys nil))
    (maphash
     #'(lambda (k v)
         (declare (ignore v))
         (push k keys))
     ht)
    keys))

(defun get-handlers-name (site)
  (let ((ht (deeds:handlers (event-loop site))))
    (remove-if-not #'(lambda (r) (search "LISPIEC-RSPACE"
                                         (handler-case (package-name
                                                        (symbol-package r))
                                           (error (c)
                                             "NONE"))
                                         :test #'string=))
                   (hash-table-keys ht))))



(defun site-clear-all-handlers (site)
  (let ((lst  (get-handlers-name site))
        (ev-lp (event-loop site)))
    (mapcar #'(lambda (row)
                (deeds:deregister-handler row ev-lp))
            lst)))


                                        ;(deeds:recompile-event-loop deeds:*standard-event-loop*)
