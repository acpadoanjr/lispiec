(in-package :guibot)

(defmacro bot-start-python ()
  `(when (py4cl:python-eval "True")
     ;; OPC Python functions
     (py4cl:import-function "GuiBot" :from "guibot.guibot")
     (py4cl:import-function "XDoToolController" :from "guibot.controller")
     (py4cl:import-function "VNCDoToolController" :from "guibot.controller")
     (py4cl:import-function "Location" :from "guibot.location")
     (py4cl:import-function "Region" :from "guibot.region")
     ))

(defclass bot-region ()
  ((guibot :accessor bot-guibot :initform nil)
   (controller :accessor bot-controller :initform nil)))

(defclass bot (bot-region)
  ())

(defclass bot-match (bot-region)
  ())

(defun make-bot (&key (vnc nil) (vnc-host "localhost") (vnc-port 0) (vnc-password "") (image-path "~/Pictures"))
  (let ((this-bot (make-instance 'bot)))
    (setf (bot-controller this-bot) (if vnc
                                        (vncdotoolcontroller t nil)
                                        (xdotoolcontroller)))
    (setf (bot-guibot this-bot) (guibot (bot-controller this-bot)))
    (py4cl:python-method (bot-guibot this-bot) "add_path" image-path)
    (when vnc
      (bot-set-vnc-parameters this-bot :vnc-host vnc-host :vnc-port vnc-port :vnc-password vnc-password)
      )
    this-bot))

(defun make-region-from-coordinates (main-bot &optional (xpos 0) (ypos 0) (width 1920) (height 1080))
  (let ((this-bot (make-instance 'bot-region)))
    (setf (bot-controller this-bot) (bot-controller main-bot))
    (setf (bot-guibot this-bot) (region xpos ypos width heigth (bot-controller this-bot)))
    this-bot))

(defun make-bot-match (python-match)
  (let ((this-bot (make-instance 'bot-match)))
    (setf (bot-guibot this-bot) python-match)
    this-bot))


(defmethod bot-set-vnc-parameters ((bot bot) &key (vnc-host "localhost") (vnc-port 0) (vnc-password ""))
  (let ((this-bot (bot-controller bot)))
    (py4cl:chain this-bot params (__getitem__ "vncdotool") (__setitem__ "vnc_hostname" vnc-host))
    (py4cl:chain this-bot params (__getitem__ "vncdotool") (__setitem__ "vnc_port" vnc-port))
    (py4cl:chain this-bot params (__getitem__ "vncdotool") (__setitem__ "vnc_password" vnc-password))))

(defmethod bot-start-vnc-connection ((bot bot))
  (let ((this-bot-controller (bot-controller bot)))
    (py4cl:chain this-bot-controller (synchronize_backend))))

(defmethod bot-get-mouse-location ((bot bot-region))
  (let ((loc (py4cl:python-method (bot-controller bot) "get_mouse_location")))
    (cons (py4cl:python-method loc "get_x")
          (py4cl:python-method loc "get_y"))))

(defmethod bot-move-mouse ((bot bot-region) x y)
  (let ((loc (location x y)))
    (py4cl:python-method (bot-controller bot) "mouse_move" loc)))

(defmethod bot-type-text ((bot bot-region) text)
  (let ((loc (location x y)))
    (py4cl:python-method (bot-controller bot) "keys_type" text)))

(defmethod bot-click-mouse ((bot bot) &optional (bouton 1) (count 1))
  (py4cl:python-method (bot-controller bot) "mouse_click" bouton count))

(defmethod bot-exists-in-region ((bot bot-region) target)
  (let ((python-obj
          (py4cl:python-method (bot-guibot bot) "exists" target)))
    (make-bot-match python-obj)))

(defmethod bot-region-click ((bot-region bot) target)
  (py4cl:python-method (bot-guibot bot) "click" target))
