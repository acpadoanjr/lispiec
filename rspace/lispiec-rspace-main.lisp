
(in-package :lispiec-rspace)

(defun unknown-option (condition)
  (format t "warning: ~s option is unknown!~%" (opts:option condition))
  (invoke-restart 'opts:skip-option))

(defmacro when-option ((options opt) &body body)
  `(let ((it (getf ,options ,opt)))
     (when it
       ,@body)))

(defparameter *project-path* (uiop:native-namestring "~/ieds/"))
(defparameter *tcp-port* 10000)
(defparameter *additional-script* (uiop:native-namestring "~/start_rspace.lisp"))
(defparameter *only-conf* nil)
(defparameter *scd-new* nil)
(defparameter *ds-regex* "(GSE|DQCE|DQPO|CYPO|DQGW)$")


(defun main (&optional (daemon t))
  (opts:define-opts
    (:name :help
     :description "Print this help text"
     :short #\h
     :long "help")
    (:name :port
     :description "Slynk port number (ied's mms server will listen to ports above this number) "
     :short #\p
     :long "port"
     :default 10000
     :arg-parser #'parse-integer) ;; <- takes an argument
    (:name :project-path
     :description "Path containing SCD and configuration files."
     :short #\d
     :long "dir-project"
     :default "~/ieds/"
     :arg-parser #'identity)
    (:name :scd-path
     :description "New SCD file to be copied in project directory. Starts the configuration files creation and then exit."
     :short #\s
     :long "scd-path"
     :arg-parser #'identity)
    (:name :additional-script
     :description "Lisp script to be loaded after site creation (with local simulation code)."
     :long "additional-script"
     :default "~/start_rspace.lisp"
     :arg-parser #'identity)
    (:name :ds-regex
     :description "Regex targeting dataset names which shall be considered while parsing SCD file."
     :long "ds-regex"
     :default "(GSE|DQCE|DQPO|CYPO|DQGW)$"
     :arg-parser #'identity))

  (multiple-value-bind (options free-args)
      (handler-case
          (handler-bind ((opts:unknown-option #'unknown-option))
            (opts:get-opts))
        (opts:missing-arg (condition)
          (format t "fatal: option ~s needs an argument!~%"
                  (opts:option condition)))
        (opts:arg-parser-failed (condition)
          (format t "fatal: cannot parse ~s as argument of ~s~%"
                  (opts:raw-arg condition)
                  (opts:option condition)))
        (opts:missing-required-option (con)
          (format t "fatal: ~a~%" con)
          (opts:exit 1)))
    ;; Here all options are checked independently, it's trivial to code any
    ;; logic to process them.
    (when-option (options :help)
      (opts:describe
       :prefix " Spacio is a IEC-61850 simulator based on Common-Lisp and libiec61850."
       :suffix "Hope you enjoy…"
       :usage-of "spacio"
       :args     "[FREE-ARGS]")
      (uiop:quit))
    (when-option (options :port)
      (format t "~% If needed, use spacio -h for help. ~%OK, port ~a is choosen …~%" it)
      (setf *tcp-port* it))
    (when-option (options :scd-path)
      (let ((nn (uiop:native-namestring it)))
        (format t "~% You've supplied this new SCD file: ~a ~%" nn)
        (if (probe-file nn)
            (progn (setf *scd-new* it)
                   (setf *only-conf* t))
            (progn (format t "~% Warning: SCD file ~a does not exist! ~% Not loading it … ~% ~%" nn)
                   (uiop:quit)))))
    (when-option (options :project-path)
      (let ((nn (uiop:native-namestring it)))
        (format t "~% You've supplied this path for configuration files: ~a ~%" nn)
        (if (probe-file nn)
            (setf *project-path* nn)
            (progn (format t "~% ERROR: project-path ~a does not exist! ~%" nn)
                   (opts:exit 1)))))
    (when-option (options :additional-script)
      (let ((nn (uiop:native-namestring it)))
        (format t "~% You've supplied this additionnal script: ~a ~%" nn)
        (if (probe-file nn)
            (setf *additional-script* nn)
            (progn (format t "~% Warning: lisp additional script ~a does not exist! ~% Not loading it … ~% ~%" nn)
                   (setf *additional-script* nil)))))
    (when-option (options :ds-regex)
      (format t "~% You've supplied this dataset regex ~a ~%" it)
      (setf *ds-regex* it))
    (format t "free args: ~{~a~^, ~}~%" free-args))

  (setf sb-ext:*muffled-warnings* 'cl:warning)

  (set-signal-handler 15
    (format t "Exiting spacio.... ~%")
    (handler-case
        (when clog::*clog-running* (clog:shutdown))
      (unbound-variable ()
        (format t "No webserver to stop.... ~%")))
    (setf daemon nil)
    (exit-rspace))

  (set-signal-handler 2
    (format t "Exiting spacio.... ~%")
    (handler-case
        (when clog::*clog-running* (clog:shutdown))
      (unbound-variable ()
        (format t "No webserver to stop.... ~%")))
    (setf daemon nil)
    (exit-rspace))

  (let ((thr (bt:make-thread
              (lambda () (progn (sb-thread:release-foreground)
                                (if *only-conf*
                                    (progn
                                      (sclparser:delete-files-in-dir *project-path* "icd")
                                      (sclparser:delete-files-in-dir *project-path* "scd")
                                      (uiop:copy-file *scd-new* (merge-pathnames *project-path* (path:basename *scd-new*)))
                                      (make-site-config-and-quit *project-path* *tcp-port* *ds-regex*))
                                    (make-rspace *project-path* *tcp-port* *ds-regex*))
                                (sleep 3.0)))
              :name "make-site")))
    (bt:join-thread thr))
  (when *additional-script*
    (format t "Loading script additional script … ~%")
    (load *additional-script*))

  (when daemon
    (loop while daemon
          do (sleep 5.0)))
  nil)

(defun main-as-thread ()
  (let ((thr (bt:make-thread
              (lambda () (main nil))
              :name "main")))
    (bt:join-thread thr)
    nil))

(defparameter *myctx* nil)
(defparameter *mysocket* nil)
;; zmq event to python
(defun start-zmq (&optional (tcp 5557))
  (setf *myctx* (pzmq:ctx-new))
  (setf *mysocket* (pzmq:socket *myctx* :pub))
  (pzmq:bind *mysocket* (concatenate 'string
                                     "tcp://*:"
                                     (write-to-string tcp)))
  (with-dobj-define-handler pydobj *rspace* pyevent "" "" ""
    (pzmq:send *mysocket* (lispiec::obj-reference-str pydobj)))
  nil)

(defun stop-zmq ()
  (pzmq:close *mysocket*)
  (with-dobj-define-handler pydobj *rspace* pyevent "" "" ""
    (format t "~a changed but zmq is stopped! ~%" (lispiec::obj-reference-str pydobj)))
  (deeds:deregister-handler (gethash 'pyevent (deeds:handlers (event-loop *rspace*)))
                            (event-loop *rspace*)))


;; (with-open-file (sst "/tmp/dobs.csv" :direction :output)
;;   (mapcar #'(lambda (r)
;;               (let ((tx (cdr r))
;;                     (iedname (lispiec::iec-server-get-name (iec-server (cdr r)))))
;;                 (unless (string= iedname "IEDTEST")
;;                   (format sst "~a~%"
;;                           (concatenate 'string
;;                                        (lispiec::iec-server-get-name (iec-server tx))
;;                                        ";"
;;                                        (lispiec::cdc-get-logical-device-inst-str tx)
;;                                        ";"
;;                                        (lispiec::cdc-get-logical-node-str tx)
;;                                        ";"
;;                                        (lispiec::cdc-get-data-object-str tx))))))
;;           dds))
