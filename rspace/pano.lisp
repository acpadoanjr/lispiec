(in-package :lispiec-pano)



(defvar *regex-bay* (ppcre:create-scanner "^(\\d)?([A-Z]+)(\\d+)$"))
(defvar *regex-ied* (ppcre:create-scanner "([A-Z]{3})(\\d)$"))
;(setf *regex-ied* (ppcre:create-scanner "([A-Z]{3})(\\d)$"))


;; button class

(defclass button ()
  ((script :accessor script :initform nil)
   (opc-client :accessor opc-client :initform nil)))


(defmacro start-python ()
  `(when (py4cl:python-eval "True")
     ;; OPC Python functions
     (py4cl:import-function "Client" :from "opcua")
     (py4cl:import-function "Node" :from "opcua.common.node")
     ;; (py4cl:import-module "opcua.ua.uatypes" :as "uatypes")
     ;; (py4cl:import-module "opcua.ua.uatypes.DataValue" :as "dv")
     (py4cl:import-module "datetime")
     (py4cl:import-module "time" :as "ptime")
     ;; (py4cl:import-function "GuiBot" :from "guibot.guibot")
     ;; (py4cl:import-function "XDoToolController" :from "guibot.controller")
     ;; (py4cl:import-function "VNCDoToolController" :from "guibot.controller")
     ;; (py4cl:import-function "Location" :from "guibot.location")
     ;; (py4cl:import-function "Region" :from "guibot.region")



     (py4cl:import-function "timezone" :from "datetime")
     (py4cl:import-function "datetime.timestamp")
     (py4cl:import-function "get_timestamp")

     (py4cl:import-function "DataValue" :from "opcua.ua.uatypes")
     ;; (py4cl:import-function "DataValue.SourceTimestamp")
     (py4cl:import-function "Client.connect")
     (py4cl:import-function "Client.disconnect")
     (py4cl:import-function "Client.send_hello")
     (py4cl:import-function "Client.get_root_node")
     (py4cl:import-function "Client.get_node")
     (py4cl:import-function "Node.get_value")
     (py4cl:import-function "Node.get_data_value")
     (py4cl:import-function "Node.set_value")
     (py4cl:import-function "Node.get_properties")
     (py4cl:import-function "Node.get_attributes")
     (sleep 2.0)
     (defmethod pano-get-timestamp (opc-client (obj lispiec::cdc-qt))
       (let* ((dv (node.get_data_value (pano-get-node opc-client obj)))
              (ts (py4cl:python-eval "round(" (py4cl::pythonize dv)
                                     ".SourceTimestamp.replace(tzinfo=timezone.utc).timestamp() * 1000)")))
         ts))))


(defmethod button-get-node ((button button))
  (opc-client-get-node (opc-client button) (opc-script-suffix (opc-prefix-conc (script button)))))

(defmethod button-click ((button button))
  (node.set_value (button-get-node button) t))

(defun opc-script-suffix (addr)
  (concatenate 'string addr ".Active"))

(defmethod make-button (opc-client pano-addr-script)
  (let ((but (make-instance 'button)))
    (setf (opc-client but) opc-client)
    (setf (script but) pano-addr-script)
    but))

  ;; opc generice functions


(defun opc-client-create (opc-tcp-address)
  (client opc-tcp-address))

(defun opc-client-connect (opc-client)
  (client.connect opc-client))

(defun opc-client-disconnect (opc-client)
  (client.disconnect opc-client))


(defun opc-client-get-root (opc-client)
  (client.get_root_node opc-client))

(defun opc-client-get-node (opc-client node-address)
  (client.get_node opc-client node-address))

(defun opc-node-get-value (opc-node)
  (node.get_value opc-node))

(defun opc-node-set-value (opc-node value)
  ()  (node.set_value opc-node value))

(defun get-pano-bay (dob)
  (let ((baystr (lispiec::ldbay dob))
        (baygrp (lispiec::bay dob))
        (ld (lispiec::cdc-get-logical-device-str dob)))
    (when (search "TG1LDMODEXPF1" ld :test 'string=)
      (setf baystr "S_AUX"))
    ;;forcing this LD to be on SAUX (tmp?)
    (when (and  (search "LDGRP" ld :test 'string=)
                (not (search "LDGRPSITE" ld :test 'string=)))
      (setf baystr baygrp))
    ;; (when (search "SAUX" baystr :test 'string=)
    ;;   (setf baystr "TGENE"))
    (ppcre:do-register-groups (one two three) (*regex-bay* baystr)
      (setq baystr (concatenate 'string one two "_" three)))
    (concatenate 'string "T_" baystr)))

(defvar *regex-cbo* (ppcre:create-scanner "SS(\\d+)$"))

(defun get-pano-function (dob)
  (let ((fun (lispiec::function-level dob))
        (bay (get-pano-bay dob)))
    (multiple-value-bind (tot vec)
        (ppcre:scan-to-strings *regex-cbo* fun)
      (if (string= "Tranche" fun)
          bay
          (if tot
              (concatenate 'string bay "/SS_1_" (aref vec 0) (write-to-string (1+ (parse-integer (aref vec 0)))))
              (concatenate 'string bay "/" fun))))))

(defun get-pano-ld (dob)
  (let* ((ldinst (lispiec::cdc-get-logical-device-inst-str dob))
         (ld (lispiec::cdc-get-logical-device-str dob))
         (ld-noprefix (if (string= "LD" (subseq ldinst 0 2))
                          (subseq ldinst 2)
                          ldinst))
         (ldreg1 (ppcre:create-scanner (concatenate 'string "(^\\w+)" ldinst)))
         (iedname (iec-server-get-name (iec-server dob)))
         (iednamenosuffix (subseq iedname 0 (- (length iedname) 1))))
    (ppcre:do-register-groups (one) (ldreg1 ld)
      (ppcre:do-register-groups (iedtype instance) (*regex-ied* one)
        (return-from get-pano-ld (concatenate 'string "LD_" ld-noprefix "_"
                                              (if (string= instance "2")
                                                  (if (and (string= iedtype "BPU")
                                                           (or (string= ldinst "LDPX")
                                                               (string= ldinst "LDPXPW")
                                                               (string= ldinst "LDPW")
                                                               (string= ldinst "LDPXVT")))
                                                      "2"
                                                      "1")
                                                  (if (and (string= iedtype "BPU")
                                                          (or (string= ldinst "LDPX")
                                                               (string= ldinst "LDPXPW")
                                                               (string= ldinst "LDPW")
                                                               (string= ldinst "LDPXVT")))
                                                      "2"
                                                      instance))
                                              (when (and (or (string= iedtype "SCU")
                                                             (string= iedtype "PIU")
                                                             (string= iedtype "TAC")
                                                             )
                                                         (not (search "CBO1" one :test 'string=)) ;exession bizarre à étudier
                                                         ;; (not (search "LDSUIED" ldinst :test 'string=))
                                                         )
                                                (progn
                                                  (if (and
                                                       (string= instance "1")
                                                       (site-get-ied-by-name *rspace* (concatenate 'string iednamenosuffix "2")))
                                                      "A"
                                                      (if (and
                                                           (string= instance "2"))
                                                          "B"
                                                          nil))))))))))

(defgeneric get-pano-address (obj)
  (:documentation "try to return Panorama address"))

(defgeneric get-pano-do-address (obj &optional use-opc-prefix)
  (:documentation "try to return Panorama DO address"))

(defgeneric get-main-attribute (obj)
  (:documentation "try to return main attribute"))

(defmethod get-main-attribute ((obj lispiec::cdc))
  "stVal")

(defmethod get-main-attribute ((obj lispiec::cdc-mv))
  (if (lispiec::mv-is-float obj)
      "mag_f"
      "mag_i"))

(defmethod get-main-attribute ((obj lispiec::cdc-act))
  "general")


(defmethod get-main-attribute ((obj lispiec::cdc-cmv))
  "cVal_mag_f")

(defmethod get-pano-do-address ((obj lispiec::cdc) &optional (use-opc-prefix t))
  (if (or (search "LD_SUIED" (get-pano-ld obj) :test 'string=)
          (search "LD_stem" (get-pano-ld obj) :test 'string=))
      (concatenate 'string
                   (when use-opc-prefix "ns=2;s=/Application")
                   "/Systeme/"
                   (iec-server-get-name (iec-server obj)) "/"
                   (get-pano-ld obj) "/"
                   (lispiec::cdc-get-logical-node-str obj) "/"
                   (lispiec::cdc-get-data-object-str obj))
      (concatenate 'string
                   (when use-opc-prefix "ns=2;s=/Application")
                   "/SITE/"
                   (lispiec::voltage-level obj) "/"
                   (get-pano-function obj) "/"
                   (get-pano-ld obj) "/"
                   (lispiec::cdc-get-logical-node-str obj) "/"
                   (lispiec::cdc-get-data-object-str obj))))


(defmethod get-pano-address ((obj lispiec::cdc))
  (concatenate 'string
               (get-pano-do-address obj) "."
               (get-main-attribute obj)))

(defmethod get-pano-addcause-address ((obj lispiec::cdc-xxc))
  (concatenate 'string
               (get-pano-do-address obj) "/"
               "AddCause.Value"))

(defmethod get-pano-validity-address ((obj lispiec::cdc))
  (concatenate 'string
               (get-pano-do-address obj) "/"
               "IEC_Validity"))


(defmethod get-pano-on-script ((obj lispiec::cdc-spc))
  (concatenate 'string
               (get-pano-do-address obj) "/"
               "CmdScriptOn.Active"))

(defmethod get-pano-off-script ((obj lispiec::cdc-spc))
  (concatenate 'string
               (get-pano-do-address obj) "/"
               "CmdScriptOff.Active"))


(defmethod get-pano-on-script ((obj lispiec::cdc-dpc))
  (concatenate 'string
               "ns=2;s=/Application"
               "/SITE/"
               (lispiec::voltage-level obj) "/"
               (get-pano-function obj) "/"
               (get-pano-ld obj) "/"
               "Scripts/Manoeuvre/OperClose.Active"))

(defmethod get-pano-off-script ((obj lispiec::cdc-dpc))
  (concatenate 'string
               "ns=2;s=/Application"
               "/SITE/"
               (lispiec::voltage-level obj) "/"
               (get-pano-function obj) "/"
               (get-pano-ld obj) "/"
               "Scripts/Manoeuvre/OperOpen.Active"))

(defun opc-prefix-conc (addr)
  (concatenate 'string "ns=2;s=/Application" addr))

  ;; exported functions


(defgeneric pano-get-value (opc-client obj)
  (:documentation "get Panorama value via OPC-UA"))

(defgeneric pano-get-node (opc-client obj)
  (:documentation "get Panorama value via OPC-UA"))

(defmethod pano-get-node (opc-client (obj lispiec::cdc))
  (opc-client-get-node opc-client (get-pano-address obj)))

(defmethod pano-get-node-validity (opc-client (obj lispiec::cdc))
  (opc-client-get-node opc-client (get-pano-validity-address obj)))

(defmethod pano-get-value (opc-client (obj lispiec::cdc))
  (node.get_value (pano-get-node opc-client obj)))

(defmethod pano-get-attributes (opc-client (obj lispiec::cdc))
  (node.get_attributes (pano-get-node opc-client obj)))

(defmethod pano-get-validity (opc-client (obj lispiec::cdc))
  (node.get_value (pano-get-node-validity opc-client obj)))

(defmethod pano-get-value (opc-client (obj lispiec::cdc-dpc))
  (foreign-enum-keyword 'lispiec-internal:dbpos (node.get_value (pano-get-node opc-client obj))))

(defmethod pano-get-value (opc-client (obj lispiec::cdc-mv))
  (float (node.get_value (pano-get-node opc-client obj))))

(defmethod pano-get-datavalue-str (opc-client (obj lispiec::cdc-qt))
  (let ((dv (node.get_data_value (pano-get-node opc-client obj))))
    (py4cl:python-method dv "__str__")))


(defmethod pano-get-timestamp-diff (opc-client (obj lispiec::cdc-qt))
  (abs (- (pano-get-timestamp opc-client obj)
          (cdc-get-timestamp obj))))

(defmethod pano-get-value-equal (opc-client (obj lispiec::cdc))
  (eql (cdc-get-value obj) (pano-get-value opc-client obj)))

(defmethod pano-get-value-equal (opc-client (obj lispiec::cdc-cmv))
  (eql (abs (cdc-get-value obj)) (pano-get-value opc-client obj)))

(defmethod pano-get-validity-equal (opc-client (obj lispiec::cdc-qt))
  (eql (cdc-get-validity obj) (case (pano-get-validity opc-client obj)
                                (3 0)
                                (1 1)
                                (2 3))))

(defmethod pano-compare-data-object (opc-client (obj lispiec::cdc))
  (handler-case (and (pano-get-value-equal opc-client obj)
                      (pano-get-validity-equal opc-client obj)
                      (eql 0 (pano-get-timestamp-diff opc-client obj)))
    (python-error ()
      (format t "~& ERROR: OPC value not found ~a." (lispiec::obj-reference-str obj))
      nil)))

(defmethod pano-get-addcause (opc-client (obj lispiec::cdc-xxc))
  (node.get_value (opc-client-get-node opc-client (get-pano-addcause-address obj))))

(defmethod pano-set-on (opc-client (obj lispiec::cdc-spc))
  (node.set_value (opc-client-get-node opc-client (get-pano-on-script obj)) t))

(defmethod pano-set-off (opc-client (obj lispiec::cdc-spc))
  (node.set_value (opc-client-get-node opc-client (get-pano-off-script obj)) t))

(defmethod pano-set-on (opc-client (obj lispiec::cdc-dpc))
  (node.set_value (opc-client-get-node opc-client (get-pano-on-script obj)) t))

(defmethod pano-set-off (opc-client (obj lispiec::cdc-dpc))
  (node.set_value (opc-client-get-node opc-client (get-pano-off-script obj)) t))

(defun start-rspace-for-test (ied-path port)
  (when (null *rspace*)
    (make-rspace ied-path port)
    (site-start-ieds *rspace*)
    (define-other-handlers)
    (loop while (not (is-site-running *rspace*))
          do (sleep 2))
    (start-cmvs))

  (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* ".*" "LPHD0" "PhyHealth") 1))
