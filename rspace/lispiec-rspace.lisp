(in-package #:lispiec-rspace)

(defparameter *rspace* nil)
(defparameter *toggle-loop-cycle* 1.0)
(defparameter *toggle-loop-run* nil)


(defparameter *toggle-number-cdc* 66)
(defparameter *toggle-number-cdc-fictif* 128)


(defparameter *mv-loop-cycle* 5.0)
(defparameter *mv-loop-run* nil)
(defparameter *dummy-loop-cycle* 10.0)
(defparameter *dummy-loop-run* nil)


(defparameter *ieds* nil)
(defparameter *ieds-number* nil)
(defparameter *slynk-tcp-port* nil)
(defparameter *bcus* nil)

(defun make-rspace (cfg-path &optional (port 10000) (ds-regex "(GSE|DQCE|DQPO|CYPO|DQGW)$"))
  (declare (type string cfg-path)
           (type integer port))
  (setf *slynk-tcp-port* port)
  (setf *rspace* (make-site "rspace" cfg-path port ds-regex))
  (sleep 5.0)
  (make-ieds *rspace*)
  (when (eq (length *ieds*) 0)
    (format t "Initial configuration executed. Better restart it.")
    (uiop:quit))
  (setf *ieds-number* (length *ieds*)))

(defun make-ieds (site)
  (let ((ieds (mapcar #'(lambda (row)
                          (concatenate 'string "*IED-" row "*"))
                      (site-get-ieds-name site))))
    (mapcar #'(lambda (n i)
                ;(format t "int name it ~a and value is ~a ~%" n i)
                (set (intern n :lispiec-rspace) i))
            ieds
            (setf *ieds* (site-get-ieds site)))))


(defun make-bcu (ied)
  (let ((iedname-prefix (concatenate 'string
                                     "*DO-"
                                     (iec-server-get-bay ied)
                                     "-")))
    (set (intern (concatenate 'string iedname-prefix "DJ*") :lispiec-rspace)
         (iec-server-get-dataobject ied "LDCMDDJ" "CSWI1" "Pos") )
    (set (intern (concatenate 'string iedname-prefix "SA1*")  :lispiec-rspace)
         (iec-server-get-dataobject ied "LDCMDSA1" "CSWI0" "Pos"))
    (set (intern (concatenate 'string iedname-prefix "SL*")  :lispiec-rspace)
         (iec-server-get-dataobject ied "LDCMDSL" "CSWI0" "Pos"))
    (set (intern (concatenate 'string iedname-prefix "DJ-ENAOPN*") :lispiec-rspace)
         (iec-server-get-dataobject ied "LDCMDDJ" "CILO0" "EnaOpn"))
    (set (intern (concatenate 'string iedname-prefix "DJ-ENACLS*") :lispiec-rspace)
         (iec-server-get-dataobject ied "LDCMDDJ" "CILO0" "EnaCls"))
    (set (intern (concatenate 'string iedname-prefix "MODEXPF*") :lispiec-rspace)
         (iec-server-get-dataobject ied "LDMODEXPF1" "LLN0" "LocSta"))
    (set (intern (concatenate 'string iedname-prefix "MODEXPF-RESULT*") :lispiec-rspace)
         (iec-server-get-dataobject ied "LDMODEXPF1" "IHMI0" "LocSta"))
    (set (intern (concatenate 'string "*MV-" (iec-server-get-name ied) "*") :lispiec-rspace)
         (iec-server-get-dataobjects-of-cdc ied 'cdc-mv))
    (set (intern (concatenate 'string "*CMV-" (iec-server-get-name ied) "*") :lispiec-rspace)
         (iec-server-get-dataobjects-of-cdc ied 'cdc-cmv))
    (set (intern (concatenate 'string iedname-prefix "RSE*")  :lispiec-rspace)
         (iec-server-get-dataobject ied "LDRSE" "LLN0" "Mod"))
    (push ied *bcus*)))

(defgeneric iec-server-get-number-dos (ied)
  (:documentation "get number of data-objects in ied")
  (:method ((ied iec-server))
    (length (iec-server-get-dataobjects ied))))

(defun get-random-dataobjects-from-ied (ied num total-site)
  (if (lispiec::enable ied)
      (let* ((lst (remove-if #'(lambda (d) (or (typep (cdr d) 'cdc-lpl)
                                               (typep (cdr d) 'cdc-vss)
                                               (typep (cdr d) 'lispiec::cdc-org)))
                             (iec-server-get-dataobjects ied)))
             (len (length lst))
             (ratiodo (/ len total-site)))
        (remove-duplicates
         (loop for i from 1 to (ceiling (* num ratiodo))
               collect (nth (random len) lst))))
      '()))

(defun toggle-random-dataobjects (num-total)
  (let* ((total-site (length (site-get-dataobjects-by-regex *rspace* "" "" "^(?!InRef[0-9]+)"))))
    (mapcar #'(lambda (ied)
                (list-dataobjects-toggle
                 (get-random-dataobjects-from-ied
                  ied
                  num-total
                  total-site)))
            *ieds*)))

(defun toggle-ied-cdcs (site ied-partial-name cdc)
  (let ((bcus (find-ied-list-by-name site ied-partial-name)))
    (mapcar #'list-dataobjects-toggle (mapcar #'(lambda (r) (iec-server-get-dataobjects-of-cdc r cdc)) bcus))))

(defun toggle-bcu-mvs ()
  (toggle-ied-cdcs *rspace* "BCU1" 'cdc-mv))

(defun toggle-bcu-cmvs ()
  (toggle-ied-cdcs *rspace* "BCU1" 'cdc-cmv))


(defun start-toggle-cycle ()
  (setf *toggle-loop-run* t)
  (bt:make-thread (lambda ()
                    (loop while *toggle-loop-run* do
                      (progn
                        (deeds:do-issue deeds::cycle-trigger
                          :message "trigger loop"
                          :loop (event-loop *rspace*))
                        (sleep *toggle-loop-cycle*))))
                  :name "rspace-cycle"))



(defun stop-toggle-cycle ()
  (setf *toggle-loop-run* nil))

(defun start-mv-cycle ()
  (setf *mv-loop-run* t)
  (bt:make-thread (lambda ()
                    (loop while *mv-loop-run* do
                      (progn
                        (deeds:do-issue deeds::cycle-trigger
                          :message "mv loop"
                          :loop (event-loop *rspace*))
                        (sleep *mv-loop-cycle*))))
                  :name "rspace-mv-cycle"))



(defun stop-mv-cycle ()
  (setf *mv-loop-run* nil))


(defun start-dummy-cycle ()
  (setf *dummy-loop-run* t)
  (bt:make-thread (lambda ()
                    (loop while *dummy-loop-run* do
                          (sleep *dummy-loop-cycle*)))
                  :name "rspace-dummy-cycle"))


(defun stop-dummy-cycle ()
  (setf *dummy-loop-run* nil))



(defun update-modexpf-resultant (obj)
  (let* ((state (cdc-get-value obj))
         (modexpfsres (site-get-dataobjects-by-regex *rspace* "LDMODEXPF" "IHMI" "LocSta"))
         (modexpfs (site-get-dataobjects-by-regex *rspace* "LDMODEXPF" "LLN0" "LocSta"))
         (modexpfsvalues (mapcar #'cdc-get-value (mapcar #'cdr modexpfs)))
         (result (mapcar #'(lambda (x y) (or x y)) (make-list (length modexpfs) :initial-element state) modexpfsvalues)))
    (iter
      (for i in modexpfsres)
      (for j in result)
      (if j
          (cdc-set-on (cdr i))
          (cdc-set-off (cdr i))))))


(defun get-modexp-test (dobj) ;; XX added to avoid this function to be called
  (let* ((dos (remove-if #'(lambda (row) (or (search "LDPXVT" (car row) :test 'char=)
                                             (search "TAC1" (car row) :test 'char=)))
                         (get-cdcs-same-bay dobj "" "LLN0" "Beh" *rspace*)))
         (dosv (mapcar #'(lambda (row) (cdc-get-value (cdr row))) dos))
         (dosvtest (mapcar #'(lambda (row) (or (eq 3 row) (eq 4 row) (eq 5 row))) dosv)))
    (every #'identity dosvtest)))

(defun get-modexp-test-values (dobj)
  (let* ((dos (remove-if #'(lambda (row) (search "LDGRP" (car row) :test 'char=)) (get-cdcs-same-bay dobj "" "LLN0" "Beh" *rspace*)))
         (dosv (mapcar #'(lambda (row) (cdc-get-value (cdr row))) dos))
         ;; (dosvtest (mapcar #'(lambda (row) (or (eq 3 row) (eq 4 row) (eq 5 row))) dosv))
         )
    (mapcar #'cons (mapcar #'car dos) dosv)))


(defun set-modexp-maint-obj (dobj)
  (let ((maint-obj (cdr (get-cdcs-same-bay-by-alias dobj "TRANCHE MAINT" *rspace*)))
        (test (get-modexp-test dobj)))
    (when maint-obj
      (if test
          (cdc-set-on maint-obj)
          (cdc-set-off maint-obj)))))

(defun exit-rspace (&optional (light-exit nil))
  (site-stop-ieds *rspace*)
  (site-client-stop-ieds *rspace*)
  (site-destroy *rspace*)
  (unless light-exit
    (site-slynk-stop-server *rspace* *slynk-tcp-port*)
    (sb-ext:exit :code 0 :timeout 10)))

;; (let ((pack (find-package :lispiec-rspace)))
;;   (do-all-symbols (sym pack) (when (eql (symbol-package sym) pack) (export sym))))
