(in-package :lispiec-pano)

(setf lispiec::log-mode nil)
(start-python)
(sleep 3.0)

(asdf:load-system :mssql)

(defparameter *dos-not-to-be-tested*
  (concatenate 'list nil
               (mapcar #'car (site-get-dataobjects-by-regex *rspace* "" "LSVS" "Beh"))
               (mapcar #'car (site-get-dataobjects-by-regex *rspace* "LDGW$" "LCCH" ""))
               (mapcar #'car (site-get-dataobjects-by-regex *rspace* "LDTDEC$" "LLN0" ""))
               ;; (mapcar #'car (site-get-dataobjects-by-regex *rspace* "" "LLN0" "Beh"))
               (mapcar #'car (site-get-dataobjects-by-regex *rspace* "LDPAP$" "" "")) ;; (mapcar #'car (site-get-dataobjects-by-regex *rspace* "PMED3BLANQ1BCU1LDSUSL" "" ""))
               ;; (mapcar #'car (site-get-dataobjects-by-regex *rspace* "TAC1[LE]" "" ""))
               ;; (mapcar #'car (site-get-dataobjects-by-regex *rspace* "SS6$" "" ""))
               ))

(defparameter *db* nil)
(defparameter *client* (opc-client-create "opc.tcp://192.168.2.50:9080"))
;; (defparameter *clienOBt* (opc-client-create "opc.tcp://12.0.1.50:9080"))
;; (defparameter *client* (opc-client-create "opc.tcp://10.195.192.105:9080"))




(defun conv-db-t (myquery)
  (if myquery
      (let  ((date-str (nth 5 myquery)) (mili (nth 4 myquery)))
        (+ (* 1000 (local-time:timestamp-to-unix (local-time:parse-timestring date-str :date-separator #\- :allow-missing-elements t :date-time-separator #\SPACE))) mili))
      0))

(defun pano-db-purge (connection)
  ;; (mssql:query "DELETE FROM dbo.Alarms WHERE 1=1")
  (mssql:query "TRUNCATE TABLE dbo.Alarms" :connection connection))

(defgeneric pano-db-query-t (connection cdc))
(defmethod pano-db-query-t (connection (cdc cdc-dpc))
  (let* ((addr (concatenate 'string (get-pano-do-address cdc nil) "/%JdB"))
         (addrq (concatenate 'string (get-pano-do-address cdc nil) "/Quality/Detail"))
         (que (first (mssql:query (concatenate 'string "SELECT TOP 10 Name,Enum_Status,Enum_Transition,Reception_DateTime,Change_Milli,CONVERT(varchar,UTC_App_DateTime,20) FROM RSPACE.dbo.Alarms WHERE Enum_Transition != 13 and Name LIKE '" addr "' ORDER BY UTC_App_DateTime DESC,Change_Milli DESC") :connection connection)))
         (queq (first (mssql:query (concatenate 'string "SELECT TOP 10 Name,Enum_Status,Enum_Transition,Reception_DateTime,Change_Milli,CONVERT(varchar,UTC_App_DateTime,20) FROM RSPACE.dbo.Alarms WHERE Enum_Transition != 13 and Name LIKE '" addrq "' ORDER BY UTC_App_DateTime DESC,Change_Milli DESC") :connection connection))))
    (max
     (conv-db-t queq)
     (conv-db-t que))))

(defmethod pano-db-query-t (connection (cdc cdc-ens))
  (let* ((addr (concatenate 'string (get-pano-do-address cdc nil) "/%JdB"))
         (addrq (concatenate 'string (get-pano-do-address cdc nil) "/Quality/Detail"))
         (que (first (mssql:query (concatenate 'string "SELECT TOP 10 Name,Enum_Status,Enum_Transition,Reception_DateTime,Change_Milli,CONVERT(varchar,UTC_App_DateTime,20) FROM RSPACE.dbo.Alarms WHERE Enum_Transition != 13 and Name LIKE '" addr "' ORDER BY UTC_App_DateTime DESC,Change_Milli DESC") :connection connection)))
         (queq (first (mssql:query (concatenate 'string "SELECT TOP 10 Name,Enum_Status,Enum_Transition,Reception_DateTime,Change_Milli,CONVERT(varchar,UTC_App_DateTime,20) FROM RSPACE.dbo.Alarms WHERE Enum_Transition != 13 and Name LIKE '" addrq "' ORDER BY UTC_App_DateTime DESC,Change_Milli DESC") :connection connection))))
    (max
     (conv-db-t queq)
     (conv-db-t que))))


(defmethod pano-db-query-t (connection (cdc cdc-sps))
  (let* ((addr (concatenate 'string (get-pano-do-address cdc nil) "/%JdB_O%"))
         (addrq (concatenate 'string (get-pano-do-address cdc nil) "/Quality/Detail"))
         (que (first (mssql:query (concatenate 'string "SELECT TOP 10 Name,Enum_Status,Enum_Transition,Reception_DateTime,Change_Milli,CONVERT(varchar,UTC_App_DateTime,20) FROM RSPACE.dbo.Alarms WHERE Enum_Transition != 13 and Name LIKE '" addr "' ORDER BY UTC_App_DateTime DESC,Change_Milli DESC") :connection connection)))
         (queq (first (mssql:query (concatenate 'string "SELECT TOP 10 Name,Enum_Status,Enum_Transition,Reception_DateTime,Change_Milli,CONVERT(varchar,UTC_App_DateTime,20) FROM RSPACE.dbo.Alarms WHERE Enum_Transition != 13 and Name LIKE '" addrq "' ORDER BY UTC_App_DateTime DESC,Change_Milli DESC") :connection connection))))
    (max
     (conv-db-t queq)
     (conv-db-t que))))

(defun compare-db (dobj-db)
  (if (or (and (typep dobj-db 'cdc-sps)
               ;(not (typep dobj-db 'cdc-act))
               )
          (typep dobj-db 'cdc-dpc)
          (and (typep dobj-db 'cdc-ens) (not (typep dobj-db 'cdc-inc)))
          )
      (let ((tt (cdc-get-timestamp dobj-db))
            (dbtt (pano-db-query-t *db* dobj-db)))
        (eq tt dbtt))
      t))


(defun exec-after ()
  (cdc-get-value *do-3theix1-dj*)
  (pano-get-value *client* *do-3theix1-dj*)
  (pano-get-node *client* *do-3theix1-dj*)


  (pano-get-timestamp-diff *client* *do-3theix1-dj*)
  (pano-get-value-equal *client* *do-3theix1-dj*)


  (cdc-get-value *do-tg-dfposte*)
  (pano-get-value *client* *do-tg-dfposte*)

  (pano-get-validity *client* *do-tg-dfposte*)
  (cdc-get-validity *do-tg-dfposte*))

(defun show-lds ()
  (let* ((ieds (site-get-ieds *rspace*))
         (dos (alexandria:flatten (mapcar #'(lambda (r) (mapcar #'cdr (iec-server-get-dataobjects-of-cdc r 'cdc-spc))) ieds))))
    (iter:iter (iter:for doj iter:in dos)
      (iter:collect (get-pano-function doj)))
    ))




(defun nshuffle (sequence)
  (loop for i from (length sequence) downto 2
        do (rotatef (elt sequence (random i))
                    (elt sequence (1- i))))
  sequence)



(defun toggle-all (&optional total-time-to-change-all only-ens)
  (let* ((dos (nshuffle
               (remove-if-not #'(lambda (x)
                                  (iec-server-is-running (iec-server (cdr x))))
                              (site-get-dataobjects-by-regex *rspace* "" "" "^(?!xxBeh)")
                              )))
         (ll (length dos))
         (pause-between (when total-time-to-change-all
                          (/ total-time-to-change-all ll))))
    (mapcar #'(lambda (r) (progn (when total-time-to-change-all
                                   (sleep pause-between))
                                 (cdc-toggle (cdr r))))
            (if only-ens
                (remove-if-not #'(lambda (d) (typep (cdr d) 'cdc-ens)) dos)
                dos))))


(defun init-all (&optional total-time-to-change-all)
  (let* ((dos (nshuffle
               (remove-if-not #'(lambda (x)
                                  (iec-server-is-running (iec-server (cdr x))))
                              (site-get-dataobjects-by-regex *rspace* "" "" "^(?!xxBeh)")
                              )))
         (ll (length dos))
         (pause-between (when total-time-to-change-all
                          (/ total-time-to-change-all ll))))
    (mapcar #'(lambda (r) (progn (when total-time-to-change-all
                                   (sleep pause-between))
                                 (lispiec::cdc-init (cdr r))))
            dos)))

(defun valid-all (&optional total-time-to-change-all)
  (let* ((dos (nshuffle
               (remove-if-not #'(lambda (x) (iec-server-is-running (iec-server (cdr x))))
                              (site-get-dataobjects-by-regex *rspace* "" "" ""))))
         (ll (length dos))
         (pause-between (when total-time-to-change-all
                          (/ total-time-to-change-all ll))))
    (mapcar #'(lambda (r) (progn (when total-time-to-change-all
                                   (sleep pause-between))
                                 (lispiec::cdc-set-valid (cdr r))))
            dos)))

(defun invalid-all (&optional total-time-to-change-all)
  (let* ((dos (nshuffle
               (remove-if-not #'(lambda (x) (iec-server-is-running (iec-server (cdr x))))
                              (site-get-dataobjects-by-regex *rspace* "" "" ""))))
         (ll (length dos))
         (pause-between (when total-time-to-change-all
                          (/ total-time-to-change-all ll))))
    (mapcar #'(lambda (r) (progn (when total-time-to-change-all
                                   (sleep pause-between))
                                 (lispiec::cdc-set-invalid (cdr r))))
            dos)))

(defun test-cdc (obj &optional with-bd)
  (let ((objname (lispiec::obj-reference-str obj))
        (doname (lispiec::cdc-get-data-object-str obj))
        res)
    (format t "dobj: ~a (~a) ~&" objname (get-pano-ld obj))
    (unless (member objname *dos-not-to-be-tested* :test #'string=)
      (unless (setf res (pano-compare-data-object *client* obj))
        (sleep 1.0)
        (unless (setf res (pano-compare-data-object *client* obj))
          (sleep 2.5)
          (setf res (pano-compare-data-object *client* obj))))
      (parachute:true res "Test comparing Data Object ~a with value ~a." objname (cdc-get-value obj))
      (when (and with-bd res (string/= doname "Beh") (string/= doname "Mod"))
        (unless (setf res (compare-db obj))
          (sleep 0.4)
          (unless (setf res (compare-db obj))
            (sleep 1.0)
            (unless (setf res (compare-db obj))
              (sleep 2.5)
              (setf res (compare-db obj))
              )
            ))
        (parachute:true res "Test comparing DB on ~a with value ~a." objname (cdc-get-value obj))))))

(defun test-cdcs (cdc-type &optional with-bd)
  (let* ((dos (nshuffle
               (remove-if-not #'iec-server-is-running
                              (remove-if-not #'(lambda (i) (typep (cdr i) cdc-type))
                                             (site-get-dataobjects-by-regex *rspace* "" "" ""))
                              :key #'(lambda (r) (iec-server (cdr r)))))))
    (mapcar #'(lambda (obj) (test-cdc (cdr obj) with-bd))
            dos)
    ))

(defun test-ens-all-values (obj &optional with-bd (sleep-time 0.6))
  (when (typep obj 'cdc-ens)
    (let ((changes (length (lispiec::cdc-get-enums obj))))
      (when (> changes 3)
        (iter:iter (iter:for i from 1 to (1- changes))
          (cdc-toggle obj)
          (sleep sleep-time)
          (test-cdc obj with-bd))))))

(define-test test-dobj-values-ens-all-values
  (let* ((dos (nshuffle
               (remove-if-not #'iec-server-is-running
                              (remove-if-not #'(lambda (i) (typep (cdr i) 'cdc-ens))
                                             (site-get-dataobjects-by-regex *rspace* "" "" ""))
                              :key #'(lambda (r) (iec-server (cdr r)))))))
    (mapcar #'(lambda (obj) (test-ens-all-values (cdr obj) t))
            dos)))

(define-test test-dobj-values)

(defparameter *with-bd* nil)

(define-test test-dobj-values-sps
  :parent test-dobj-values
  (test-cdcs 'cdc-sps *with-bd*))

(define-test test-dobj-values-dpc
  :parent test-dobj-values
  (test-cdcs 'cdc-dpc *with-bd*))

(define-test test-dobj-values-ens
  :parent test-dobj-values
  (test-cdcs 'cdc-ens *with-bd*))

(define-test test-dobj-values-mv
  :parent test-dobj-values
  (test-cdcs 'cdc-mv *with-bd*))

(define-test test-dobj-values-cmv
  :parent test-dobj-values
  (test-cdcs 'cdc-cmv *with-bd*))

(defun run-tests-cdc (&optional with-bd)
  (let ((old-with *with-bd*))
    (setf *with-bd* with-bd)
    (let ((ret (test 'test-dobj-values)))
      (setf *with-bd* old-with)
      ret)))

(defun run-tests-ens (&optional with-bd)
  (let ((old-with *with-bd*))
    (setf *with-bd* with-bd)
    (let ((ret (test 'test-dobj-values-ens)))
      (setf *with-bd* old-with)
      ret)))



(defparameter *list-dobjs-changed* nil)

(with-site-define-cycle-handler *rspace* cycle "trigger loop"
  (let ((dobjs-to-test (mapcar #'(lambda (d) (site-get-dataobject-by-ref *rspace* d)) *list-dobjs-changed*)))
    (when dobjs-to-test
      (mapcar #'(lambda (d) (unless (lispiec-pano::pano-compare-data-object lispiec-pano::*client* d)
                              (format t "~& ERROR: dobj ~a not equal on t=~a !" (lispiec::obj-reference-str d) (local-time:unix-to-timestamp (floor (/ (cdc-get-timestamp d) 1000))))))
              dobjs-to-test)))
  (setf *list-dobjs-changed* (remove-if-not #'(lambda (r) (search "/" r :test #'char=))
                                            (remove-if-not #'stringp (alexandria:flatten
                                                                      (toggle-random-dataobjects *toggle-number-cdc*)
                                                                      )))))

;; (with-site-define-cycle-handler *rspace* cycle "trigger loop"  (toggle-random-dataobjects *toggle-number-cdc*))


(defparameter *list-gtw*
  (concatenate 'list
               ;; (site-get-dataobjects-by-regex *rspace* "SCU1LDITFTG" "GAPC4" "Ind5")
               ;; (site-get-dataobjects-by-regex *rspace* "SCU1LDITFTG" "CALH1" "GrInd")
               ;; (site-get-dataobjects-by-regex *rspace* "SCU1LDITFTG" "CALH9" "GrWrn")
               ;; (site-get-dataobjects-by-regex *rspace* "CBO1BCU1LDMQUB1" "CB0\+1001FXUT" "Op")
               ;; (site-get-dataobjects-by-regex *rspace* "CBO1BCU1LDMQUB1" "CB0\+1003FXUT" "Op")
               (site-get-dataobjects-by-regex *rspace* "BLANQ1BCU1LDCMDDJ" "CSWI1" "Pos")))

;; (list-dataobjects-get-value *list-gtw*)
;; (mapcar #'(lambda (d v) (cdc-set-value (cdr d) v)) *list-gtw* '(nil nil nil nil nil :dbpos-off :dbpos-off))
;; (mapcar #'(lambda (r) (lispiec::cdc-get-main-attribute-triggerops (cdr r))) *list-gtw*)

;; (defparameter *gtw-cycle* 0)
;; (with-site-define-cycle-handler *rspace* test-gtw "trigger loop"
;;   (let ((cycle-len (length *list-gtw*)))
;;     (when (eql *gtw-cycle* cycle-len )
;;       (setf *gtw-cycle* 0))
;;     (cdc-toggle (cdr (nth *gtw-cycle* *list-gtw*)))
;;     ;; (format t "~& Changing DO ~a " (car (nth *gtw-cycle* *list-gtw*)))
;;     (incf *gtw-cycle*)))

(with-site-define-cycle-handler *rspace* testlog "trigger loop"
  (list-dataobjects-toggle *list-gtw*))
;; (setf lispiec-rspace:*toggle-loop-cycle* (* 60 10))


;; (defparameter *is-even* nil)
;; (with-site-define-cycle-handler *rspace* h-cycle-mv "mv loop"
;;   (let ((vref (/ 63000 (sqrt 3))))
;;     (if lispiec-pano::*is-even*
;;         (start-cmvs (* 0.9 vref))
;;         (start-cmvs vref))
;;     (setf lispiec-pano::*is-even* (not lispiec-pano::*is-even*))))

(defun start-perf-test (minutes &optional (vars-per-cycle 61) (cycle-sec 61.0))
  (lispiec::site-clear-all-handlers *rspace*)
  (mapcar #'lispiec::iec-server-clean-inrefs (site-get-ieds *rspace*))
  (setf *list-dobjs-changed* nil)
  (setf lispiec-rspace:*toggle-loop-cycle* cycle-sec)
  (setf lispiec-rspace:*toggle-number-cdc* vars-per-cycle)
  (start-mv-cycle)
  (start-toggle-cycle)
  (sleep (* 60 minutes))
  (stop-mv-cycle)
  (stop-toggle-cycle)
  ;; (sleep 60.0)
  ;; (test 'test-dobj-values)
  )



(defmethod summarize-short ((report plain))
  (let ((failures (results-with-status :failed report)))
    (format (output report)
            "~&~%~
             ;; Summary:~%~
             Passed:  ~4d~%~
             Failed:  ~4d~%~
             Skipped: ~4d~%"
            (length (parachute::filter-test-results (results-with-status :passed report)))
            (length (parachute::filter-test-results failures))
            (length (parachute::results-with-status :skipped report
                                                    )))
    report))

(defun make-jdb-test (&optional (toggle-all-time 15) (wait-base-stable 65))
  (start-connections)
  (lispiec::site-clear-all-handlers *rspace*)
  (format t "purging db... ~%")
  (pano-db-purge *db*)
  (sleep 2)

  (let ((first-test (run-tests-cdc)))
    (if (eql :passed (status first-test))
        (progn
          (toggle-all 10)
          (sleep wait-base-stable)
          (format t "toggling all data objects... ~%")
          (toggle-all (/ toggle-all-time 1))
          (site-change-orcat-local *rspace*)
          ;; (sleep wait-base-stable)
          ;; (toggle-all (/ toggle-all-time 2))
          ;; (site-change-orcat-local *rspace*)
          ;; (init-all toggle-all-time)
          ;; (site-change-orcat-local *rspace*)
          (format t "wait for db to get stable... ~%")
          (sleep wait-base-stable)
          (let ((sec-test (run-tests-cdc t)))
            (if (eql :passed (status sec-test))
                (progn
                  (test 'test-dobj-values-ens-all-values)
                  (iter:iter (iter:for i from 1 to 3)
                    (toggle-all toggle-all-time t)
                    (sleep wait-base-stable)
                    (run-tests-ens t)))
                (summarize-short sec-test))))
        (summarize-short first-test))))



(defun get-pano-subs-dos ()
  (nshuffle
   (remove-if #'(lambda (r)
                  (or (and (string= "" (lispiec::alias r))
                           (string= "" (lispiec::uuid r)))
                      (typep r 'cdc-act)
                      (typep r 'cdc-mv)
                      (not (iec-server-is-running (iec-server r)))
                      (search "RecCycMod" (lispiec::obj-reference-str r) :test #'string=)))
              (site-get-dataobjects-by-regex *rspace* "" "" "")
              :key #'cdr)))




(defun get-dd (ref)
  (setf dd (site-get-dataobject-by-ref *rspace* ref)))

(defun test-pano-substitution (dobj)
  (let ((pano-add (get-pano-do-address dobj))
        (dobj-name (lispiec::obj-reference-str dobj)))
    (let ((force-on (make-button *client*
                                 (concatenate 'string
                                              pano-add
                                              "/ForceOn")))
          (force-off (make-button *client*
                                 (concatenate 'string
                                              pano-add
                                              "/ForceOff")))
          (unforce (make-button *client*
                                 (concatenate 'string
                                              pano-add
                                              "/Unforce"))))
      ;; (parachute:finish (button-click unforce)
      ;;     "Button Unforce failed on ~a."
      ;;     dobj-name)
      ;; (sleep 1.6)
      ;; (parachute:false (lispiec::cdc-is-q-sub dobj)
      ;;                  "Data Object ~a should be unforced." dobj-name)
      (parachute:true (lispiec::is-sub dobj) "DO ~a without subena." dobj-name)
      (when (and (lispiec::is-sub dobj)
                 (string/= "Mod" (lispiec::cdc-get-data-object-str dobj)))
        (sleep 0.7)
        (parachute:finish (button-click force-on)
                          "Button ForceOn failed on ~a."
                          dobj-name)
        (sleep 0.7)
        (unless (lispiec::cdc-is-q-sub dobj) (sleep 1.1))
        (parachute:true (lispiec::cdc-is-q-sub dobj)
                        "Data Object ~a should be forced." dobj-name)
        (sleep 0.7)
        (parachute:finish (button-click unforce)
                          "Button Unforce failed on ~a."
                          dobj-name)
        (when (lispiec::cdc-is-q-sub dobj)
          (sleep 1.1))
        (parachute:false (lispiec::cdc-is-q-sub dobj)
                         "Data Object ~a should be unforced." dobj-name)

        ;; (unless (lispiec::cdc-is-q-sub dobj) (sleep 1.1))
        ;; (parachute:true (lispiec::cdc-is-q-sub dobj)
        ;;                 "Data Object ~a should be forced." dobj-name)
        ;; (parachute:finish (button-click unforce)
        ;;                   "Button Unforce failed on ~a."
        ;;                   dobj-name)
        ;; (sleep 0.7)
        ;; (when (lispiec::cdc-is-q-sub dobj) (sleep 1.1))
        ;; (parachute:false (lispiec::cdc-is-q-sub dobj)
        ;;                  "Data Object ~a should be unforced." dobj-name)
        ))))

(define-test test-pano-substitutions
  (let ((dobs-to-test (mapcar #'cdr
                              ;; (subseq)
                              (get-pano-subs-dos)
                              )))
    (mapcar #'test-pano-substitution dobs-to-test)))

(defun start-connections ()
  (opc-client-connect *client*))

(setf *db* (mssql:connect "RSPACE" "sa" "R5p4ce@@@" "192.168.2.2" :external-format :utf-8b))
;; (defparameter *db* (mssql:connect "RSPACE" "sa" "Rspace_A2021" "10.195.192.105" :external-format :utf-8b))
