(defpackage #:lispiec-rspace
  (:use #:cl #:lispiec  #:iterate #:clog)
  (:nicknames "rspace")
  (:export
   #:main
   #:main-as-thread
   #:*bcus*
   #:*cilos*
   #:*dummy-loop-cycle*
   #:*dummy-loop-run*
   #:*ieds*
   #:*ieds-number*
   #:*mv-loop-cycle*
   #:*mv-loop-run*
   #:*rspace*
   #:*toggle-loop-cycle*
   #:*toggle-loop-run*
   #:*toggle-number-cdc*
   #:*toggle-number-cdc-fictif*
   #:*tranche-maintenance*
   #:define-handlers
   #:define-other-handlers
   #:exit-rspace
   #:export-cdc-file
   #:find-ied-fullname
   #:find-ied-list-by-name
   #:get-cdcs-same-bay
   #:get-cdcs-same-bay-by-alias
   #:get-modexp-test-values
   #:get-random-dataobjects-from-ied
   #:iedname-prefix
   #:ieds
   #:make-bcu
   #:make-ieds
   #:make-rspace
   #:propagate-values
   #:propagate-values-same-bay
   #:resto
   #:set-modexp-maint-obj
   #:start-cmvs
   #:start-dummy-cycle
   #:start-mv-cycle
   #:start-toggle-cycle
   #:start-web
   #:start-zmq
   #:stop-dummy-cycle
   #:stop-mv-cycle
   #:stop-toggle-cycle
   #:to-alias
   #:to-do-regex
   #:to-ld-regex
   #:to-ln-regex
   #:toggle-bcu-cmvs
   #:toggle-bcu-mvs
   #:toggle-random-dataobjects
   #:update-modexpf-resultant
   ))

(defpackage #:guibot
  (:use #:cl #:cffi #:py4cl)
  (:nicknames "bot"))

(defpackage #:lispiec-pano
  (:use #:cl #:cffi #:lispiec #:lispiec-rspace #:py4cl #:parachute #:guibot)
  (:nicknames "pano")
  (:export #:get-pano-address
           #:start-rspace-for-test
                                        ;#:get-pano-do-address
                                        ;#:get-pano-addcause-address
                                        ;#:get-pano-on-script
                                        ;#:get-pano-off-script
           #:pano-get-value
           #:pano-get-node
           #:pano-get-addcause
           #:pano-set-on
           #:pano-set-off
           #:button
           #:button-get-node
           #:button-click
           #:client
           #:node
           #:opc-client-create
           #:opc-client-connect
           #:opc-client-disconnect
           #:opc-client-get-node
           #:opc-client-get-value
           #:opc-client-set-value))
