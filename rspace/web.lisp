(in-package :lispiec-rspace)

(defun on-new-window (body)
  (let* ((h1 (create-section body :h1 :content "Spacio"))
         (tmp (create-section body :h3 :content "Memorized Data Objects:"))
         (do1 (create-button body :content "Data Object 1"))
         (do2 (create-button body :content "Data Object 2"))
         (do3 (create-button body :content "Data Object 3"))
         (doactive do1)
         (listdos nil)
         (d1 (create-div body))
         (tmp (create-section d1 :h3 :content "Select Data Object by Regex:"))
         (tmp (create-br do1))
         (f1 (create-form d1))
         (fe1 (create-form-element f1 :text
                                   :label (create-label f1 :content "Type LD:")))
         (tmp (create-br f1))
         (fe2 (create-form-element f1 :text
                                   :label (create-label f1 :content "Type LN:")))
         (tmp (create-br f1))
         (fe3 (create-form-element f1 :text
                                   :label (create-label f1 :content "Type DO:")))
         (tmp (create-br f1))
         (tmp (create-form-element f1 :submit :value "OK"))
         (tmp (create-form-element f1 :reset :value "Start Again"))
         (d4 (create-div body))
         (tmp (create-section d4 :h3 :content "Select Data Object from Ref:"))
         (f3 (create-form d4))
         (fe5 (create-form-element f3 :text :label (create-label f3 :content "DataObject Ref: ")))
         (tmp (create-form-element f3 :submit :value "OK"))
         (tmp (create-form-element f3 :reset :value "Start Again"))
         (d2 (create-div body))
         (tmp (create-section d2 :h3 :content "Selected Data Object"))
         (p1 (create-p d2 :content "Data Object name: "))
         (p2 (create-p d2 :content "Data Object RISA: "))
         (p3 (create-p d2 :content "Data Object Bay: "))
         (p4 (create-p d2 :content "Data Object Value: "))
         (p5 (create-p d2 :content "Data Object Validity: "))
         (p6 (create-p d2 :content "Data Object Timestamp: "))
         (d3 (create-div body))
         (bupdate (create-button d3 :content "Update"))
         (btoggle (create-button d3 :content "Toggle"))
         (bvalid (create-button d3 :content "Set Valid"))
         (binvalid (create-button d3 :content "Set Invalid"))
         (tmp (create-br d3))
         (f2 (create-form d3))
         (fe4 (create-form-element f2 :number :label (create-label f2 :content "Set Value: ")))
         (tmp (create-form-element f2 :submit :value "OK"))
         (tmp (create-form-element f2 :reset :value "Start Again"))
         (d5 (create-div body))
         (tmp (create-section d5 :h3 :content "Select IED:"))
         (f4 (create-form d5))
         (fe6 (create-form-element f4 :text :label (create-label f4 :content "IED: ")))
         (tmp (create-form-element f4 :submit :value "OK"))
         (tmp (create-form-element f4 :reset :value "Start Again"))
         (tmp (create-br f4))
         (tmp (create-br d5))
         (p7 (create-p d5 :content "IED name: "))
         (p8 (create-p d5 :content "IED status: "))
         (tmp (create-br d5))
         (updateied (create-button d5 :content "Update"))
         (startied (create-button d5 :content "Start"))
         (stopied (create-button d5 :content "Stop"))
         (gsimied (create-button d5 :content "Goose Sim Mode"))
         (d6 (create-div body))
         (tmp (create-section d6 :h3 :content "Data Attribute write (choose IED before!):"))
         (f5 (create-form d6))
         (fe7 (create-form-element f5 :text :label (create-label f5 :content "Data Attribute: ")))
         (tmp (create-br d6))
         (fe8 (create-form-element f5 :text :label (create-label f5 :content "Set Value: ")))
         (tmp (create-form-element f5 :submit :value "OK"))
         (tmp (create-form-element f5 :reset :value "Start Again"))
         (p9 (create-p d6 :content "Data Attribute Value: "))
         (dfile (create-div body))
         (tmp (create-section dfile :h2 :content "SCD File"))
         (hscd (create-section dfile
                               :h3
                               :content "No site loaded."))
         (tmp (create-section dfile :h4 :content "Select SCD to replace:"))
         (dfile2 (create-div body))
         (ffile (create-form dfile2 :encoding "multipart/form-data" :method :post :action "/uploading"))
         (ffile1 (create-form-element ffile :file :name "uploadf" :label (create-label ffile :content "File: ")))
         (tmp (create-form-element ffile :submit :value "OK"))
         ;; (tmp (create-form-element ffile :reset :value "Start Again"))
         )
    (flet ((update-scd-name ()
             (setf (text hscd)
                   (if *rspace*
                       (let ((myscd (first
                                     (directory
                                      (merge-pathnames
                                       (lispiec::conf-path *rspace*) "*.scd")))))
                         (if myscd (probe-file myscd) "No SCD file found!"))
                       "No site loaded.")))
           (update-do (my-do)
             (let ((my-name (lispiec::obj-reference-str my-do))
                   (my-value (cdc-get-value my-do))
                   (my-validity (cdc-get-validity my-do))
                   (my-t (cdc-get-timestamp my-do t))
                   (my-risa (lispiec::alias my-do))
                   (my-bay (lispiec::bay my-do)))
               (setf (text p1) (concatenate 'string
                                            "Data Object name: "
                                            my-name))
               (setf (text p2) (concatenate 'string
                                            "Data Object RISA: "
                                            my-risa))
               (setf (text p3) (concatenate 'string
                                            "Data Object Bay: "
                                            my-bay))
               (setf (text p4) (concatenate 'string
                                            "Data Object Value: "
                                            (write-to-string my-value)))
               (setf (text p5) (concatenate 'string
                                            "Data Object Validity: "
                                            (write-to-string my-validity)))
               (setf (text p6) (concatenate 'string
                                            "Data Object Timestamp: "
                                            my-t))))
           (get-do-tab (obj)
             (let ((my-name (text obj)))
               ;; (format t "got-do-tab ~a~%" my-name)
               (case (uiop:last-char my-name)
                 (#\1 (connection-data-item obj "dobj1"))
                 (#\2 (connection-data-item obj "dobj2"))
                 (#\3 (connection-data-item obj "dobj3")))))
           (save-do-tab (obj)
             (let ((my-name (text obj))
                   (my-do (connection-data-item obj "dobj")))
               ;; (format t "Save-to-tab ~a~%" my-name)
               (case (uiop:last-char my-name)
                 (#\1 (setf (connection-data-item obj "dobj1") my-do))
                 (#\2 (setf (connection-data-item obj "dobj2") my-do))
                 (#\3 (setf (connection-data-item obj "dobj3") my-do)))))
           (update-ied (obj)
             (setf (text p7) (concatenate 'string
                                          "IED name: "
                                          (iec-server-get-name obj)))
             (setf (text p8) (concatenate 'string
                                          "IED status: "
                                          (if (iec-server-is-running obj)
                                              (if (lispiec::goose-sim obj)
                                                  "Running(goose in simulation)"
                                                  "Running")
                                              (if (lispiec::goose-sim obj)
                                                  "Stopped(goose in simulation)"
                                                  "Stopped"))))))
      (update-scd-name)
      ;; (setf (file-accept ffile1) "text/xml")
      (flet ((update-do-tab (obj)
               (let ((my-do (get-do-tab obj)))
                 (format t "update-do-tab ~a~%" my-do)
                 (setf doactive obj)
                 (setf (color do1) "black")
                 (setf (color do2) "black")
                 (setf (color do3) "black")
                 (setf (color obj) "red")
                 (when my-do
                   (setf (connection-data-item obj "dobj") my-do)
                   (update-do my-do)))))
        (setf (color do1) "red")
        (setf (place-holder fe1) "type here..")
        (setf (requiredp fe1) t)
        (setf (size fe1) 40)
        (setf (place-holder fe2) "type here..")
        (setf (requiredp fe2) nil)
        (setf (size fe2) 40)
        (setf (place-holder fe3) "type here..")
        (setf (requiredp fe3) nil)
        (setf (size fe3) 40)
        (setf (size fe7) 40)
        (setf (place-holder fe7) "type here..")
        (setf (connection-data-item doactive "dobj") nil)
        (setf (connection-data-item doactive "dobj1") nil)
        (setf (connection-data-item doactive "dobj2") nil)
        (setf (connection-data-item doactive "dobj3") nil)
        (setf (size fe5) 60)
        ;; (setf (size ffile1) 60)
        (set-on-click bupdate (lambda (obj)
                                (declare (ignore obj))
                                (when (connection-data-item doactive "dobj")
                                  (update-do (connection-data-item doactive "dobj")))))
        (set-on-click btoggle (lambda (obj)
                                (declare (ignore obj))
                                (let ((my-do (connection-data-item doactive "dobj")))
                                  (when my-do
                                    (cdc-toggle my-do)
                                    (update-do my-do)))))
        (set-on-click bvalid (lambda (obj)
                               (declare (ignore obj))
                               (let ((my-do (connection-data-item doactive "dobj")))
                                 (when my-do
                                   (cdc-set-valid my-do)
                                   (update-do my-do)))))
        (set-on-click binvalid (lambda (obj)
                                 (declare (ignore obj))
                                 (let ((my-do (connection-data-item doactive "dobj")))
                                   (when my-do
                                     (cdc-set-invalid my-do)
                                     (update-do my-do)))))
        (set-on-click do1 (lambda (obj) (update-do-tab obj)))
        (set-on-click do2 (lambda (obj) (update-do-tab obj)))
        (set-on-click do3 (lambda (obj) (update-do-tab obj)))
        (set-on-submit f1
                       (lambda (obj)
                         (declare (ignore obj))
                         (let ((dd (site-get-dataobject-by-regex *rspace*
                                                                 (value fe1)
                                                                 (value fe2)
                                                                 (value fe3)))
                               (lldo (site-get-dataobjects-by-regex *rspace*
                                                                    (value fe1)
                                                                    (value fe2)
                                                                    (value fe3))))
                           (when dd
                             (setf (connection-data-item doactive "dobj") dd)
                             (make-data-list fe5 (mapcar #'car lldo))
                             (save-do-tab doactive)
                             (update-do dd)))))
        (set-on-submit f2
                       (lambda (obj)
                         (declare (ignore obj))
                         (let ((my-do (connection-data-item doactive "dobj")))
                           (when my-do
                             (cdc-set-value my-do (parse-integer (value fe4)))
                             (update-do my-do)))))
        (set-on-submit f3
                       (lambda (obj)
                         (declare (ignore obj))
                         (unless (string= (value fe5) "")
                           (let ((dd (site-get-dataobject-by-ref *rspace*
                                                                 (value fe5))))
                             (when dd
                               (setf (connection-data-item doactive "dobj") dd)
                               (save-do-tab doactive)
                               (update-do dd))))))
        (when *rspace* (make-data-list fe6 (site-get-ieds-name *rspace*)))
        (set-on-submit f4
                       (lambda (obj)
                         (declare (ignore obj))
                         (let ((my-ied (site-get-ied-by-name *rspace* (value fe6))))
                           (setf (connection-data-item f4 "ied") my-ied)
                           (update-ied my-ied))))
        (set-on-click updateied (lambda (obj)
                                  (let ((my-ied (connection-data-item obj "ied")))
                                    (update-ied my-ied))))
        (set-on-click startied (lambda (obj)
                                 (let ((my-ied (connection-data-item obj "ied")))
                                   (iec-server-start my-ied)
                                   (sleep 0.5)
                                   (iec-server-start-goose-on-interface my-ied "PRD")
                                   (update-ied my-ied))))
        (set-on-click stopied (lambda (obj)
                                (let ((my-ied (connection-data-item obj "ied")))
                                  (iec-server-stop my-ied)
                                  (update-ied my-ied))))
        (set-on-click gsimied (lambda (obj)
                                (let ((my-ied (connection-data-item obj "ied")))
                                  (if (lispiec::goose-sim my-ied)
                                      (setf (lispiec::goose-sim my-ied) nil)
                                      (setf (lispiec::goose-sim my-ied) t))
                                  (update-ied my-ied))))
        (set-on-submit f5
                       (lambda (obj)
                         (declare (ignore obj))
                         (let ((my-ied (connection-data-item obj "ied"))
                               (my-att (value fe7))
                               (my-value (value fe8)))
                           (when my-ied
                             (lispiec::iec-server-client-read-value-from-ref-and-connect
                              my-ied
                              my-att)
                             (unless (string= my-value "")
                               (alexandria:switch (my-value :test #'string=)
                                 ("True" (lispiec::iec-server-client-write-value my-ied t my-att))
                                 ("False" (lispiec::iec-server-client-write-value my-ied nil my-att))
                                 (t (lispiec::iec-server-client-write-value my-ied (parse-integer my-value) my-att))))
                             (setf (text p9) (concatenate
                                              'string
                                              "Data Attribute Value: "
                                              (write-to-string
                                               (lispiec::iec-server-client-read-value-from-ref-and-connect
                                                my-ied
                                                my-att))))))))

        ))))



(defun uploadfile (obj)
  (let ((mydata (form-multipart-data obj))
        (mypath (lispiec::conf-path *rspace*))
        (hscd (create-section obj
                              :h3
                              :content "Uploading....")))
    (sclparser::cp-files-in-dir-to-tmp mypath "scd")
    (sclparser:delete-files-in-dir mypath "scd")
    (sclparser:delete-files-in-dir mypath "icd")
    (sclparser:delete-files-in-dir mypath "cxml")
    (sclparser:delete-files-in-dir mypath "sh")
    (sclparser:delete-files-in-dir mypath "xml")
    (sclparser:delete-files-in-dir mypath "lisp")
    (sclparser:delete-files-in-dir mypath "cfg")
    (destructuring-bind (stream fname content-type)
        (form-data-item mydata "uploadf")
      (format t "filename = ~A - (contents printed in REPL)" fname)
      (let ((s (flexi-streams:make-flexi-stream stream :external-format :utf-8))
            )
        (with-open-file (wstr
                         (merge-pathnames fname mypath)
                         :direction :output
                         :if-exists :supersede
                         :if-does-not-exist :create)
          (uiop:copy-stream-to-stream s wstr))))
    (delete-multipart-data obj)
    (let ((thr (bt:make-thread
                (lambda () (progn (sb-thread:release-foreground)
                                  (progn
                                    (setf (text hscd) "No SCD File uploaded! ")
                                    (create-home-link hscd)
                                    (when
                                        (ignore-errors
                                         (probe-file
                                          (first
                                           (directory
                                            (merge-pathnames
                                             (lispiec::conf-path *rspace*) "*.scd")))))
                                      (exit-rspace t)
                                      (site-slynk-stop-server *rspace*)
                                      (setf (text hscd) "Loading New SCD File... around 15 minutes to reload ... wait ...")
                                      (handler-case
                                          (let ((inst (make-instance 'site :path (lispiec::conf-path *rspace*))))
                                            (site-add-ieds-from-dir inst (lispiec::conf-path *rspace*) lispiec-rspace::*slynk-tcp-port* *ds-regex*)
                                            ;; (update-scd-name)
                                            (setf (text hscd) "Restarting spacio...")
                                            (slynk:stop-server lispiec-rspace::*slynk-tcp-port*)
                                            (make-rspace (lispiec::conf-path *rspace*) lispiec-rspace::*slynk-tcp-port* *ds-regex*)
                                            (when *additional-script*
                                              (load *additional-script*))
                                            (setf (text hscd) "Spacio restarted. You can return to main spacio page. Attention: network is not reconfigured here. ")
                                            (create-home-link hscd))
                                        (error (c)
                                          (format t "Reconfiguration error.~&")
                                          (setf (text hscd) "Reconfiguration error. Bad SCD file. ")
                                          (create-home-link hscd)))))))
                :name "make-site"))))))


(defun start-web (&optional (port 8080))
  "Start Spacio web."
  (initialize 'on-new-window :port port)
  (set-on-new-window 'uploadfile :path "/uploading"))


(defun create-home-link (parent)
  (let ((link (create-a parent :content "Back to Home" :link "/")))
    (setf (color link) "blue")
    link))
