#!/bin/sh

for f in *.icd; do
    profile=$(basename "$f" .icd)
    cp $f $profile.cxml
done

for f in *.cxml; do
    sed --in-place "s/VDF/0/g" $f
    sed --in-place "s/ADU/0/g" $f
    sed --in-place 's/sAddr\=\"[0-9]\+\.[0-9]\+\"/sAddr=\"0\"/g' $f
    sed --in-place 's/^ \+//g' $f
    sed --in-place 's/None/0/g' $f
    sed --in-place ':label1 ; N ; $! b label1 ; s/\n//g' $f
done

for f in *.cxml; do
    profile=$(basename "$f" .cxml)
    java -jar genconfig.jar $f $profile.cfg
done
