(in-package #:lispiec)

;; (with-foreign-objects ((model 'iedmodel))
;;   (setf model (configfileparser-createmodelfromconfigfileex "/home/antonio/git/OUISTITI-network-toolkit/examples/python-demo-with-native-libiec61850/tg.cfg"))
;;   (defcvar devs :int)

;;   (setf *devs* (iedmodel-getlogicaldevicecount model))
;;   (print *devs*)
;;   (setf server (iedserver_create *devs*))
;;   )

(defcvar ptconf :pointer)
(setf *ptconf* (configfileparser-createmodelfromconfigfileex "/home/antonio/common-lisp/lispiec/TG.cfg"))

;(iedmodel-setiedname *ptconf* "toto")

(defcvar myld :pointer)
(setf *myld*
      (iedmodel-getdevicebyindex *ptconf* 1))

(defcvar myldi ModelNode)
(setf *myldi* (mem-aref *myld* '(:struct ModelNode)))

(defcvar mydo :pointer)
(setf *mydo*
      (iedModel-getModelNodeByObjectReference *ptconf* "TGLDTGINFRA/GAPC1.SPCSO1"))

(modelnode-getchildcount *mydo*)


(get-obj-name (get-child-by-name *mydo* "stVal"))


(get-obj-children *mydo*)


(defcvar myty modelnodetype)
(setf *myty*
      (modelnode-gettype *myld*))

(defcvar myatt :pointer)
(setf *myatt* (get-child-by-name *mydo* "stVal"))

(iedmodel-getlogicaldevicecount *ptconf*)

(defcvar myserver iedserver)

(setf *myserver* (iedserver-create *ptconf*))
(defcvar myfn controlhandler)
(setf *myfn* (callback spc-check))
(iedserver-setcontrolhandler *myserver* *mydo* *myfn* *myserver*)
                                        ; (iedserver-setperformcheckhandler *myserver* *mydo* (get-callback 'basic-perform) *mydo*)


(iedserver-start *myserver* 10109)

(iedserver-updatebooleanattributevalue *myserver* *myatt* nil)

(iedserver-stop *myserver*)
(iedserver-destroy *myserver*)
(iedmodel-destroy *ptconf*)


(setf *con* (iedconnection-create))
(setf *conerror* (foreign-alloc 'iedclienterror))
(iedconnection-connect *con* *conerror* "127.0.0.1" 10002)
(mem-ref *conerror* 'iedclienterror)
(setf *dir* (iedconnection-getdatasetdirectory *con* *conerror* "PLOE53CBO1PIU1LDTM2/LLN0.lot4_DQCE" (null-pointer)))
(linkedlist-string-parse *dir*)

(first (client-list-dataset-fcdas *con* "PLOE53CBO1PIU1LDTM2/LLN0.lot4_DQCE"))


(setf *con* (client-create))
(client-connect *con* "127.0.0.1" 10002)
(length (flatten (client-collect-var-spec-str-from-dataset *con* "PLOE53CBO1PIU1LDTM2/LLN0.lot4_DQCE")))
(setf *ds-read* (client-read-ds *con* "PLOE53CBO1PIU1LDTM2/LLN0.lot4_DQCE"))
(dataset-listify (clientdataset-getvalues *ds-read*))


(clientdataset-destroy *ds-read*)
(client-close-and-destroy *con*)
