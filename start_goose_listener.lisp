(require 'asdf)
(asdf:load-system :lispiec)
(in-package :lispiec-rspace)


(defvar *rspace-icd-path* "/home/antonio/ieds/")
(defvar *rspace-tcp-port* 20000)

(make-rspace *rspace-icd-path* *rspace-tcp-port*)


(defparameter *client-tg* (client-create))
(client-connect *client-tg* "10.0.0.2" 10001)
(defparameter dsn "SITETGENETG1LDMODEXPS/LLN0.DS_LDMODEXPS_GSE")
(client-list-dataset-fcdas *client-tg* dsn)
(lispiec::client-collect-var-spec-str-from-dataset *client-tg* dsn)

(defparameter *ds-mms1* (client-read-ds *client-tg* dsn))
(defparameter *ds-mms2* (client-read-ds *client-tg* "PLOE5TGENETG1LDMODEXPS/LLN0.DS_LDMODEXPS_GSE"))

(dataset-listify (clientdataset-getvalues *ds-mms1*))

(defun get-my-ds-value (myclientdataset)
  (lispiec-internal:mmsvalue-getboolean
   (first
        (nth 3
         (dataset-listify (clientdataset-getvalues myclientdataset))))))

(get-my-ds-value *ds-mms1*)

(defparameter *goose-rec* (client-create-goose-reciever "veth-goose"))
(defparameter *goose-sub* (client-create-goose-subscriber *rspace* "PLOE5TGENETG1LDMODEXPS/LLN0$GO$CB_LDMODEXPS_GSE" *ds-mms1* *ds-mms2*))

                                        ;>01-0C-CD-01-00-0B
(client-goose-subscriber-set-dst-mac *goose-sub* #x1 #xC #xCD #x1 #x0 #xB)
(client-goose-subscriber-set-appid *goose-sub* #x3000)

(with-goose-define-handler *rspace* goose-test-handler "PLOE5TGENETG1LDMODEXPS/LLN0$DS_LDMODEXPS_GSE" (prin1 "ok - received! "))

(client-close-and-destroy *client-tg*)

(client-goose-receiver-add-subs *goose-rec* *goose-sub*)

;; (iec-server-start-goose-on-interface *ied-tg* "eno16777736")
;; (client-goose-start-listening *goose-rec*)


;; (client-goose-stop-and-destroy *goose-rec*)
;; (cdc-toggle *do-locsta*)
