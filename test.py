import cl4py


class IecSession():
    def __init__(self):
        self._lisp = cl4py.Lisp()
        self._load = self._lisp.function('load')
        self._load('~/common-lisp/lispiec/lispiec.asd')
        self._load_system = self._lisp.function('asdf:load-system')
        self._load_system('lispiec')
        # List of lisp functions to be used :
        self.make_server = self._lisp.function('lispiec::make-iec-server')
        self.make_spc = self._lisp.function('lispiec::make-cdc-spc')
        self.make_sps = self._lisp.function('lispiec::make-cdc-sps')
        self.toggle = self._lisp.function('lispiec::cdc-toggle')
        self.on = self._lisp.function('lispiec::cdc-set-on')
        self.off = self._lisp.function('lispiec::cdc-set-off')
        self.start_iec = self._lisp.function('lispiec::iec-server-start')
        self.stop_iec = self._lisp.function('lispiec::iec-server-stop')
        self.destroy_iec = self._lisp.function(
            'lispiec::destroy-iec-server')
        self.set_invalid = self._lisp.function('lispiec::cdc-set-invalid')
        self.set_valid = self._lisp.function('lispiec::cdc-set-valid')

    def session(self):
        return self._lisp


class IecServer():
    def __init__(self, o_iec_session, s_config_path, s_ied_name):
        self._config_path = s_config_path
        self._s_ied_name = s_ied_name
        self._session = o_iec_session
        self._iec_server = self._session.make_server(s_config_path, s_ied_name)

    def start(self, port):
        self._session.start_iec(self._iec_server, port)

    def stop(self):
        self._session.stop_iec(self._iec_server)

    def destroy(self):
        self._session.destroy_iec(self._iec_server)


class CdcSpc():
    def __init__(self, o_iecserver, ld, ln, dobj):
        self._dobj = o_iecserver._session.make_spc(
            o_iecserver._iec_server, ld, ln, dobj)
        self._server = o_iecserver
        self._toggle = self._server._session.toggle
        self._on = self._server._session.on
        self._off = self._server._session.off
        self._set_invalid = self._server._session.set_invalid
        self._set_valid = self._server._session.set_valid

    def toggle(self):
        self._toggle(self._dobj)

    def set_on(self):
        self._on(self._dobj)

    def set_off(self):
        self._off(self._dobj)

    def set_invalid(self):
        self._set_invalid(self._dobj)

    def set_valid(self):
        self._set_valid(self._dobj)


class CdcSps():
    def __init__(self, o_iecserver, ld, ln, dobj):
        self._dobj = o_iecserver._session.make_sps(
            o_iecserver._iec_server, ld, ln, dobj)
        self._server = o_iecserver
        self._toggle = self._server._session.toggle
        self._on = self._server._session.on
        self._off = self._server._session.off
        self._set_invalid = self._server._session.set_invalid
        self._set_valid = self._server._session.set_valid

    def toggle(self):
        self._toggle(self._dobj)

    def set_on(self):
        self._on(self._dobj)

    def set_off(self):
        self._off(self._dobj)

    def set_invalid(self):
        self._set_invalid(self._dobj)

    def set_valid(self):
        self._set_valid(self._dobj)


if __name__ == '__main__':
    session = IecSession()
    server = IecServer(
        session, "/home/antonio/common-lisp/lispiec/TG.cfg", "MY")
    cdc = CdcSpc(server, "LDTGINFRA", "GAPC1", "SPCSO1")
