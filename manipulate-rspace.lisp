

(in-package :lispiec-rspace)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Check which ieds are running ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(site-list-ieds-running *rspace*)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Check ports being used ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(site-list-ieds-port *rspace*)

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Manage all CILOs/AIVO ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setf *cilos*
      (site-get-dataobjects-by-regex *rspace* "LDCMD" "CILO" "Ena"))

;; *cilos* now is a variable representing a list of CILO.Ena data objects

(list-dataobjects-set-on *cilos*)

(list-dataobjects-set-off *cilos*)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Manipulate data objects ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(cdc-toggle *do-tg-modexps*)
(cdc-toggle *do-4zbran1-modexpf*)
(cdc-toggle *do-4zbran1-dj*)

(setf *eclairage* (iec-server-get-dataobject *ied-tg* "LDTGINFRA" "GAPC1" "SPCSO1"))
(setf *eclairageind* (iec-server-get-dataobject *ied-tg* "LDTGINFRA" "GAPC1" "Ind1"))

(cdc-get-value *eclairage*)
(cdc-get-value *eclairageind*)

(cdc-toggle *eclairage*)

(setf *almth* (iec-server-get-dataobject *ied-tg* "LDTGINFRA" "GAPC2" "Ind1"))
(cdc-get-value *almth*)
(cdc-toggle *almth*)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Manipulate lists of data objects ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setf *modresultant*
      (site-get-dataobjects-by-regex *rspace* "LDMODEXPF" "IHMI" "LocSta"))

(list-dataobjects-get-value *modresultant*)

(site-get-dataobject-by-regex *rspace* "BCU4ZBRAN1LDCMDDJ" "CSWI1" "Pos")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Check which client's orcat is allowed to send commands ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(lispiec::orcat *ied-bcu4zbran1*)
(lispiec::orcat *ied-tg*)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Change which client's orcat is allowed to send commands in all site ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(site-change-orcat-local *rspace*)
(site-change-orcat-remote *rspace*)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Creating a list of data objects using RISA (alias) ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setf *dfs*
      (site-get-dataobjects-by-alias *rspace* "DF\."))
(list-dataobjects-toggle *dfs*)

;;;;;;;;;;;;;;;;;;
;; Stoping ieds ;;
;;;;;;;;;;;;;;;;;;

(iec-server-stop *ied-bcu4zbran1*)
(iec-server-stop *ied-scu1a4zbran1*)
(iec-server-stop *ied-scu1b4zbran1*)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Restart ied in 20 seconds ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(iec-server-restart *ied-bcu4zbran1* 20.0)
;; attention it may not work very well!

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Start publishing GOOSE on a giving interface ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(site-start-ieds-goose *rspace* "eno16777736")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Gracefuly stop everything  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(exit-rspace)
;;or
(site-stop-ieds *rspace*)
(site-destroy *rspace*)
(sb-ext:quit)

