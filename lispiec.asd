(asdf:defsystem :lispiec
  :serial t
  :version "1.6.1"
  :description "CFFI for libiec61850"
  :license "Public Domain"
  :author "Antonio Padoan"
  :defsystem-depends-on (:cffi-grovel)
  :build-operation "program-op"
  :build-pathname "spaciod"
  :entry-point "lispiec-rspace:main"
  :depends-on (:cffi :cffi-libffi :iterate :bordeaux-threads :cl-ppcre :cl-csv :alexandria :slynk :deeds :py4cl :parachute :clog :cxml-stp :unix-opts :pzmq :cl-json :local-time)
  :components
  ((:module "sclparser"
    :components ((:file "package")
                 (:file "parsesax")
                 (:file "sclparser-aux")
                 (:file "sclparser")))
   (:module "src"
    :components ((:file "package")
                 (:cffi-grovel-file "lispiec-internal") ;; c stuctures and enum
                 (:file "lispiec-functions") ;; c functions/declarations
                 (:file "lispiec-callbacks") ;;
                 (:file "lispiec-csv") ;; not much used anymore
                 (:file "lispiec-iec-server")
                 (:file "lispiec-inputs")
                 (:file "lispiec-setting-groups")
                 (:file "lispiec-classes-cdc-abs")
                 (:file "lispiec-classes-cdc")
                 (:file "lispiec-classes-cdc-extra")
                 (:file "lispiec-events")
                 (:file "lispiec-containers")
                 (:file "lispiec-site")
                 (:file "lispiec-client-aux")
                 (:file "lispiec-client-ds")
                 (:file "lispiec-client")))
   (:module "rspace"
    :components ((:file "package")
                 (:file "lispiec-rspace")
                 (:file "lispiec-rspace-main")
                 (:file "web")
                 ;; (:file "bot")
                 (:file "pano")))))
