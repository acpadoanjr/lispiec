
(in-package :lispiec-rspace)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                             User parameters                                 ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ieds à desactiver en simulation explicitement, sur besoin.
;; Pensez à a mettre le nom du ied complèt si possible.
;; Si le nom n'est pas trouvé parmis les ieds du SCD, il sera ignoré.
;; Dans la liste ci-dessous, il faut rajouter des strings ou les éditer sur besoin.
(defparameter *disable-explicitly*
  ;; add their name here:
  (list
   "ied-a"
   "ied-b"
   "ied-c"
   ))

;; Desactive tous les ieds du lot B (il ne seront pas simulés)  ==> surtout en PFPI : il faut la mettre t (true)
(defparameter *disable-tg* nil) ;; use t (for true) or nil (for false)

;; make all ACTs transient ==> utile pour tester les fugitives dans le PO
(defparameter *transient-act* nil) ;; use t (for true) or nil (for false)

;; on veut publier des gooses?  En OPF il faut pas utiliser des gooses!
(defparameter *goose-publishing* nil) ;; use t (for true) or nil (for false)

;; si on veut publier les goose, il faut lui dire quel inteface utiliser
(defparameter *goose-interface* "PRD")

;; en PFPI c'est utile pour faire marcher le MODEXPS avec spacio et
;; tester la veilla amu si la TG est simulé, il n'y a pas de sens de
;; garder cela à true.
(defparameter *supervise-tg* nil) ;; use t (for true) or nil (for false)

;; En tant que serveur le setting group n'est pas encore crée
;; automatiquement, il faut le créer sur besoin. Pour l'agsa on peut
;; le créer avec ce paramètre.  Mais attention, cela est utile que si
;; l'agsa est simulé avce spacio. Si c'est la vrai TG (en PFPI), ce
;; paramètre est inutile.
(defparameter *create-agsa-setting-group* nil)

;; Port pour lancer spacio web hmi. Si nil, on ne demarre pas le web
(defparameter *web-port* nil)


(setf lispiec::log-mode nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                             End of User parameters                          ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; do desactivation exlicitily:
(mapcar #'iec-server-disable
        (remove nil
                (mapcar #'(lambda (x) (site-get-ied-by-name *rspace* x))
                        *disable-explicitly*)))


;; search for the ied named TGENETG:
(defparameter *ied-tg-name* (find-ied-fullname *rspace* "TGTG"))

(when *disable-tg*
  (iec-server-disable (site-get-ied-by-name *rspace*
                                            *ied-tg-name*))
  (iec-server-disable (site-get-ied-by-name *rspace*
                                            "TGTOP"))
  (iec-server-disable (site-get-ied-by-name *rspace*
                                            "TGGRP1"))
  (iec-server-disable (site-get-ied-by-name *rspace*
                                            "TGGRPR"))
  (iec-server-disable (site-get-ied-by-name *rspace*
                                            "AUTP"))
  (iec-server-disable (site-get-ied-by-name *rspace*
                                            "TGGW1")))

;; disable garbage in SCD

(when (site-get-ied-by-name *rspace* "TEST")
  (iec-server-disable (site-get-ied-by-name *rspace* "TEST")))

(setf *switches*
      (remove-if-not #'(lambda (r) (cl-ppcre:scan "SW[ACTB][A-Z][0-9]" (iec-server-get-name r))) (site-get-ieds *rspace*)))

(mapcar #'iec-server-disable *switches*)

(setf *vms*
      (remove-if-not #'(lambda (r) (cl-ppcre:scan "FP(HYPER|AUTP|ICT|PO|OND)[0-9]" (iec-server-get-name r))) (site-get-ieds *rspace*)))

(mapcar #'iec-server-disable *vms*)

(when (site-get-ied-by-name *rspace*
                            "SYNC")
  (iec-server-disable (site-get-ied-by-name *rspace*
                                            "SYNC")))
(defparameter *ied-tg* (site-get-ied-by-name *rspace* *ied-tg-name*))

;; make BCU variables and objets that can be useful later
(mapcar #'make-bcu (find-ied-list-by-name *rspace* "BCU1"))


;; some others variables we may want do play latter:

(defparameter *do-tg-modexps* (iec-server-get-dataobject *ied-tg* "LDMODEXPS" "LLN0" "LocSta"))
(defparameter *do-tg-dfposte* (iec-server-get-dataobject *ied-tg* "LDTGSEC" "CALH1" "GrAlm"))
(defparameter *do-tg-dfposteta* (iec-server-get-dataobject *ied-tg* "LDTGSEC" "CALH5" "GrAlm"))
(defparameter *do-tg-almdanger* (iec-server-get-dataobject *ied-tg* "LDTGSEC" "ISAF1" "Alm"))
(defparameter *do-tg-arret-son*
  (iec-server-get-dataobject *ied-tg* "LDSONKLAX" "GAPC0" "SPCSO3"))
(defparameter *cilos* (site-get-dataobjects-by-regex *rspace* "LDCMD" "CILO" "Ena"))

;; starting make high level logic:

(with-dobj-define-handler dobj *rspace* modexps "LDMODEXPS" "LLN0" "LocSta"
  (update-modexpf-resultant dobj)
  (let ((ieds (remove-if-not #'(lambda (i) (or (search "PIU" (iec-server-get-name i))
                                               (search "SCU" (iec-server-get-name i))))
                       (site-get-ieds *rspace*))))
    (if (cdc-get-value dobj)
        (mapcar #'lispiec::iec-server-change-orcat-local ieds)
        (mapcar #'lispiec::iec-server-change-orcat-remote ieds))))
(with-dobj-define-handler dobj *rspace* modexpf "LDMODEXPF" "LLN0" "LocSta"
  (let* ((lispiec::ied *ied-tg*)
         (lispiec::iobj (iec-server-get-dataobject lispiec::ied "LDMODEXPS" "LLN0" "LocSta")))
    (update-modexpf-resultant lispiec::iobj)))
;; (with-dobj-define-handler dobj *rspace* ecalire1 "LDTGINFRA" "GAPC1" "SPCSO1"
;;   (propagate-values dobj (iec-server-get-dataobject *ied-tg* "LDTGINFRA" "GAPC1" "Ind1")))
(with-dobj-define-handler dobj *rspace* ecalire2 "LDTGINFRA" "GAPC1" "SPCSO2"
  (propagate-values dobj (iec-server-get-dataobject *ied-tg* "LDTGINFRA" "GAPC1" "Ind2")))
(with-dobj-define-handler dobj *rspace* ecalire3 "LDTGINFRA" "GAPC1" "SPCSO3"
  (propagate-values dobj (iec-server-get-dataobject *ied-tg* "LDTGINFRA" "GAPC1" "Ind3")))
(with-dobj-define-handler dobj *rspace* ecalire4 "LDTGINFRA" "GAPC1" "SPCSO4"
  (propagate-values dobj (iec-server-get-dataobject *ied-tg* "LDTGINFRA" "GAPC1" "Ind4")))
(with-dobj-define-handler dobj *rspace* coupatemps "LDTGSEC" "GAPC0" "SPCSO3"
  (propagate-values dobj (iec-server-get-dataobject *ied-tg* "LDTGSEC" "GAPC0" "Ind2")))
(with-dobj-define-handler dobj *rspace* alarmedanger "LDTGSEC" "GAPC0" "SPCSO2"
  (propagate-values dobj *do-tg-almdanger*))
(with-dobj-define-transient *rspace* transspc02 "LDSONKLAX" "GAPC0" "SPCSO2" (* 60 2))
;; (with-dobj-define-handler dobj *rspace* modexpf "BCU1LDMODEXPF1" "LLN0" "LocSta"
;;   (propagate-values-same-bay dobj "BCU1LDMODEXPF1" "IHMI" "LocSta" *rspace*))


  ;; code automatique BCU et SCU

(defun define-handlers ()


  (with-dobj-define-handler dobj *rspace* djbcu2scua "BCU1LDCMDDJ" "CSWI1" "Pos"
    (propagate-values-same-bay dobj "1LDDJ$" "XCBR0" "Pos" *rspace*)
    (propagate-values-same-bay dobj "BCU1LDCMDDJ" "CSWI2" "Pos" *rspace*))

  (with-dobj-define-handler dobj *rspace* djbcu2bcu "BCU1LDCMDDJ" "CSWI2" "Pos"
    (propagate-values-same-bay dobj "BCU1LDCMDDJ" "CSWI1" "Pos" *rspace*))

  (with-dobj-regex-define-handler dobj *rspace* djscua2scub "1LDDJ$" "XCBR0" "Pos"
    (propagate-values-same-bay dobj "2LDDJ$" "XCBR0" "Pos" *rspace*)
    (propagate-values-same-bay dobj "BCU1LDCMDDJ$" "CSWI1" "Pos" *rspace*))

  (with-dobj-define-handler dobj *rspace* ssbcu2scua "BCU1LDCMDSS1" "CSWI0" "Pos"
    (propagate-values-same-bay dobj "SCU1LDSS1$" "XSWI1" "Pos" *rspace*))

  (with-dobj-regex-define-handler dobj *rspace* ssscua2scub "SCU1LDSS1$" "XSWI1" "Pos"
    (propagate-values-same-bay dobj "SCU2LDSS1$" "XSWI1" "Pos" *rspace*)
    (propagate-values-same-bay dobj "BCU1LDCMDSS1$" "CSWI0" "Pos" *rspace*))

  (with-dobj-define-handler dobj *rspace* ss2bcu2scua "BCU1LDCMDSS2" "CSWI0" "Pos"
    (propagate-values-same-bay dobj "SCU1LDSS2$" "XSWI1" "Pos" *rspace*))

  (with-dobj-regex-define-handler dobj *rspace* ss2scua2scub "SCU1LDSS2$" "XSWI1" "Pos"
    (propagate-values-same-bay dobj "SCU2LDSS2$" "XSWI1" "Pos" *rspace*)
    (propagate-values-same-bay dobj "BCU1LDCMDSS2$" "CSWI0" "Pos" *rspace*))

  (with-dobj-define-handler dobj *rspace* sabcu2scua "BCU1LDCMDSA1" "CSWI0" "Pos"
    (propagate-values-same-bay dobj "SCU1LDSA1$" "XSWI1" "Pos" *rspace*))

  (with-dobj-regex-define-handler dobj *rspace* sascua2scub "SCU1LDSA1$" "XSWI1" "Pos"
    (propagate-values-same-bay dobj "SCU2LDSA1$" "XSWI1" "Pos" *rspace*)
    (propagate-values-same-bay dobj "BCU1LDCMDSA1$" "CSWI0" "Pos" *rspace*))

  (with-dobj-define-handler dobj *rspace* stbcu2scua "BCU1LDCMDST" "CSWI0" "Pos"
                            (propagate-values-same-bay dobj "SCU1LDST" "XSWI1" "Pos" *rspace*))

  (with-dobj-regex-define-handler dobj *rspace* stscua2scub "SCU1LDST" "XSWI1" "Pos"
                                  (propagate-values-same-bay dobj "SCU2LDST" "XSWI1" "Pos" *rspace*)
                                  (propagate-values-same-bay dobj "BCU1LDCMDST" "CSWI0" "Pos" *rspace*))

  ;; perturbe le mode maintenance:
  (with-dobj-define-handler dobj *rspace* changeorcat "LDMODEXPF1" "IHMI" "LocSta"
                            (let ((loc (cdc-get-value dobj)))
                              (if loc
                                  (lispiec::iec-server-change-orcat-local (iec-server dobj))
                                (progn
                                  ;; (sleep 5.0) ;; wait to avoid trouble maintenance
                                  (unless (lispiec::cdc-is-test dobj)
                                    (lispiec::iec-server-change-orcat-remote (iec-server dobj)))))))


  (with-dobj-regex-define-handler dobj *rspace* balisage "SCU\\dLDBALIS" "GAPC0" "SPCSO1$"
    (let* ((ied (iec-server dobj))
           (out (iec-server-get-dataobject ied "LDBALIS" "GAPC0" "Ind1")))
      (propagate-values dobj out)))
  (with-dobj-regex-define-handler dobj *rspace* ecl1 "LDTGINFRA" "GAPC1$" "SPCSO1"
    (let ((ret (site-get-dataobject-by-regex *rspace* "LDTGINFRA" "GAPC1$" "Ind1$")))
      (when ret (propagate-values dobj ret))))
  (with-dobj-regex-define-handler dobj *rspace* ecl2 "LDTGINFRA" "GAPC1$" "Ind1$"
    (let ((ret1 (site-get-dataobject-by-regex *rspace* "1LDITFTG" "GAPC4$" "Ind5$"))
          (ret2 (site-get-dataobject-by-regex *rspace* "2LDITFTG" "GAPC4$" "Ind5$"))
          (vl (cdc-get-value dobj)))
      (when vl
        (when ret1 (cdc-toggle ret1))
        (when ret2 (cdc-toggle ret2)))))
                                        ;
                                        ;(with-dobj-define-transient *rspace* transreinitcoup "LDTGSEC" "GAPC0" "SPCSO1")
                                        ;(with-dobj-alias-define-transient *rspace* changetat1 "CHANGEMENT ETAT" "3THEIX1")
                                        ;(with-dobj-alias-define-transient *rspace* changetat2 "CHANGEMENT ETAT" "3THEIX2")
  (with-site-define-cycle-handler *rspace* cycle-mv "mv loop"
    (toggle-bcu-mvs)
    (toggle-bcu-cmvs)))
(define-handlers)

(site-change-orcat-local *rspace*)
;; (site-change-orcat-remote *rspace*)



;; start simulation ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(site-start-ieds *rspace*)


;; Automatic functions to be created after site-start-ieds (in other to avoid event avalanche if started before site-start-ieds)

(defun propagate-mod (mod parent-beh-int)
  (let ((mod-int (cdc-get-value mod))
        (beh (iec-server-get-dataobject (iec-server mod) (lispiec::cdc-get-logical-device-inst-str mod) (lispiec::cdc-get-logical-node-str mod) "Beh")))
    (unless (null beh)
      (if (eql parent-beh-int 1)
          (cdc-set-value beh mod-int)
          (if (eql parent-beh-int 5)
              (cdc-set-value beh 5)
              (if (and (eql parent-beh-int 3)
                       (eql mod-int 1))
                  (cdc-set-value beh 3)
                  (cdc-set-value beh mod-int)))))))

(defun define-other-handlers ()
  (with-dobj-define-handler dobj *rspace* modbeh "" "LLN0" "Mod"
    (let ((beh (iec-server-get-dataobject (iec-server dobj) (lispiec::cdc-get-logical-device-inst-str dobj) (lispiec::cdc-get-logical-node-str dobj) "Beh"))
          (dos (iec-server-get-dataobjects-regex (iec-server dobj) "Mod" "^(?!L).*"
                                                 (concatenate 'string
                                                              (lispiec::cdc-get-logical-device-str dobj)
                                                              "$"))))
      (unless (null beh)
        (propagate-values dobj beh)
        (mapcar #'(lambda (mydo) (propagate-mod (cdr mydo) (cdc-get-value dobj))) dos))))
  (with-dobj-regex-define-handler dobj *rspace* modbehln "" "^[A-K|M-Z]" "Mod"
    (let ((parent-beh-int (lispiec::cdc-get-beh dobj)))
      (propagate-mod dobj parent-beh-int)))
  (with-dobj-define-handler dobj *rspace* systememod "System" "LLN0" "Mod"
    (let ((mods (iec-server-get-dataobjects-regex (iec-server dobj) "Mod" "LLN0" "[^m]$")))
      (unless (null mods)
        (mapcar #'(lambda (row) (propagate-values dobj (cdr row))) mods)))) ;; BPU GE
  (with-dobj-define-handler dobj *rspace* behtest "" "LLN0" "Beh"
    (let* ((dos (iec-server-get-dataobjects-regex (iec-server dobj) "^(?!Beh$).*" ""
                                                  (concatenate 'string
                                                               (lispiec::cdc-get-logical-device-str dobj)
                                                               "$")))
           (mod (iec-server-get-dataobject (iec-server dobj) (lispiec::cdc-get-logical-device-inst-str dobj) "LLN0" "Mod"))
           (old-value (lispiec::old-value dobj))
           (new-value (cdc-get-value dobj))
           (dos-no-mod (remove (lispiec::obj-reference-str mod) dos :key #'car)))
      (when (or (and (or (eql old-value 5) (< old-value 3)) ; no test before
                     (and (< new-value 5) (> new-value 2))) ; test now
                (and (or (eql new-value 5) (< new-value 3)) ; no test now
                     (and (< old-value 5) (> old-value 2))) ; test before
                )
        (mapcar (lambda (row) (cdc-update-model (cdr row))) dos))
      (when (eql new-value 5)
        (mapcar #'(lambda (row) (cdc-set-invalid (cdr row))) dos-no-mod))
      (when (and (eql old-value 5) (< new-value 5))
        (mapcar #'(lambda (row) (cdc-set-valid (cdr row))) dos-no-mod))
      (set-modexp-maint-obj dobj)
      ))
  (defparameter *bal-regex* (ppcre:create-scanner ".+_LDSUIED_(\\w+) ([0-9]+) \\w+ ([0-9]+)_([0-9]+)"))
  (with-dobj-regex-define-handler dobj *rspace* balisageied "(LDBALIS|LDSL|LDDJ|LDSA|LDST|LDSS)\\w*$" "GAPC" "SPCSO"
    (let ((ied (iec-server dobj))
          (main-ref (lispiec::obj-reference-str dobj))
          (dobj2 (site-get-dataobject-by-ref
                  *rspace*
                  (ppcre:regex-replace "SPCSO"
                                       (lispiec::obj-reference-str dobj)
                                       "Ind"))))
      (when dobj2
        (let ((inref (cdr
                      (find (lispiec::obj-reference-str dobj2)
                            ;; (list-dataobjects-get-value)
                            (iec-server-get-inrefs (iec-server dobj))
                            :test #'string=
                            :key #'(lambda (r) (cdc-get-value (cdr r)))))))
          (when inref
            (let ((purp (cdc-get-purpose inref)))
              (multiple-value-bind (tot vec)
                  (ppcre:scan-to-strings *bal-regex* purp)
                (when vec
                  (let ((dobj3
                          (site-get-dataobject-by-ref
                           *rspace*
                           (concatenate 'string
                                        (iec-server-get-name (iec-server dobj))
                                        "LDSUIED/"
                                        (aref vec 0)
                                        (aref vec 1)
                                        ".CmdDO"
                                        (aref vec 2)))))
                    (when dobj3
                      (propagate-values dobj dobj3)))))))
          (mapcar #'(lambda (r) (lispiec::iec-server-del-dataobject ied r)) (mapcar #'cdr (iec-server-get-inrefs ied)))
          (with-slots (lispiec::inrefs) ied (setf lispiec::inrefs nil)))))))



(define-other-handlers)

  ;; wait until to be sure all ieds are running

(loop while (not (is-site-running *rspace*))
      do (sleep 2))

;; other logic:

(list-dataobjects-set-on *cilos*)
;(cdc-set-value *do-3theix2-rse* 5)
;(cdc-set-value *do-3theix1-rse* 5)
(defparameter *dfs*
      (site-get-dataobjects-by-alias *rspace* "DF\."))
(defparameter *alarme-danger*
      (site-get-dataobjects-by-alias *rspace* "ALA"))


  ;; analog values initial values

(defun start-cmvs (&optional (basev (/ 63000 (sqrt 3))))
  (let ((*ph* basev))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU0" "PhV.phsA") (lispiec::polar-to-complex *ph* 0.1))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU0" "PhV.phsB") (lispiec::polar-to-complex *ph* 120.0))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU0" "PhV.phsC") (lispiec::polar-to-complex *ph* -120.3))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDPHAS1" "MMXU0" "PhV.phsA") (lispiec::polar-to-complex *ph* -0.1))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDPHAS1" "MMXU0" "PhV.phsB") (lispiec::polar-to-complex *ph* 120.1))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDPHAS1" "MMXU0" "PhV.phsC") (lispiec::polar-to-complex *ph* -120.1))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU0" "A.phsA") (lispiec::polar-to-complex 1.2 -10.1))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU0" "A.phsB") (lispiec::polar-to-complex 1.2 109.8))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU0" "A.phsC") (lispiec::polar-to-complex 1.2 -130.1))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDPHAS1" "MMXU0" "A.phsA") (lispiec::polar-to-complex 1.2 -10.2))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDPHAS1" "MMXU0" "A.phsB") (lispiec::polar-to-complex 1.2 110.1))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDPHAS1" "MMXU0" "A.phsC") (lispiec::polar-to-complex 1.2 -129.9))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU0" "TotW") 500.0)
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU0" "TotVAr") -40.0)
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU101" "PhV.phsA") (lispiec::polar-to-complex *ph* 0.1))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU101" "PhV.phsB") (lispiec::polar-to-complex *ph* 120.0))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU101" "PhV.phsC") (lispiec::polar-to-complex *ph* -120.3))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU102" "PhV.phsA") (lispiec::polar-to-complex *ph* 0.1))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU102" "PhV.phsB") (lispiec::polar-to-complex *ph* 120.0))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU102" "PhV.phsC") (lispiec::polar-to-complex *ph* -120.3))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU103" "PhV.phsA") (lispiec::polar-to-complex *ph* 0.1))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU103" "PhV.phsB") (lispiec::polar-to-complex *ph* 120.0))
    (list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* "BCU1LDCAP1" "MMXU103" "PhV.phsC") (lispiec::polar-to-complex *ph* -120.3))))

(start-cmvs)

                                        ; substation initial topology


(site-change-orcat-local *rspace*)

;verrines manque tension
;; (setf *verrinephase1* (site-get-dataobject-by-regex *rspace* "CBO1BCU1LDMQUB1" "CB00001001FXOT0" "Op"));verrine gauche
;; (cdc-set-on *verrinephase1*)
;; (setf *verrinephase2* (site-get-dataobject-by-regex *rspace* "CBO1BCU1LDMQUB1" "CB00001003FXOT0" "Op"));verrine droite
;; (setf *verrinephase2b* (site-get-dataobject-by-regex *rspace* "CBO1BCU1LDMQUB1" "CB00001003FXUT0" "Op"));verrine droite
;; (cdc-set-on *verrinephase2*)
;; (cdc-set-off *verrinephase2b*)

;; (setf *verrinephaset1* (site-get-dataobject-by-regex *rspace* "X1BCU1LDCMDDJ" "FXOT1" "Op"))
;; (cdc-set-on *verrinephaset1*)
;; (setf *verrinephaset2* (site-get-dataobject-by-regex *rspace* "X2BCU1LDCMDDJ" "FXOT1" "Op"))
;; (cdc-set-on *verrinephaset2*)

;; (cdc-set-on (site-get-dataobject-by-alias *rspace* "PRESENCE1" "TGENE"))


;; etat reseau
(list-dataobjects-set-on (site-get-dataobjects-by-regex *rspace* "LDSUIED" "LCCH" "ChLiv"))


;; PhysicalHealths start ok

(list-dataobjects-set-value (site-get-dataobjects-by-regex *rspace* ".*" "LPHD0" "PhyHealth") 1)


;; inputs physiques

(list-dataobjects-set-on (site-get-dataobjects-by-regex *rspace* "SCU2LDSUIED" "LPDI" "Ind[2|6]$"))
(list-dataobjects-set-on (site-get-dataobjects-by-regex *rspace* "SCU2LDSUIED" "DO" "CmdDO[3|9]$"))
;; (list-dataobjects-set-off (site-get-dataobjects-by-regex *rspace* "LDSUIED" "LPDI" "Ind[0-9]?[2|6]"))
;; (list-dataobjects-set-off (site-get-dataobjects-by-regex *rspace* "LDSUIED" "DO" "CmdDO[0-9]?[3|9]$"))


;; making acts transient
(when *transient-act*
  (with-act-define-transient *rspace* act-handler))


(when *goose-publishing*
  (site-start-ieds-goose *rspace* *goose-interface*))

;; setting group

(when *create-agsa-setting-group*
  (defparameter *mysg1* (make-setting-group *rspace* "TGAUT1" "LDAGSA1"))
  (with-sgcb-define-handler sg setpoint *rspace* agsa "AGSA"
    (let ((server (lispiec::setting-group-server sg))
          (ld (lispiec::setting-group-logical-device-inst sg)))
      (let ((dd (site-get-dataobject-by-regex *rspace*
                                              (concatenate 'string (iec-server-get-name server) ld)
                                              "GENGAPC0"
                                              (concatenate 'string (write-to-string setpoint) "$")))
            (ll (site-get-dataobjects-by-regex *rspace*
                                              (concatenate 'string (iec-server-get-name server) ld)
                                              "GENGAPC0"
                                              "")))
        (list-dataobjects-set-off ll)
        (cdc-set-on dd))))
  (with-sgcb-define-handler-edit sg setpoint *rspace* agraed "AGSA")
  (with-sgcb-define-handler-edit-confirm sg setpoint *rspace* agraedc "AGSA"))

;; veille amu

(setf *triggervm* (cdr (first (site-get-dataobjects-by-regex *rspace* "CMDMULTAMU" "GAPC3" "Ind1"))))
;; (cdc-toggle *triggervm*)
(setf *vamus* (site-get-dataobjects-by-regex *rspace* "LDAMU" "GAPC" "Ind1"))
(list-dataobjects-set-on *vamus*)

(with-dobj-define-transient *rspace* vamutr1 "CMDMULTAMU" "GAPC3" "SPCSO1" 1)
(with-dobj-define-handler dobj *rspace* vamumult "CMDMULTAMU" "GAPC3" "SPCSO1"
  (propagate-values dobj *triggervm*))
(with-dobj-define-inv-transient *rspace* vamutr "LDAMU" "GAPC" "Ind1" 10)
(with-dobj-define-handler dobj *rspace* vamu "CMDMULTAMU" "GAPC3" "Ind1"
  (when (cdc-get-value dobj)
    (list-dataobjects-set-off *vamus*)))


;; goose mirroring

(defun mirror-ied-using-goose (ied)
  (unless (iec-server-client-test ied)
    (iec-server-client-create-connection ied))
  (lispiec::make-thread  #'(lambda ()
                             (loop while (not (iec-server-client-test ied))
                                   do (progn (sleep 20)
                                             (iec-server-client-create-connection ied)))
                             (let ((rr (if (lispiec::goose-recs *rspace*)
                                           (first goose-recs)
                                           (lispiec::make-site-client-goose-receiver *rspace* *goose-interface*))))
                               (iec-server-client-supervise-gooses *ied-tg* rr "GS[IE]$")
                               (lispiec::site-client-goose-receivers-start-listening *rspace*)))))


(when (and *supervise-tg* *disable-tg*)
  (mirror-ied-using-goose *ied-tg*))

(when *web-port*
  (start-web *web-port*))

(site-change-orcat-local *rspace*)
